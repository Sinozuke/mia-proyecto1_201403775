#ifndef PERMISOS_H
#define PERMISOS_H

void cambiar_permisos(Montaje *montaje, Sesion *sesion, char id[], char path[], char permisos[]);
void cambiar_propetario(Montaje *montaje, Sesion *sesion, char id[], char path[], char username[]);

#endif /* PERMISOS_H */

