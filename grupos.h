#ifndef GROUP_H
#define GROUP_H

void crear_grupo(Montaje *montaje, char *nombre, char *id);
void eliminar_grupo(Montaje *montaje, char *nombre, char *id);
void cambiar_grupo(Montaje *montaje, char nombre[], char id[],char usuario[]);

#endif /* GROUP_H */

