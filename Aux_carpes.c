#include <string.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_carpes.h"

void llenar_carpes(carpetas *carpes, char *cont) {
    int contador = 0;
    int contador2 = 0;
    int lon = strlen(cont);
    for (int i = 1; i < lon; i++) {
        if (cont[i] != '/') {
            if (cont[i] == 46 || cont[i] == 11 || cont[i] == 95 || cont[i] == 32 || cont[i] > 47 && cont[i] < 58 || cont[i] > 63 && cont[i] < 91 || cont[i] > 96 && cont[i] < 122) {
                carpes[contador].bname[contador2] = cont[i];
                contador2++;
            }if(cont[i] == 92){
                i++;
                carpes[contador].bname[contador2] = cont[i];
                contador2++;
            }
        } else {
            contador++;
            contador2 = 0;
        }
    }
}

void vaciar_carpes(carpetas *carpes, int numero) {
    for (int i = 0; i < numero; i++)
        clean_string(carpes[i].bname, MAX_NAME_SIZE);
}

int contar_carpes(char *path) {
    int devolver = 0;
    for (int i = 0; path[i]; i++)
        if (path[i] == '/')
            devolver++;
        else if(path[i]==92)
            i++;
    return devolver;
}