#ifndef FS_OP_H
#define FS_OP_H

void copiar_elemento(Montaje *montaje, Sesion *sesion, char id[], char path_destino[], char path_elemento[]);
void mover_elemento(Montaje *montaje, Sesion *sesion, char id[], char path_destino[], char path_elemento[]);

#endif /* FS_OP_H */

