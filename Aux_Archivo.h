#ifndef ARCHIVO_H
#define ARCHIVO_H

void contenido_archivo(char *buffer,FILE *disco,SB_S2 sb,inodo_S2 inode);
void editar_archivo(FILE *disco, SB_S2 *sb, int posicion_inodo, char cont[], char name[]);
void update_archivo(FILE *disco, SB_S2 *sb, int posicion_inodo, char name[]);

#endif /* ARCHIVO_H */

