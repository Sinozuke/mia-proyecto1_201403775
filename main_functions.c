#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "main_data.h"
#include "main_functions.h"

char cadena[MAXLON + 1];

void get_string(char *buffer) {

    clean_string(cadena, MAXLON + 1);
    
    while(1){
        fgets(cadena, MAXLON, stdin);
        for (int i = 0; cadena[i] != '\0'; i++)
            if (cadena[i] == '\n')
                cadena[i] = '\0';
        if(cadena[0]=='\0'){
            printf("Error: no deve dejar la casilla vacia\n");
            clean_string(cadena,MAXLON);
        }else{
            break;
        }
    }

    strncpy(buffer, cadena, MAXLON);

}

void get_permisos(char *buffer) {

    clean_string(cadena, MAXLON + 1);
    
    while(1){
        fgets(cadena, MAXLON, stdin);
        for (int i = 0; cadena[i] != '\0'; i++)
            if (cadena[i] == '\n')
                cadena[i] = '\0';
        if(cadena[0]=='\0'){
            printf("Error: no deve dejar la casilla vacia\n");
            clean_string(cadena,MAXLON);
        }else{
            if(cadena[0]<56 && cadena[0]>47 && cadena[1]<56 && cadena[1]>47 && cadena[2]<56 && cadena[2]>47 && cadena[3]=='\0'){            
                break;
            }else{
                printf("Error: valor para los permisos invalido\n");
                clean_string(cadena,MAXLON);
            }
        }
    }

    strncpy(buffer, cadena, MAXLON);

}

void clean_string(char *buffer, int longitud) {

    for (int i = 0; i<longitud; i++) {
        buffer[i] = '\0';
    }

}

void print_menu(int menu){
    switch(menu){
        case 1://SELECCION MENU PRINCIPAL
            printf("Seleccione una opcion:\n");
            printf("1.Crear disco\n");
            printf("2.Eliminar disco\n");
            printf("5.Crear particion\n");
            printf("6.Montar particion\n");
            printf("7.Desmontar particion\n");
            printf("8.Formatear Particion(Ext3)\n");
            printf("9.Iniciar sesion\n");
            printf("10.Cerrar sesion\n");
            printf("11.Crear grupo\n");
            printf("12.Eliminar grupo\n");
            printf("13.Crear usuario\n");
            printf("14.Eliminar usuario\n");
            printf("15.Cambiar permisos de un elemento\n");
            printf("16.Crear Archivo\n");
            printf("17.Crear Directorio\n");
            printf("18.Mostrar Contenido Archivo\n");
            printf("19.Eliminar Archivo\n");
            printf("20.Eliminar Carpeta\n");
            printf("21.Editar Archivo\n");
            printf("22.Renombrar Archivo\n");
            printf("23.Renombrar Carpeta\n");
            printf("24.Copiar Elemento\n");
            printf("25.Mover elemento\n");
            printf("26.Cambiar Propetario a Elemento\n");
            printf("27.cambiar Grupo a Usuario\n");
            printf("28.Reporte Bitmap Bloques\n");
            printf("29.Reporte Bitmap Inodos\n");
            printf("30.Reporte Journaling\n");
            printf("31.Reporte Inodo\n");
            printf("32.Reporte Bloque\n");
            printf("33.Reporte Super Bloque\n");
            printf("34.Reporte Grafico de directorios\n");
            printf("35.Reporte Grafico de un Archivo\n");
            printf("36.Reporte Grafico de los Archivos en un directorio\n");
            printf("37.Reporte Completo del sistema\n");
            printf("0.--SALIR--\n");
            break;
        case 6:
            printf("Seleccione la medida que desea utilizar\n");
            printf("1.megabytes\n");
            printf("2.kilobytes\n");
            break;
        case 7:
            printf("Seleccione el tipo de particion que desea crear:\n");
            printf("1.Primaria\n");
            printf("2.Extendida\n");
            printf("3.Logica\n");
            break;
        case 8:
            printf("Seleccione la medida que desea utilizar\n");
            printf("1.megabytes\n");
            printf("2.kilobytes\n");
            printf("3.bytes\n");
            break;
        case 9:
            printf("Seleccione que tipo de formato deseas aplicar\n");
            printf("1.Rapido\n");
            printf("2.Completo\n");
            break;
        case 10:
            printf("Seleccione que tipo de formato deseas aplicar\n");
            printf("1.Rapido\n");
            printf("2.Completo\n");
            break;
    }

}

void print_welcome(){
    printf("--------------------------------------------------------------\n");
    printf("--------------MANEJO E IMPLEMENTACION DE ARCHIOS--------------\n");
    printf("--------------------SEGUNDO SEMESTRE 2017---------------------\n");
    printf("-------------------------PROYECTO NO.1------------------------\n");
    printf("--------------------------------------------------------------\n");
}

void get_number(int *buffer) {

    int bandera = 1;
    int resultado = 0;
    int correct = 1;

    clean_string(cadena, MAXLON + 1);

    while (bandera) {
        get_string(cadena);
        for (int i = 0; cadena[i]; i++) {
            if (cadena[i] > 47 && cadena[i] < 58) {
                if (correct) {
                    resultado = (resultado * 10)+(cadena[i] - 48);
                }
            } else {
                correct = 0;
            }
        }

        if (correct) {
            bandera = 0;
            *buffer = resultado;
        } else {
            printf("Error: el valor no es entero.\n");
            clean_string(cadena, MAXLON + 1);
            correct = 1;
            resultado = 0;
        }
    }

}

int crear_directorios(char *path) {

    char *direc = strtok(path, "/");

    if (!direc)
        return 0;

    char dirreccion[MAXLON+1] = "/";

    struct stat st = {0};

    while (direc) {
        if (!strstr(dirreccion, ".txt") && !strstr(dirreccion, ".jpg"))
            if (stat(dirreccion, &st) == -1)
                mkdir(dirreccion, 0700);

        strcat(dirreccion, direc);
        strcat(dirreccion, "/");
        direc = strtok(NULL, "/");
    }

    if (!strstr(dirreccion, ".txt") && !strstr(dirreccion, ".jpg"))
        if (stat(dirreccion, &st) == -1)
            mkdir(dirreccion, 0700);

    return 1;
}


int string_lenght(char *buffer){
    
    int total = 0;
    
    while(buffer[total]!='\0'){
       total++;
    }

    return total;
}