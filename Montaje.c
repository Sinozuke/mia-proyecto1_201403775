#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "Montaje.h"

int path_e(Montaje *montajes, char *path);
int name_e(Montaje *montajes, int j, char *name);
int id_e(Montaje *montajes, int j, char *id);
int contar_montajes(Montaje *mont, int j);

int encontrar_particion(int *size_p, char *tipo, char *status_p, FILE *disco, MBR mbr, char *name, char *allocation, char* fit);

void mount_c(Montaje *montajes, char *path, char *name) {

    struct stat st = {0};
    if (stat(path, &st) == -1) {
        printf("ERROR: el archivo especificado no existe.\n");
        printf("-----------MONTAJE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-----------MONTAJE FALLIDO-----------\n\n");
        return;
    }

    MBR leido;

    if (fread(&leido, sizeof (MBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-----------MONTAJE FALLIDO-----------\n\n");
        fclose(disco);
        return;
    }

    int size_p;
    char status_p;
    char tipo;
    char allocation;
    char fit;
    int encontrado = encontrar_particion(&size_p, &tipo, &status_p, disco, leido, name, &allocation, &fit);

    if (encontrado < 1) {
        printf("ERROR: No existe la particion.\n");
        printf("----------MONTAJE FALLIDO----------\n\n");
        fclose(disco);
        return;
    }

    fclose(disco);
    
    SB_S2 sb;
    int n;

    int a = path_e(montajes, path);
    char abecedario[26] = "abcdefghijklmnopqrstuvwxyz";
    if (a != -1) {
        if (name_e(montajes, a, name) != -1) {
            printf("ERROR: la particion ya se encuentra montada.\n");
            return;
        } else {
            for (int i = 0; i < 100; i++) {
                if (strlen(montajes[a].lista[i].nombre) == 0) {
                    char buffer[20];
                    sprintf(buffer, "%d", i + 1);
                    strcpy(montajes[a].path, path);
                    montajes[a].lista[i].letra = abecedario[a];
                    montajes[a].indice = abecedario[a];
                    montajes[a].lista[i].numero = i + 1;
                    strcpy(montajes[a].lista[i].nombre, "");
                    strcpy(montajes[a].lista[i].id, "");
                    strcpy(montajes[a].lista[i].nombre, name);
                    strcat(montajes[a].lista[i].id, "vd");
                    montajes[a].lista[i].id[2] = abecedario[a];
                    strcat(montajes[a].lista[i].id, buffer);
                    montajes[a].lista[i].comienzo = encontrado;
                    montajes[a].lista[i].size = size_p;
                    montajes[a].lista[i].status = status_p;
                    montajes[a].lista[i].tipo = tipo;
                    if (status_p == 'f') {
                        disco = fopen(path, "r+b");
                        fseek(disco,encontrado,SEEK_SET);
                        fread(&sb,sizeof(SB_S2),1,disco);
                        sb.sb_mount_time = time(NULL);
                        fseek(disco,encontrado,SEEK_SET);
                        fwrite(&sb,sizeof(SB_S2),1,disco);
                    }
                    printf("Particion Montada como %s\n", montajes[a].lista[i].id);
                    return;
                }
            }
        }
    } else {
        for (int i = 0; i < 25; i++) {
            if (strlen(montajes[i].path) == 0) {
                strcpy(montajes[i].path, path);
                montajes[i].lista[0].letra = abecedario[i];
                montajes[i].lista[0].numero = 1;
                strcpy(montajes[i].lista[0].nombre, "");
                strcpy(montajes[i].lista[0].id, "");
                strcpy(montajes[i].lista[0].nombre, name);
                strcat(montajes[i].lista[0].id, "vd");
                montajes[i].lista[0].id[2] = abecedario[i];
                montajes[i].indice = abecedario[i];
                montajes[i].lista[0].id[3] = montajes[i].lista[0].numero + '0';
                montajes[i].lista[0].comienzo = encontrado;
                montajes[i].lista[0].size = size_p;
                montajes[i].lista[0].status = status_p;
                montajes[i].lista[0].tipo = tipo;
                if (status_p == 'f') {
                    disco = fopen(path, "r+b");
                    fseek(disco,encontrado,SEEK_SET);
                    fread(&sb,sizeof(SB_S2),1,disco);
                    sb.sb_mount_time = time(NULL);
                    fseek(disco,encontrado,SEEK_SET);
                    fwrite(&sb,sizeof(SB_S2),1,disco);
                }
                printf("Particion Montada como %s\n", montajes[i].lista[0].id);
                return;
            }
        }
    }
    printf("Algo ocrurrio mal.\n");
}

void monstrar_montajes(Montaje *montajes){
    for (int j = 0; j < 26; j++) {
        if(strcmp(montajes[j].path,"")!=0){
            for (int i = 0; i < 100; i++) {
                if (strcmp(montajes[j].lista[i].id, "") != 0) {
                    printf("id->%s &páth->\"%s\" &name->\"%s\"\n",montajes[j].lista[i].id,montajes[j].path,montajes[j].lista[i].nombre);
                }
            }    
        }
    }
}

void umount_c(Montaje *mont, char *id) {
    int encontrado;
    int bandera = 1;
    for (int i = 0; i < 26; i++) {
        encontrado = id_e(mont, i, id);
        if (encontrado != -1) {
            strcpy(mont[i].lista[encontrado].nombre, "");
            bandera = 0;
            printf("id:\"%s\" Desmontado.\n", id);
            if (contar_montajes(mont, i) == 0) {
                strcpy(mont[i].path, "");
                return;
            }
            return;
        }
    }
    if (bandera) {
        printf("ERROR: no se ha encontrado un montaje como \"%s\".\n", id);
    }
}

int contar_montajes(Montaje *mont, int j) {
    int devolver = 0;
    for (int i = 0; i < 100; i++) {
        if (strlen(mont[j].lista[i].nombre) != 0) {
            devolver++;
        }
    }
    return devolver;
}

int path_e(Montaje *montajes, char *path) {
    for (int i = 0; i < 25; i++) {
        if (strcmp(montajes[i].path, path) == 0) {
            return i;
        }
    }
    return -1;
}

int id_e(Montaje *montajes, int j, char *id) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(montajes[j].lista[i].id, id) == 0) {
            return i;
        }
    }
    return -1;
}

int name_e(Montaje *montajes, int j, char *name) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(montajes[j].lista[i].nombre, name) == 0) {
            return i;
        }
    }
    return -1;
}

int encontrar_particion(int *size_p, char *tipo, char *status_p, FILE *disco, MBR mbr, char *name, char *allocation, char *fit) {

    if (mbr.mbr_partition_1.part_status != 'n')
        if (strcmp(mbr.mbr_partition_1.part_name, name) == 0) {
            *size_p = mbr.mbr_partition_1.part_size;
            *status_p = mbr.mbr_partition_1.part_status;
            *tipo = mbr.mbr_partition_1.part_type;
            return mbr.mbr_partition_1.part_start;
        }
    if (mbr.mbr_partition_2.part_status != 'n')
        if (strcmp(mbr.mbr_partition_2.part_name, name) == 0) {
            *size_p = mbr.mbr_partition_2.part_size;
            *status_p = mbr.mbr_partition_2.part_status;
            *tipo = mbr.mbr_partition_2.part_type;
            return mbr.mbr_partition_2.part_start;
        }

    if (mbr.mbr_partition_3.part_status != 'n')
        if (strcmp(mbr.mbr_partition_3.part_name, name) == 0) {
            *size_p = mbr.mbr_partition_3.part_size;
            *status_p = mbr.mbr_partition_3.part_status;
            *tipo = mbr.mbr_partition_3.part_type;
            return mbr.mbr_partition_3.part_start;
        }

    if (mbr.mbr_partition_4.part_status != 'n')
        if (strcmp(mbr.mbr_partition_4.part_name, name) == 0) {
            *size_p = mbr.mbr_partition_4.part_size;
            *status_p = mbr.mbr_partition_4.part_status;
            *tipo = mbr.mbr_partition_4.part_type;
            return mbr.mbr_partition_4.part_start;
        }

    int extendida = 0;

    if (mbr.mbr_partition_1.part_status != 'n')
        if (mbr.mbr_partition_1.part_type == 'e')
            extendida = 1;

    if (mbr.mbr_partition_2.part_status != 'n')
        if (mbr.mbr_partition_2.part_type == 'e')
            extendida = 2;

    if (mbr.mbr_partition_3.part_status != 'n')
        if (mbr.mbr_partition_3.part_type == 'e')
            extendida = 3;

    if (mbr.mbr_partition_4.part_status != 'n')
        if (mbr.mbr_partition_4.part_type == 'e')
            extendida = 4;

    if (extendida != 0) {
        EBR leido;

        switch (extendida) {
            case 1:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 2:
                fseek(disco, mbr.mbr_partition_2.part_start, SEEK_SET);
                break;
            case 3:
                fseek(disco, mbr.mbr_partition_3.part_start, SEEK_SET);
                break;
            case 4:
                fseek(disco, mbr.mbr_partition_4.part_start, SEEK_SET);
                break;
        }

        if (fread(&leido, sizeof (EBR), 1, disco) != 1) {
            printf("ERROR:al cargar la data del disco\n");
            fclose(disco);
            return-1;
        }

        while (leido.part_next != -1) {
            if (leido.part_status == 's' && strcmp(leido.part_name, name) == 0) {
                *size_p = leido.part_size;
                *status_p = leido.part_status;
                *tipo = 'l';
                return leido.part_start;
            }
            fseek(disco, leido.part_next, SEEK_SET);
            fread(&leido, sizeof (EBR), 1, disco);
        }

        if (leido.part_status == 's' && strcmp(leido.part_name, name) == 0) {
            *size_p = leido.part_size;
            *status_p = leido.part_status;
            *tipo = 'l';
            return leido.part_start;
        }
    }

    return 0;
}

int encontrar_id(Montaje *montajes, int j, char *id) {
    for (int i = 0; i < 100; i++) {
        if (strcmp(montajes[j].lista[i].id, id) == 0) {
            return i;
        }
    }
    return -1;
}