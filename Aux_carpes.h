
#ifndef AUX_CARPES_H
#define AUX_CARPES_H

void llenar_carpes(carpetas *carpes, char *cont);
void vaciar_carpes(carpetas *carpes, int numero);
int contar_carpes(char *path);

#endif /* AUX_CARPES_H */

