#ifndef PARTICION_H
#define PARTICION_H

void crear_pp(char *nombre, char *path, int tamano, int unit);
void crear_pl(char *nombre, char *path, int tamano, int unit);
void crear_pe(char *nombre, char *path, int tamano, int unit);
//falta eliminar el punto de montaje si la particion s encuentra montada...
void eliminar_particion(char *name, char *path, char tipo);
void modificar_tamano_particion(char *name, char*path, char unit, int add);

#endif /* PARTICION_H */

