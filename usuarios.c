#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "main_data.h"
#include "main_functions.h"
#include "usuarios.h"
#include "Aux_Archivo.h"
#include "Aux_Sesion.h"
#include "Aux_FS.h"
#include "Montaje.h"

void crear_usuario(Montaje *montaje, char *nombre, char *grupo, char *password, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(nombre) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(grupo) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(password) > 10) {
        printf("ERROR: el valor para el nombre de usuario es demaciado largo.(MAX 10)\n");
        return;
    }

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB_S2 sb;
    inodo_S2 ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");

    int comienzo = encontrado.comienzo;
    
    fseek(disco, comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);
    fseek(disco, sb.sb_ap_inodos + sb.sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb.sb_tamano_inodo, 1, disco);

    char contenido[ino.i_tam_archivo];
    clean_string(contenido, ino.i_tam_archivo);

    contenido_archivo(contenido, disco, sb, ino);

    int numero_registros = contar_saltos(contenido);

    Registro registros[numero_registros + 1];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    int bandera = 1;
    int bandera2 = 0;
    int bandera3 = 0;
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, nombre) == 0) {
                if (registros[i].id != 0) {
                    printf("ERROR: El Usuario \"%s\" ya Exite.\n",nombre);
                    fclose(disco);
                    return;
                } else {
                    printf("- Reactivando Usuario \"%s\" -\n", nombre);
                    encontrar_id_anterior(registros, &registros[i].id, numero_registros, nombre, 'u');
                    printf("- Reactivacion Completa -\n");
                    bandera = 0;
                }
            }
        }
    }
    
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'g') {
            if (strcmp(registros[i].grupo, grupo) == 0) {
                if(registros[i].id!=0){
                    bandera3 = 1;
                }
            }
        }
    }

    if (!bandera3) {
        printf("ERROR: el grupo \"%s\" no existe.\n",grupo);
        fclose(disco);
        return;
    }
    
    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'g') {
            if (strcmp(registros[i].grupo, grupo) == 0) {
                if (registros[i].id == 0) {
                    bandera2 = 1;
                }
            }
        }
    }

    if (bandera2) {
        printf("ERROR: el grupo especificado se encuentra actualmente eliminado.\n");
        fclose(disco);
        return;
    }



    if (bandera) {
        registros[numero_registros].id = contar_g_u(registros, numero_registros, 'u') + 1;
        strcpy(registros[numero_registros].grupo, grupo);
        registros[numero_registros].tipo = 'u';
        strcpy(registros[numero_registros].username, nombre);
        strcpy(registros[numero_registros].password, password);
    }
    int conte = lon_contenido(registros, numero_registros);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros, nuevo_contenido, conte);

    editar_archivo(disco, &sb, 1, nuevo_contenido, "users.txt");
    insert_SB_S2(disco, &sb, comienzo);

    fclose(disco);
    printf("Creacion Completa.\n");
}

void eliminar_usuario(Montaje *montaje, char *nombre, char *id) {
    char path_disco[100];
    ids encontrado;
    int a;

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: la articion non ha sido formateada.\n");
        return;
    }

    SB_S2 sb;
    inodo_S2 ino;
    FILE *disco;

    disco = fopen(path_disco, "r+b");
    
    
    int comienzo = encontrado.comienzo;

    fseek(disco, comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);
    fseek(disco, sb.sb_ap_inodos + sb.sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb.sb_tamano_inodo, 1, disco);

    char contenido[ino.i_tam_archivo];
    clean_string(contenido,ino.i_tam_archivo);
    contenido_archivo(contenido, disco, sb, ino);
    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);

    int bandera = 1;

    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, nombre) == 0) {
                if (registros[i].id != 0) {
                    eliminar_id(registros, &registros[i].id, numero_registros, nombre, 'u');
                    bandera = 0;
                } else {
                    printf("ERROR: El Usuario ya se ha eliminado.\n");
                    fclose(disco);
                    return;
                }
            }
        }
    }

    if (bandera) {
        printf("ERROR: no se ha encontrado ningun usuario como  \"%s\"\n", nombre);
        fclose(disco);
        return;
    }

    int conte = lon_contenido(registros, numero_registros - 1);
    char nuevo_contenido[conte];
    rtc(registros, numero_registros - 1, nuevo_contenido, conte);

    editar_archivo(disco, &sb, 1, nuevo_contenido, "users.txt");
    insert_SB_S2(disco, &sb, comienzo);

    fclose(disco);
    printf("Eliminacion Completa.\n");

}