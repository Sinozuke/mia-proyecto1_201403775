#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Permisos.h"
#include "Aux_FS.h"
#include "Montaje.h"
#include "Aux_Archivo.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"
#include "Sesion.h"
#include "Aux_Sesion.h"

void chmod_r(FILE *disco, SB_S2 *sb, Sesion *sesion, int inodo, char permisos[]);

void cambiar_permisos(Montaje *montaje, Sesion *sesion, char id[], char path[], char permisos[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 dir_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&dir_padre, sb.sb_tamano_inodo, 1, disco);

    int existe1 = dir_exist(disco, &sb, dir_padre, carpes[numero - 1].bname, 'a');
    int existe2 = dir_exist(disco, &sb, dir_padre, carpes[numero - 1].bname, 'c');

    if (existe1 == -1 && existe2 == -1) {
        printf("ERROR: NO EXISTE UN ELEMENTO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    if (existe1 != -1 && existe2 == -1) {
        chmod_r(disco, &sb, sesion, existe1, permisos);
    } else if (existe1 == -1 && existe2 != -1) {
        chmod_r(disco, &sb, sesion, existe2, permisos);
    } else {
        printf("CAGADA...\n");
        fclose(disco);
        return;
    }


    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

}

void chmod_r0(FILE *disco, SB_S2 *sb, Sesion *sesion, int pos_bc, char permisos[]) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        chmod_r(disco, sb, sesion, bc.avd_bloque[i], permisos);
    }

}

void chmod_r1(FILE *disco, SB_S2 *sb, Sesion *sesion, int pos_ba, char permisos[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        chmod_r0(disco, sb, sesion, ba.apuntador[i], permisos);
    }

}

void chmod_r2(FILE *disco, SB_S2 *sb, Sesion *sesion, int pos_ba, char permisos[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        chmod_r1(disco, sb, sesion, ba.apuntador[i], permisos);
    }

}

void chmod_r(FILE *disco, SB_S2 *sb, Sesion *sesion, int inodo, char permisos[]) {

    if (inodo == -1) {
        return;
    }

    inodo_S2 modificar;
    char old_permisos[3];
    int bandera=1;
    
    fseek(disco, sb->sb_ap_inodos + inodo * sb->sb_tamano_inodo, SEEK_SET);
    fread(&modificar, sb->sb_tamano_inodo, 1, disco);

    if(sesion->UID==modificar.i_idPropetario || sesion->UID==ROOT_ID){
        strncpy(old_permisos, modificar.i_idPermisos, 3);
        strncpy(modificar.i_idPermisos, permisos, 3);
    }else{
        bandera=0;
    }
    
    if (modificar.i_tipo == 'c') {
        for (int i = 0; i < MAX_DIREC_APUNT; i++) {
            chmod_r0(disco, sb, sesion, modificar.i_bloque[i], permisos);
        }
        chmod_r1(disco, sb, sesion, modificar.i_indirecto[0], permisos);
        chmod_r2(disco, sb, sesion, modificar.i_indirecto[1], permisos);
    }

    insert_inodo(sb, disco, &modificar, modificar.i_llave);

    nueva_bitacora('2', modificar.i_tipo, modificar.i_name, *sb, disco);

    if(bandera){
        printf("CAMBIO DE PERMISOS DEL ELEMENTO \"%s\", DE %.3s A %.3s EXITOSO.\n", modificar.i_name, old_permisos, permisos);
    }else{
        printf("ERROR: USTED NO ES EL PROPETARIO DEL ELEMENTO \"%s\", CAMBIO DENGADO.\n", modificar.i_name);
    }
}

void cambiar_propetario(Montaje *montaje, Sesion *sesion, char id[], char path[], char username[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }
    inodo_S2 users;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fseek(disco, sb.sb_ap_inodos + sb.sb_tamano_inodo, SEEK_SET);
    fread(&users, sizeof (inodo_S2), 1, disco);

    char contenido[users.i_tam_archivo];

    clean_string(contenido, users.i_tam_archivo);

    contenido_archivo(contenido, disco, sb, users);

    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);

    int nuevo_propetario = -1;

    int bandera = 1;

    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, username) == 0) {
                if (registros[i].id != 0) {
                    nuevo_propetario = registros[i].id;
                    bandera = 0;
                }
            }
        }
    }

    if (bandera) {
        printf("--No Hay Ningun usuario como \"%s\"\n", username);
        fclose(disco);
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 dir_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&dir_padre, sb.sb_tamano_inodo, 1, disco);

    int existe1 = dir_exist(disco, &sb, dir_padre, carpes[numero - 1].bname, 'a');
    int existe2 = dir_exist(disco, &sb, dir_padre, carpes[numero - 1].bname, 'c');

    if (existe1 == -1 && existe2 == -1) {
        printf("ERROR: NO EXISTE UN ELEMENTO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    inodo_S2 modificar;

    if (existe1 != -1 && existe2 == -1) {
        fseek(disco, sb.sb_ap_inodos + existe1 * sb.sb_tamano_inodo, SEEK_SET);
    } else if (existe1 == -1 && existe2 != -1) {
        fseek(disco, sb.sb_ap_inodos + existe2 * sb.sb_tamano_inodo, SEEK_SET);
    } else {
        printf("CAGADA...\n");
        fclose(disco);
        return;
    }

    fread(&modificar, sb.sb_tamano_inodo, 1, disco);
    
    if(modificar.i_idPropetario!=sesion->UID && sesion->UID!=ROOT_ID){
        printf("ERROR: NO TIENE PERMISOS PARA REALIZAR ESTA ACCION SOBRE ESTE ELEMENTO.\n");
        fclose(disco);
        return;
    }    

    modificar.i_idPropetario = nuevo_propetario;

    insert_inodo(&sb, disco, &modificar, modificar.i_llave);

    nueva_bitacora('2', modificar.i_tipo, modificar.i_name, sb, disco);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CAMBIO DE PROPETARIO DEL ELEMENTO \"%s\" - \"%s\" EXITOSO.\n", modificar.i_name, carpeta_a_crear);

}
