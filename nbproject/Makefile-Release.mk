#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Archivo.o \
	${OBJECTDIR}/Aux_Archivo.o \
	${OBJECTDIR}/Aux_Directorio.o \
	${OBJECTDIR}/Aux_FS.o \
	${OBJECTDIR}/Aux_FS_OP.o \
	${OBJECTDIR}/Aux_Sesion.o \
	${OBJECTDIR}/Aux_carpes.o \
	${OBJECTDIR}/Completo_sistema.o \
	${OBJECTDIR}/Contenido_directorio.o \
	${OBJECTDIR}/Directorio.o \
	${OBJECTDIR}/Disco.o \
	${OBJECTDIR}/FS_OP.o \
	${OBJECTDIR}/Grafico_Directorio.o \
	${OBJECTDIR}/Grafico_archivo.o \
	${OBJECTDIR}/Journaling.o \
	${OBJECTDIR}/Menu_parameters.o \
	${OBJECTDIR}/Montaje.o \
	${OBJECTDIR}/Particion.o \
	${OBJECTDIR}/Permisos.o \
	${OBJECTDIR}/Sesion.o \
	${OBJECTDIR}/block.o \
	${OBJECTDIR}/bm_block.o \
	${OBJECTDIR}/bm_inodo.o \
	${OBJECTDIR}/formato.o \
	${OBJECTDIR}/grupos.o \
	${OBJECTDIR}/inodo.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/main_functions.o \
	${OBJECTDIR}/primeros.o \
	${OBJECTDIR}/sb.o \
	${OBJECTDIR}/usuarios.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia-proyecto1_201403775

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia-proyecto1_201403775: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia-proyecto1_201403775 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Archivo.o: Archivo.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Archivo.o Archivo.c

${OBJECTDIR}/Aux_Archivo.o: Aux_Archivo.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_Archivo.o Aux_Archivo.c

${OBJECTDIR}/Aux_Directorio.o: Aux_Directorio.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_Directorio.o Aux_Directorio.c

${OBJECTDIR}/Aux_FS.o: Aux_FS.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_FS.o Aux_FS.c

${OBJECTDIR}/Aux_FS_OP.o: Aux_FS_OP.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_FS_OP.o Aux_FS_OP.c

${OBJECTDIR}/Aux_Sesion.o: Aux_Sesion.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_Sesion.o Aux_Sesion.c

${OBJECTDIR}/Aux_carpes.o: Aux_carpes.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Aux_carpes.o Aux_carpes.c

${OBJECTDIR}/Completo_sistema.o: Completo_sistema.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Completo_sistema.o Completo_sistema.c

${OBJECTDIR}/Contenido_directorio.o: Contenido_directorio.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Contenido_directorio.o Contenido_directorio.c

${OBJECTDIR}/Directorio.o: Directorio.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Directorio.o Directorio.c

${OBJECTDIR}/Disco.o: Disco.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Disco.o Disco.c

${OBJECTDIR}/FS_OP.o: FS_OP.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FS_OP.o FS_OP.c

${OBJECTDIR}/Grafico_Directorio.o: Grafico_Directorio.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Grafico_Directorio.o Grafico_Directorio.c

${OBJECTDIR}/Grafico_archivo.o: Grafico_archivo.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Grafico_archivo.o Grafico_archivo.c

${OBJECTDIR}/Journaling.o: Journaling.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Journaling.o Journaling.c

${OBJECTDIR}/Menu_parameters.o: Menu_parameters.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Menu_parameters.o Menu_parameters.c

${OBJECTDIR}/Montaje.o: Montaje.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Montaje.o Montaje.c

${OBJECTDIR}/Particion.o: Particion.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Particion.o Particion.c

${OBJECTDIR}/Permisos.o: Permisos.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Permisos.o Permisos.c

${OBJECTDIR}/Sesion.o: Sesion.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Sesion.o Sesion.c

${OBJECTDIR}/block.o: block.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/block.o block.c

${OBJECTDIR}/bm_block.o: bm_block.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bm_block.o bm_block.c

${OBJECTDIR}/bm_inodo.o: bm_inodo.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/bm_inodo.o bm_inodo.c

${OBJECTDIR}/formato.o: formato.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/formato.o formato.c

${OBJECTDIR}/grupos.o: grupos.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/grupos.o grupos.c

${OBJECTDIR}/inodo.o: inodo.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/inodo.o inodo.c

${OBJECTDIR}/main.o: main.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.c

${OBJECTDIR}/main_functions.o: main_functions.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_functions.o main_functions.c

${OBJECTDIR}/primeros.o: primeros.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/primeros.o primeros.c

${OBJECTDIR}/sb.o: sb.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/sb.o sb.c

${OBJECTDIR}/usuarios.o: usuarios.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/usuarios.o usuarios.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia-proyecto1_201403775

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
