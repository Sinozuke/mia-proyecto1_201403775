#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_journaling(Montaje *montaje, char *id, char *path) {

    char directorio[150];
    strcpy(directorio, path);
    if (!strstr(directorio, ".txt")) {
        strcat(directorio, ".txt");
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.tipo == 'e') {
        printf("ERROR: La Particion Mopntada es de Tipo Extendida, solo puede ser te tipo Logica o Primaria para generar el reporte.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;

    disco = fopen(path_disco, "r+b");
    reporte = fopen(directorio, "w");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        fclose(reporte);
        return;
    }

    if (!reporte) {
        printf("ERROR: el Archivo de reporte no ha podido Abrirse.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        fclose(reporte);
        return;
    }
    
    switch (encontrado.status) {
        case 'f':
            fseek(disco, encontrado.comienzo, SEEK_SET);
            break;
        default:
            printf("ERROR: La Particion Montada No esta formateada o no tiene un sistema de archivos valido.\n");
            printf("-----------REPORTE FALLIDO-----------\n\n");
            fclose(disco);
            return;
    }

    char comando_abertura[100] = "gnome-open ";

    strcat(comando_abertura, directorio);

    SB_S2 sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);
    fseek(disco,sb.sb_ap_ficheros+sb.sb_tamano_bloque*sb.sb_numero_bloques,SEEK_SET);

    Log_S2 bitacora;
    int exit=0;
    char buffer[20];

    for (int i = 0; i < sb.sb_ap_inodos * NUMBER_LOGS_FS && exit!=1; i++) {
        fread(&bitacora, sizeof (Log_S2), 1, disco);
        if (bitacora.log_tipo_operacion != '0') {
            clean_string(buffer,20);
            strftime(buffer, 20, "%x - %I:%M%p", localtime(&bitacora.Journal_fecha));
            fprintf(reporte,"[%s]",buffer);
            fprintf(reporte,"[");
            switch(bitacora.log_tipo_operacion){
                case '1':
                    fprintf(reporte,"Creacion ");
                    break;
                case '2':
                    fprintf(reporte,"Modificacion ");
                    break;
                case '3':
                    fprintf(reporte,"Eliminacion ");
                    break;
                case '4':
                    fprintf(reporte,"Copia ");
                    break;
                case '5':
                    fprintf(reporte,"Cambio de lugar(Mover) ");
                    break;
            }
            switch(bitacora.log_tipo){
                case 'a':
                    fprintf(reporte,"del archivo \"%s\"]\n",bitacora.log_nombre);
                    break;
                case 'c':
                    fprintf(reporte,"de la carpeta \"%s\"]\n",bitacora.log_nombre);
                    break;
            }
        }else{
            exit=1;
        }
    }

    fclose(reporte);
    fclose(disco);

    system(comando_abertura);
    printf("-- REPORTE JOURNALING GENERADO --\n");
    sleep(SLEEP_TIME);
}