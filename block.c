#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_block(Montaje *montaje, char *id, char *path) {

    char directorio[200];
    
    for(int i=0;i<200;i++){
        directorio[i]='\0';
    }
    
    
    strncpy(directorio, path, strlen(path));

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }
    
    if (!strstr(directorio, ".svg")) {
        strcat(directorio, ".svg");
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" BLOCK.dot", 11);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");
    reporte = fopen("BLOCK.dot", "w");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    SB_S2 sb;
    BD_S2 bloqueD;
    BC_S2 bloqueC;
    BA_S2 bloqueA;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fseek(disco, sb.sb_ap_bitmap_ficheros, SEEK_SET);
    char leido = '0';
    
    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "node [shape=plaintext];\n");
    fprintf(reporte, "rankdir = LR;\n");

    for (int i = 0; i < sb.sb_numero_bloques; i++) {
        fseek(disco, sb.sb_ap_bitmap_ficheros + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fprintf(reporte, "\"%i\" [label=<\n", i+1);
            fseek(disco,sb.sb_ap_ficheros+i*sb.sb_tamano_bloque,SEEK_SET);
            switch(leido){
                case 'a':
                    fread(&bloqueA,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BA_COLOR);
                    break;
                case 'c':
                    fread(&bloqueC,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BC_COLOR);
                    break;
                case 'd':
                    fread(&bloqueD,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BD_COLOR);
                    break;
            }
            fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Bloque No.%i</TD>\n</TR>\n", i+1);
            switch(leido){
                case 'a':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Apuntador</TD>\n</TR>\n");
                    for(int j=0;j<MAX_BD_APUNTS;j++)
                        fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD>%i</TD>\n</TR>\n",j+1,bloqueA.apuntador[j]);
                    break;
                case 'c':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Carpeta</TD>\n</TR>\n");
                    fprintf(reporte, "<TR>\n<TD>Nombre</TD><TD>%.24s</TD>\n</TR>\n",bloqueC.avd_nombre_directorio);
                    fprintf(reporte, "<TR>\n<TD>Padre</TD><TD>%.24s</TD>\n</TR>\n",bloqueC.avd_padre);
                    for(int j=0;j<MAX_AVD_DIREC_APUNT;j++)
                        fprintf(reporte, "<TR>\n<TD>hijo_%i</TD><TD>%i</TD>\n</TR>\n",j+1,bloqueC.avd_bloque[j]);
                    break;
                case 'd':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Dato</TD>\n</TR>\n");
                    fprintf(reporte, "<TR>\n<TD>Nombre</TD><TD>%.24s</TD>\n</TR>\n",bloqueD.db_nombre);
                    fprintf(reporte, "<TR>\n<TD>Contenido</TD><TD>");                    
                    for(int j=0;j<MAX_BD_CONT_SIZE && bloqueD.db_data[j]!='\0';j++){
                        if(bloqueD.db_data[j]=='\n'){
                            fprintf(reporte, "\\n");                    
                        }else{
                            fprintf(reporte, "%c",bloqueD.db_data[j]);                    
                        }
                    }
                    fprintf(reporte, "</TD>\n</TR>\n");                    
                    break;
            }
            fprintf(reporte, "</TABLE>>];\n");
        }
    }

    int anterior = 0;
    int actual = 0;
    fseek(disco, sb.sb_ap_bitmap_ficheros, SEEK_SET);
    fread(&leido, sizeof (char), 1, disco);
    if (leido != '0') {
        fprintf(reporte, "\"1\"");
        for (int i = 1; i < sb.sb_numero_inodos; i++) {
            fread(&leido, sizeof (char), 1, disco);
            if (leido != '0') {
                fprintf(reporte, "->\"%i\"",i+1);
            }
        }
    }
    fprintf(reporte,";\n");
    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);

    printf("-- REPORTE BLOCK GENERADO --\n");
    sleep(SLEEP_TIME);
}
