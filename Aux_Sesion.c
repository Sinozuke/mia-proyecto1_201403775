#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_Sesion.h"

int contar_saltos(char *cont) {
    int resultado = 0;

    for (int i = 0; cont[i] != '\0'; i++)
        if (cont[i] == '\n')
            resultado++;

    return resultado;
}

void vaciar(Registro *registro, int numero) {
    for (int i = 0; i < numero; i++) {
        for (int j = 0; j < 10; j++) {
            registro[i].grupo[j] = '\0';
            registro[i].username[j] = '\0';
            registro[i].password[j] = '\0';
        }
        registro[i].id = -1;
        registro[i].tipo = '-';
    }

}

void almacenar_registros(Registro *regis, char *cont) {
    char copia[strlen(cont)];
    clean_string(copia,strlen(cont));
    strncpy(copia, cont,strlen(cont));
    char *token;
    char registro[40];
    clean_string(registro,40);
    int conteo = 0, conteo2 = 0;
    int j = 0;
    for (int i = 0; copia[i] != '\0'; i++) {
        if (copia[i] != '\n') {
            registro[j] = copia[i];
            j++;
        } else {
            token = strtok(registro, ",");
            while (token) {
                switch (conteo) {
                    case 0:
                        regis[conteo2].id = atoi(token);
                        break;
                    case 1:
                        regis[conteo2].tipo = token[0];
                        break;
                    case 2:
                        strcpy(regis[conteo2].grupo, token);
                        break;
                    case 3:
                        strcpy(regis[conteo2].username, token);
                        break;
                    case 4:
                        strcpy(regis[conteo2].password, token);
                        break;
                }
                conteo++;
                token = strtok(NULL, ",");
            }
            clean_string(registro,40);
            conteo = 0;
            conteo2++;
            j = 0;
        }
    }
}

void encontrar_id_anterior(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo) {
    int numero = 0;
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo) {
            numero++;
            if(tipo=='u'){
                if (strcmp(registros[i].username, nombre) == 0) {
                    *retorno = numero;
                    return;
                }        
            }else{
                if (strcmp(registros[i].grupo, nombre) == 0) {
                    *retorno = numero;
                    return;
                }
            }
        }
    }
}

void eliminar_id(Registro *registros, int *retorno, int cantidad, char *nombre,char tipo){
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo) {
            if(tipo=='u'){
                if (strcmp(registros[i].username, nombre) == 0) {
                    *retorno = 0;
                    return;
                }
            }else{
                if (strcmp(registros[i].grupo, nombre) == 0) {
                    *retorno = 0;
                    return;
                }
            }
        }
    }
}

void rtc(Registro *registro, int cantidad, char *dest, int longitud) {
    clean_string(dest,longitud);
    for (int i = 0; i <= cantidad; i++) {
        if (registro[i].tipo == 'u' || registro[i].tipo == 'g') {
            char buffer[20];
            sprintf(buffer, "%d", registro[i].id);
            strcat(dest, buffer);
            strcat(dest, ",");
            dest[strlen(dest)] = registro[i].tipo;
            strcat(dest, ",");
            strcat(dest, registro[i].grupo);

            if (registro[i].tipo == 'u') {
                strcat(dest, ",");
                strcat(dest, registro[i].username);
                strcat(dest, ",");
                strcat(dest, registro[i].password);
            }
            strcat(dest, "\n");
        }
    }
}

int contar_g_u(Registro *registros, int cantidad, char tipo) {
    int devolver = 0;
    for (int i = 0; i < cantidad; i++) {
        if (registros[i].tipo == tipo)
            devolver++;
    }
    return devolver;
}

int lon_contenido(Registro *registro, int cantidad) {
    int devolver = 0;

    for (int i = 0; i <= cantidad; i++) {
        if (registro[i].tipo != '-') {
            char buffer[20];
            sprintf(buffer, "%d", registro[i].id);
            devolver += strlen(buffer);
            devolver += sizeof (char);
            devolver += sizeof (char);
            devolver += sizeof (char);
            devolver += strlen(registro[i].grupo);

            if (registro[i].tipo == 'u') {
                devolver += sizeof (char);
                devolver += strlen(registro[i].password);
                devolver += sizeof (char);
                devolver += strlen(registro[i].username);
            }
            devolver += sizeof (char);
        }
    }

    return devolver;
}