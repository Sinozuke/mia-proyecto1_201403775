#ifndef AUX_DIRECTORIO_H
#define AUX_DIRECTORIO_H

void enlazar_inodos(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int hijo);
void desenlazar_inodos(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int hijo);
int buscar_dir_padre(FILE *disco, SB_S2 *sb, carpetas carpes[], int total);
int dir_exist(FILE *disco, SB_S2 *sb, inodo_S2 padre, char nombre[], char tipo);
void eliminar_recursivamente(FILE *disco, SB_S2 *sb, int pos_ino);
void update_directorio(FILE *disco, SB_S2 *sb, int posicion_inodo, char name[]);

#endif /* AUX_DIRECTORIO_H */

