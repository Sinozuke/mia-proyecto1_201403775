#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void escribir_directorio_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_bc) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] == -1) {
            fprintf(reporte, "\t\t<TD BGCOLOR=\"#b3daff\">%i</TD>\n", bc.avd_bloque[i]);
        } else {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                fprintf(reporte, "\t\t<TD BGCOLOR=\"#b3daff\" PORT=\"f%i\">%i</TD>\n", bc.avd_bloque[i], bc.avd_bloque[i]);
            }
        }
    }


}

void escribir_directorio_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        escribir_directorio_lvl0(disco, reporte, sb, ba.apuntador[i]);
    }


}

void escribir_directorio_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        escribir_directorio_lvl1(disco, reporte, sb, ba.apuntador[i]);
    }

}

void contar_directorio_lvl0(FILE *disco, SB_S2 *sb, int pos_bc,int *total) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            *total=(*total)+1;
        }
    }


}

void contar_directorio_lvl1(FILE *disco, SB_S2 *sb, int pos_ba,int *total) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contar_directorio_lvl0(disco, sb, ba.apuntador[i],total);
    }


}

void contar_directorio_lvl2(FILE *disco, SB_S2 *sb, int pos_ba,int *total) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contar_directorio_lvl1(disco, sb, ba.apuntador[i],total);
    }

}

void contar_directorio(FILE *disco, SB_S2 *sb,inodo_S2 *ino,int *total) {

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        if (ino->i_bloque[i] != -1) {
            contar_directorio_lvl0(disco,sb,ino->i_bloque[i],total);
        }
    }
    contar_directorio_lvl1(disco,sb,ino->i_indirecto[0],total);
    contar_directorio_lvl2(disco,sb,ino->i_indirecto[1],total);
    
}

void escribir_directorios(FILE *disco, FILE *reporte, SB_S2 *sb) {

    char bloque = '\0';
    inodo_S2 ino;
    int total = 0;

    for (int i = 0; i < sb->sb_numero_inodos; i++,total=0) {
        fseek(disco, sb->sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&bloque, sizeof (char), 1, disco);
        if (bloque != '0') {
            fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                fprintf(reporte, "\"Directorio%i\" [label=<\n", i);
                fprintf(reporte, "<TABLE CELLSPACING=\"0\" BGCOLOR=\"%s\">\n",BC_COLOR);
                fprintf(reporte, "\t<TR>\n");
                contar_directorio(disco, sb,&ino,&total);
                if(total==0){
                    fprintf(reporte, "\t\t<TD COLSPAN=\"6\" WIDTH=\"50\" HEIGHT=\"50\">\n");            
                }else{
                    fprintf(reporte, "\t\t<TD COLSPAN=\"%i\" WIDTH=\"50\" HEIGHT=\"50\">\n", total);
                }
                fprintf(reporte, "\t\t\t<FONT POINT-SIZE=\"50\">%s</FONT>\n", ino.i_name);
                fprintf(reporte, "\t\t</TD>\n");
                fprintf(reporte, "\t</TR>\n");
                fprintf(reporte, "\t<TR>\n");
                for (int j = 0; j < MAX_DIREC_APUNT; j++) {
                    escribir_directorio_lvl0(disco, reporte, sb, ino.i_bloque[j]);
                }
                escribir_directorio_lvl1(disco, reporte, sb, ino.i_indirecto[0]);
                escribir_directorio_lvl2(disco, reporte, sb, ino.i_indirecto[1]);

                fprintf(reporte, "\t</TR>\n");
                fprintf(reporte, "</TABLE>>];\n");
            }
        }
    }

}

void enlazar_directorio_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_bc, int padre) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                fprintf(reporte, "\"Directorio%i\":f%i->\"Directorio%i\";\n", padre, bc.avd_bloque[i], bc.avd_bloque[i]);
            }
        }
    }


}

void enlazar_directorio_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba, int padre) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_directorio_lvl0(disco, reporte, sb, ba.apuntador[i], padre);
    }


}

void enlazar_directorio_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba, int padre) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_directorio_lvl1(disco, reporte, sb, ba.apuntador[i], padre);
    }

}

void enlazar_directorios(FILE *disco, FILE *reporte, SB_S2 *sb) {

    char leido = '0';
    inodo_S2 ino;

    for (int i = 0; i < sb->sb_numero_inodos; i++) {
        fseek(disco, sb->sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                for (int j = 0; j < MAX_DIREC_APUNT; j++) {
                    enlazar_directorio_lvl0(disco, reporte, sb, ino.i_bloque[j], ino.i_llave);
                }
                enlazar_directorio_lvl1(disco, reporte, sb, ino.i_indirecto[0], ino.i_llave);
                enlazar_directorio_lvl2(disco, reporte, sb, ino.i_indirecto[1], ino.i_llave);
            }
        }
    }
}

void ordernar_directorio_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_bc) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino,sb->sb_tamano_inodo,1,disco);
            if(ino.i_tipo=='c'){
                fprintf(reporte, "\"Directorio%i\"\n", bc.avd_bloque[i]);
            }
        }
    }


}

void ordernar_directorio_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        ordernar_directorio_lvl0(disco, reporte, sb, ba.apuntador[i]);
    }


}

void ordernar_directorio_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        ordernar_directorio_lvl1(disco, reporte, sb, ba.apuntador[i]);
    }

}

void ordernar_directorios(FILE *disco, FILE *reporte, SB_S2 *sb) {

    char leido = '0';
    inodo_S2 ino;

    fprintf(reporte, "{rank=same\n\"Directorio0\"\n}\n");


    for (int i = 0; i < sb->sb_numero_inodos; i++) {
        fseek(disco, sb->sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                fprintf(reporte, "{rank=same\n");
                for (int j = 0; j < MAX_DIREC_APUNT; j++) {
                    ordernar_directorio_lvl0(disco, reporte, sb, ino.i_bloque[j]);
                }
                ordernar_directorio_lvl1(disco, reporte, sb, ino.i_indirecto[0]);
                ordernar_directorio_lvl2(disco, reporte, sb, ino.i_indirecto[1]);
                fprintf(reporte, "}\n");
            }
        }
    }
}

void rep_Directorio(Montaje *montaje, char *id, char *path) {

    char directorio[300];
    for (int i = 0; i < 300; i++) {
        directorio[i] = '\0';
    }
    strncpy(directorio, path, strlen(path));

    if (!strstr(directorio, ".svg")) {
        strcat(directorio, ".svg");
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strncpy(path_disco, montaje[id[2] - 97].path, strlen(montaje[id[2] - 97].path));

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: La Particion Montada No esta formateada con un sistema de archivos valido.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" DIR1.dot", 10);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");
    
    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    
    reporte = fopen("DIR1.dot", "w");

    if (!reporte) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    SB_S2 sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");

    escribir_directorios(disco, reporte, &sb);
    ordernar_directorios(disco, reporte, &sb);
    enlazar_directorios(disco, reporte, &sb);

    fprintf(reporte, "}");


    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);
    printf("-- REPORTE DIR1 GENERADO --\n");
    sleep(SLEEP_TIME);

}