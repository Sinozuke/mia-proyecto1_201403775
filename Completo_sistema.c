#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_Sistema(Montaje *montaje, char *id, char *path) {

    char directorio[200];

    for (int i = 0; i < 200; i++) {
        directorio[i] = '\0';
    }


    strncpy(directorio, path, strlen(path));

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    if (!strstr(directorio, ".svg")) {
        strncat(directorio, ".svg",4);
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" COMPLETE.dot", 14);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");
    reporte = fopen("COMPLETE.dot", "w");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    SB_S2 sb;
    inodo_S2 inode;
    BD_S2 bloqueD;
    BC_S2 bloqueC;
    BA_S2 bloqueA;
    char leido = '0';

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "node [shape=plaintext];\n");
    
    for (int i = 0; i < sb.sb_numero_bloques; i++) {
        fseek(disco, sb.sb_ap_bitmap_ficheros + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fprintf(reporte, "\"B%i\" [label=<\n", i);
            fseek(disco,sb.sb_ap_ficheros+i*sb.sb_tamano_bloque,SEEK_SET);
            switch(leido){
                case 'a':
                    fread(&bloqueA,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE  BGCOLOR=\"%s\">\n",BA_COLOR);
                    break;
                case 'c':
                    fread(&bloqueC,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE  BGCOLOR=\"%s\">\n",BC_COLOR);
                    break;
                case 'd':
                    fread(&bloqueD,sb.sb_tamano_bloque,1,disco);
                    fprintf(reporte, "<TABLE  BGCOLOR=\"%s\">\n",BD_COLOR);
                    break;
            }
            fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Bloque No.%i</TD>\n</TR>\n", i+1);
            switch(leido){
                case 'a':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Apuntador</TD>\n</TR>\n");
                    for(int j=0;j<MAX_BD_APUNTS;j++){
                        if(bloqueA.apuntador[j]!=-1){
                            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n",j+1,bloqueA.apuntador[j],bloqueA.apuntador[j]);
                        }else{
                            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD>%i</TD>\n</TR>\n",j+1,bloqueA.apuntador[j]);
                        }
                    }
                    break;
                case 'c':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Carpeta</TD>\n</TR>\n");
                    fprintf(reporte, "<TR>\n<TD>Nombre</TD><TD>%.24s</TD>\n</TR>\n",bloqueC.avd_nombre_directorio);
                    fprintf(reporte, "<TR>\n<TD>Padre</TD><TD>%.24s</TD>\n</TR>\n",bloqueC.avd_padre);
                    for(int j=0;j<MAX_AVD_DIREC_APUNT;j++){
                        if(bloqueC.avd_bloque[j]!=-1){
                            fprintf(reporte, "<TR>\n<TD>hijo_%i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n",j+1,bloqueC.avd_bloque[j],bloqueC.avd_bloque[j]);
                        }else{
                            fprintf(reporte, "<TR>\n<TD>hijo_%i</TD><TD>%i</TD>\n</TR>\n",j+1,bloqueC.avd_bloque[j]);
                        }
                    }
                    break;
                case 'd':
                    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Dato</TD>\n</TR>\n");
                    fprintf(reporte, "<TR>\n<TD>Nombre</TD><TD>%.24s</TD>\n</TR>\n",bloqueD.db_nombre);
                    fprintf(reporte, "<TR>\n<TD>Contenido</TD><TD>");                    
                    for(int j=0;j<MAX_BD_CONT_SIZE && bloqueD.db_data[j]!='\0';j++){
                        if(bloqueD.db_data[j]=='\n'){
                            fprintf(reporte, "\\n");                    
                        }else{
                            fprintf(reporte, "%c",bloqueD.db_data[j]);                    
                        }
                    }
                    fprintf(reporte, "</TD>\n</TR>\n");                    
                    break;
            }
            fprintf(reporte, "</TABLE>>];\n");
            
        }
    }
    
    for (int i = 0; i < sb.sb_numero_inodos; i++) {
        fseek(disco, sb.sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco, sb.sb_ap_inodos + i * sb.sb_tamano_inodo, SEEK_SET);
            fread(&inode, sb.sb_tamano_inodo, 1, disco);
            fprintf(reporte, "\"I%i\" [label=<\n", i);
            fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",INODE_COLOR);
            fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Inodo No.%i</TD>\n</TR>\n", i + 1);
            switch (inode.i_tipo) {
                case 'c':
                    fprintf(reporte, "<TR>\n<TD>i_type</TD><TD>Carpeta</TD>\n</TR>\n");
                    break;
                case 'a':
                    fprintf(reporte, "<TR>\n<TD>i_type</TD><TD>Archivo</TD>\n</TR>\n");
                    break;
            }
            fprintf(reporte, "<TR>\n<TD>i_nombre</TD><TD>%.24s</TD>\n</TR>\n", inode.i_name);
            fprintf(reporte, "<TR>\n<TD>i_llave</TD><TD>%i</TD>\n</TR>\n", i + 1);
            fprintf(reporte, "<TR>\n<TD>i_tam_archivo</TD><TD>%i</TD>\n</TR>\n", inode.i_tam_archivo);
            fprintf(reporte, "<TR>\n<TD>i_asig_bloques</TD><TD>%i</TD>\n</TR>\n", inode.i_asig_bloques);
            for (int j = 0; j < MAX_DIREC_APUNT; j++){
                if(inode.i_bloque[j]!=-1){
                    fprintf(reporte, "<TR>\n<TD>i_bloque_%i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n", j + 1, inode.i_bloque[j], inode.i_bloque[j]);
                }else{
                    fprintf(reporte, "<TR>\n<TD>i_bloque_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, inode.i_bloque[j]);
                }
            }
            for (int j = 0; j < MAX_INDIREC_APUNT; j++){
                if(inode.i_indirecto[j]!=-1){
                    fprintf(reporte, "<TR>\n<TD>i_indirecto_%i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n", j + 1, inode.i_indirecto[j], inode.i_indirecto[j]);
                }else{
                    fprintf(reporte, "<TR>\n<TD>i_indirecto_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, inode.i_indirecto[j]);
                }
            }
            fprintf(reporte, "<TR>\n<TD>I_idPropetario</TD><TD>%i</TD>\n</TR>\n", inode.i_idPropetario);
            fprintf(reporte, "<TR>\n<TD>I_idPermisos</TD><TD>%.3s</TD>\n</TR>\n", inode.i_idPermisos);
            fprintf(reporte, "</TABLE>>];\n");
            
        }
    }
    
    for (int i = 0; i < sb.sb_numero_inodos; i++) {
        fseek(disco, sb.sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco, sb.sb_ap_inodos + i * sb.sb_tamano_inodo, SEEK_SET);
            fread(&inode, sb.sb_tamano_inodo, 1, disco);
            for (int j = 0; j < MAX_DIREC_APUNT; j++){
                if(inode.i_bloque[j]!=-1){
                    fprintf(reporte, "\"I%i\":f%i->\"B%i\";\n", i, inode.i_bloque[j], inode.i_bloque[j]);
                }
            }
            for (int j = 0; j < MAX_INDIREC_APUNT; j++){
                if(inode.i_indirecto[j]!=-1){
                    fprintf(reporte, "\"I%i\":f%i->\"B%i\";\n", i, inode.i_indirecto[j], inode.i_indirecto[j]);
                }
            }
        }
    }
    
    for (int i = 0; i < sb.sb_numero_bloques; i++) {
        fseek(disco, sb.sb_ap_bitmap_ficheros + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco,sb.sb_ap_ficheros+i*sb.sb_tamano_bloque,SEEK_SET);
            switch(leido){
                case 'a':
                    fread(&bloqueA,sb.sb_tamano_bloque,1,disco);
                    break;
                case 'c':
                    fread(&bloqueC,sb.sb_tamano_bloque,1,disco);
                    break;
                case 'd':
                    fread(&bloqueD,sb.sb_tamano_bloque,1,disco);
                    break;
            }
     
            switch(leido){
                case 'a':
                    for(int j=0;j<MAX_BD_APUNTS;j++){
                        if(bloqueA.apuntador[j]!=-1){
                            fprintf(reporte, "\"B%i\":f%i->\"B%i\";\n",i,bloqueA.apuntador[j],bloqueA.apuntador[j]);
                        }
                    }
                    break;
                case 'c':
                    for(int j=0;j<MAX_AVD_DIREC_APUNT;j++){
                        if(bloqueC.avd_bloque[j]!=-1){
                            fprintf(reporte, "\"B%i\":f%i->\"I%i\";\n",i,bloqueC.avd_bloque[j],bloqueC.avd_bloque[j]);
                        }
                    }
                    break;
            }
        }
    }

    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);

    printf("-- REPORTE COMPLETO DEL SISTEMA GENERADO --\n");
    sleep(SLEEP_TIME);
}
