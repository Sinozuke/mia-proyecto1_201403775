#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "Montaje.h"
#include "FS_OP.h"
#include "Aux_FS_OP.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"
#include "Sesion.h"

void copiar_elemento(Montaje *montaje, Sesion *sesion, char id[], char path_destino[], char path_elemento[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);
    
    
    /* DESTINO */
    
    char carpeta_destino[MAXLON];
    
    strncpy(carpeta_destino, path_destino, MAXLON);
    
    char copia_path_destino[strlen(path_destino)];
    
    clean_string(copia_path_destino, strlen(path_destino));
    
    strncpy(copia_path_destino, path_destino, strlen(path_destino));

    char *carpeta_p1;
    
    carpeta_p1 = strtok(copia_path_destino, "/");
    
    while (carpeta_p1) {
        if (strlen(carpeta_p1) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p1);
            fclose(disco);
            return;
        }
        carpeta_p1 = strtok(NULL, "/");
    }
    
    int numero1 = contar_carpes(carpeta_destino);
    
    carpetas carpes_destino[numero1];
    vaciar_carpes(carpes_destino, numero1);
    
    llenar_carpes(carpes_destino, carpeta_destino);
    
    int padre_destino;
    inodo_S2 inodo_padre_destino;
    int existe_destino;
    
    if(strcmp(carpeta_destino,"/")!=0){
        padre_destino = buscar_dir_padre(disco, &sb, carpes_destino, numero1);

        if (padre_destino == -1) {
            fclose(disco);
            return;
        }
    
        fseek(disco, sb.sb_ap_inodos + padre_destino * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_destino, sb.sb_tamano_inodo, 1, disco);
        existe_destino = dir_exist(disco, &sb, inodo_padre_destino, carpes_destino[numero1 - 1].bname, 'c');

        if (existe_destino == -1) {
            printf("ERROR: NO EXISTE EL DIRECTORIO \"%s\"\n", carpes_destino[numero1 - 1].bname);
            fclose(disco);
            return;
        }

    }else{
        existe_destino = 0;
    }

    /* ORIGEN */
    
    
    char elemento_origen[MAXLON];

    strncpy(elemento_origen, path_elemento, MAXLON);
    
    char copia_path_origen[strlen(path_elemento)];

    clean_string(copia_path_origen, strlen(path_elemento));

    strncpy(copia_path_origen, path_elemento, strlen(path_elemento));
    
    char *carpeta_p2;
    
    carpeta_p2 = strtok(copia_path_origen, "/");
    
    char elemento_final[MAX_NAME_SIZE];
    
    while (carpeta_p2) {
        if (strlen(carpeta_p2) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p2);
            fclose(disco);
            return;
        }
        strncpy(elemento_final, carpeta_p2, MAX_NAME_SIZE);
        carpeta_p2 = strtok(NULL, "/");
    }

    int numero2 = contar_carpes(elemento_origen);
    
    carpetas carpes_origen[numero2];
    
    vaciar_carpes(carpes_origen, numero2);
    llenar_carpes(carpes_origen, elemento_origen);
    
    int padre_origen;
    inodo_S2 inodo_padre_origen;
    int existe_origen;
    char tipo_elemento='a';
    int repetido;
    inodo_S2 inodo_destino;
        
    if(strcmp(elemento_origen,"/")!=0){
    
        padre_origen = buscar_dir_padre(disco, &sb, carpes_origen, numero2);

        if (padre_origen == -1) {
            fclose(disco);
            return;
        }
    
        fseek(disco, sb.sb_ap_inodos + padre_origen * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_origen, sb.sb_tamano_inodo, 1, disco);
        
        existe_origen = dir_exist(disco, &sb, inodo_padre_origen, carpes_origen[numero2 - 1].bname, 'a');

        if (existe_origen == -1) {
            existe_origen = dir_exist(disco, &sb, inodo_padre_origen, carpes_origen[numero2 - 1].bname, 'c');
            tipo_elemento='c';
            if (existe_origen == -1) {
                printf("ERROR: NO EXISTE ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
        fseek(disco,sb.sb_ap_inodos+existe_origen*sb.sb_tamano_inodo,SEEK_SET);
        fread(&inodo_destino,sb.sb_tamano_inodo,1,disco);
        
        repetido = dir_exist(disco, &sb, inodo_destino, carpes_origen[numero2 - 1].bname, 'a');

        if (repetido == -1) {
            repetido = dir_exist(disco, &sb, inodo_destino, carpes_origen[numero2 - 1].bname, 'c');
            if (repetido != -1) {
                printf("ERROR: YA EXISTE UN ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
    }else{
        
        fseek(disco, sb.sb_ap_inodos + padre_origen * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_origen, sb.sb_tamano_inodo, 1, disco);
        
        repetido = dir_exist(disco, &sb, inodo_padre_destino, "/", 'a');

        if (repetido == -1) {
            repetido = dir_exist(disco, &sb, inodo_padre_destino, carpes_origen[numero2 - 1].bname, 'c');
            if (repetido != -1) {
                printf("ERROR: YA EXISTE UN ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
        existe_origen=0;
        strncpy(elemento_final,"/",1);
    }    
    
    
    
    if(strcmp(carpeta_destino,"/")!=0 || numero1 == 1){
        copiar(disco,&sb,sesion,"/",existe_destino,existe_origen);
    }else{
        copiar(disco,&sb,sesion,carpes_destino[numero1-2].bname,existe_destino,existe_origen);
    }
    
    nueva_bitacora('4', tipo_elemento, elemento_origen, sb, disco);
    
    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CREACION DE COPIA DEL ELEMENTO \"%s\" HACIA \"%s\" EXITOSA.\n", elemento_origen,carpeta_destino);

}

void mover_elemento(Montaje *montaje, Sesion *sesion, char id[], char path_destino[], char path_elemento[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);
    
    
    /* DESTINO */
    
    char carpeta_destino[MAXLON];
    
    strncpy(carpeta_destino, path_destino, MAXLON);
    
    char copia_path_destino[strlen(path_destino)];
    
    clean_string(copia_path_destino, strlen(path_destino));
    
    strncpy(copia_path_destino, path_destino, strlen(path_destino));

    char *carpeta_p1;
    
    carpeta_p1 = strtok(copia_path_destino, "/");
    
    while (carpeta_p1) {
        if (strlen(carpeta_p1) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p1);
            fclose(disco);
            return;
        }
        carpeta_p1 = strtok(NULL, "/");
    }
    
    int numero1 = contar_carpes(carpeta_destino);
    
    carpetas carpes_destino[numero1];
    vaciar_carpes(carpes_destino, numero1);
    
    llenar_carpes(carpes_destino, carpeta_destino);
    
    int padre_destino;
    inodo_S2 inodo_padre_destino;
    int existe_destino;
    inodo_S2 inodo_destino;
    
    if(strcmp(carpeta_destino,"/")!=0){
        padre_destino = buscar_dir_padre(disco, &sb, carpes_destino, numero1);

        if (padre_destino == -1) {
            fclose(disco);
            return;
        }
    
        fseek(disco, sb.sb_ap_inodos + padre_destino * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_destino, sb.sb_tamano_inodo, 1, disco);
        existe_destino = dir_exist(disco, &sb, inodo_padre_destino, carpes_destino[numero1 - 1].bname, 'c');

        if (existe_destino == -1) {
            printf("ERROR: NO EXISTE EL DIRECTORIO \"%s\"\n", carpes_destino[numero1 - 1].bname);
            fclose(disco);
            return;
        }
        
        fseek(disco,sb.sb_ap_inodos+existe_destino*sb.sb_tamano_inodo,SEEK_SET);
        fread(&inodo_destino,sb.sb_tamano_inodo,1,disco);

    }else{
        existe_destino = 0;
    }

    /* ORIGEN */
    
    
    char elemento_origen[MAXLON];

    strncpy(elemento_origen, path_elemento, MAXLON);
    
    char copia_path_origen[strlen(path_elemento)];

    clean_string(copia_path_origen, strlen(path_elemento));

    strncpy(copia_path_origen, path_elemento, strlen(path_elemento));
    
    char *carpeta_p2;
    
    carpeta_p2 = strtok(copia_path_origen, "/");
    
    char elemento_final[MAX_NAME_SIZE];
    
    while (carpeta_p2) {
        if (strlen(carpeta_p2) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p2);
            fclose(disco);
            return;
        }
        strncpy(elemento_final, carpeta_p2, MAX_NAME_SIZE);
        carpeta_p2 = strtok(NULL, "/");
    }

    int numero2 = contar_carpes(elemento_origen);
    
    carpetas carpes_origen[numero2];
    
    vaciar_carpes(carpes_origen, numero2);
    llenar_carpes(carpes_origen, elemento_origen);
    
    int padre_origen;
    inodo_S2 inodo_padre_origen;
    int existe_origen;
    char tipo_elemento='a';
    int repetido;
    inodo_S2 inodo_origen;
        
    if(strcmp(elemento_origen,"/")!=0){
    
        padre_origen = buscar_dir_padre(disco, &sb, carpes_origen, numero2);

        if (padre_origen == -1) {
            fclose(disco);
            return;
        }
    
        fseek(disco, sb.sb_ap_inodos + padre_origen * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_origen, sb.sb_tamano_inodo, 1, disco);
        
        existe_origen = dir_exist(disco, &sb, inodo_padre_origen, carpes_origen[numero2 - 1].bname, 'a');

        if (existe_origen == -1) {
            existe_origen = dir_exist(disco, &sb, inodo_padre_origen, carpes_origen[numero2 - 1].bname, 'c');
            tipo_elemento='c';
            if (existe_origen == -1) {
                printf("ERROR: NO EXISTE ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
        repetido = dir_exist(disco, &sb, inodo_destino, carpes_origen[numero2 - 1].bname, 'a');

        if (repetido == -1) {
            repetido = dir_exist(disco, &sb, inodo_destino, carpes_origen[numero2 - 1].bname, 'c');
            if (repetido != -1) {
                printf("ERROR: YA EXISTE UN ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
    }else{
        
        fseek(disco, sb.sb_ap_inodos + padre_origen * sb.sb_tamano_inodo, SEEK_SET);
        fread(&inodo_padre_origen, sb.sb_tamano_inodo, 1, disco);
        
        repetido = dir_exist(disco, &sb, inodo_destino, "/", 'a');

        if (repetido == -1) {
            repetido = dir_exist(disco, &sb, inodo_destino, carpes_origen[numero2 - 1].bname, 'c');
            if (repetido != -1) {
                printf("ERROR: YA EXISTE UN ELEMENTO CON EL NOMBRE DE \"%s\"\n", carpes_destino[numero2 - 1].bname);
                fclose(disco);
                return;
            }
        }
        
        existe_origen=0;
        strncpy(elemento_final,"/",1);
    }    
    
    if(!can_write(disco,&sb,sesion,existe_destino)){
        printf("ERROR: NO TIENE PERMISOS DE ESCRITURA EN LA CARPETA DESTINO.\n");
        fclose(disco);
        return;
    }

    if(!can_read(disco,&sb,sesion,existe_origen)){
        printf("ERROR: NO TIENE PERMISOS DE LECTURA DEL ELEMENTO ORIGEN.\n");
        fclose(disco);
        return;
    }
    
    enlazar_inodos(disco,&sb,&inodo_destino, existe_origen);
    desenlazar_inodos(disco,&sb, &inodo_padre_origen, existe_origen);

    insert_inodo(&sb,disco, &inodo_destino,inodo_destino.i_llave);
    insert_inodo(&sb,disco, &inodo_padre_destino,inodo_padre_destino.i_llave);
    
    nueva_bitacora('5', tipo_elemento, elemento_origen, sb, disco);
    
    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("MOVER ELEMENTO \"%s\" HACIA \"%s\" EXITOSO.\n", elemento_origen,carpeta_destino);

}