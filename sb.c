#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_sb(Montaje *montaje, char *id, char *path) {

    char directorio[300];
    for (int i = 0; i < 300; i++) {
        directorio[i] = '\0';
    }
    strncpy(directorio, path, strlen(path));

    if (!strstr(directorio, ".svg")) {
        strcat(directorio, ".svg");
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[MAXLON+1];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strncpy(path_disco, montaje[id[2] - 97].path,MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: La Particion Montada No esta formateada con un sistema de archivos valido.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;
    
    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"",1);
    strncat(comando_dot, directorio,strlen(directorio));
    strncat(comando_dot, "\" SB.dot",8);

    strncat(comando_abertura, "\"",1);
    strncat(comando_abertura, directorio,strlen(directorio));
    strncat(comando_abertura, "\"",1);

    disco = fopen(path_disco, "r+b");
    
    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    
    SB_S2 sb;
    struct tm *info;
    char buffer[80];
    
    fseek(disco,encontrado.comienzo,SEEK_SET);
    fread(&sb,sizeof(SB_S2),1,disco);
    
    fclose(disco);
    
    reporte = fopen("SB.dot", "w");
    
    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");
    fprintf(reporte, "\t\"mbr\" [label=<\n");
    fprintf(reporte, "\t\t<TABLE>\n");
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD COLSPAN=\"2\">Super Boot</TD>\n");
    fprintf(reporte, "\t\t\t</TR>\n");
        
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_inodos_count</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_numero_inodos);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_bloques_count</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_numero_bloques);
    fprintf(reporte, "\t\t\t</TR>\n");

    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_inodos_free</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_inodos_free);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_bloques_free</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_bloques_free);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    for (int i = 0; i < 80; i++) {
        buffer[i] = '\0';
    }
    info = localtime(&sb.sb_mount_time);
    strftime(buffer, 80, "%x - %I:%M%p", info);
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_mount_time</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%s</TD>\n",buffer);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_ap_bitmap_inodo</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_ap_bitmap_inodos);
    fprintf(reporte, "\t\t\t</TR>\n");     
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_ap_inodo</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_ap_inodos);
    fprintf(reporte, "\t\t\t</TR>\n");     
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_ap_bitmap_bloques</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_ap_bitmap_ficheros);
    fprintf(reporte, "\t\t\t</TR>\n");     
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_ap_bloques</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_ap_ficheros);
    fprintf(reporte, "\t\t\t</TR>\n");   
        
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_size_struct_inodo</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_tamano_inodo);
    fprintf(reporte, "\t\t\t</TR>\n");

    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_size_struct_bloque</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_tamano_bloque);
    fprintf(reporte, "\t\t\t</TR>\n");
        
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_magic_num</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_numero_magico);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_first_free_inodo</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_first_free_inodo);
    fprintf(reporte, "\t\t\t</TR>\n");
    
    fprintf(reporte, "\t\t\t<TR>\n");
    fprintf(reporte, "\t\t\t\t<TD>sb_first_free_fichero</TD>\n");
    fprintf(reporte, "\t\t\t\t<TD>%i</TD>\n",sb.sb_first_free_fichero);
    fprintf(reporte, "\t\t\t</TR>\n");

    fprintf(reporte, "</TABLE>>];");
    
    fprintf(reporte, "}");


    fclose(reporte);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);
    printf("-- REPORTE SB GENERADO --\n");
    sleep(SLEEP_TIME);
    
}