#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "Montaje.h"
#include "Aux_Archivo.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"
#include "Sesion.h"

void mostrar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'a');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    if(!can_read(disco,&sb,sesion,existe)){
        printf("ERROR: EL USUARIO NO TIENE PERMISOS PARA LECTURA EN \"%s\".\n",nueva_carpeta_padre.i_name);
        fclose(disco);
        return;
    }

    inodo_S2 archivo;
    
    fseek(disco, sb.sb_ap_inodos+existe*sb.sb_tamano_inodo, SEEK_SET);
    fread(&archivo, sizeof (inodo_S2), 1, disco);

    char contenido[archivo.i_tam_archivo];
    
    clean_string(contenido,archivo.i_tam_archivo);

    contenido_archivo(contenido, disco, sb, archivo);
    
    printf("------------------------------------------------------------\n\n");
    printf("%s\n\n",contenido);
    printf("------------------------------------------------------------\n\n");
    
    fclose(disco);

    printf("MODIFICACION DEL ARCHIVO \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void crear_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[], char contenido[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nuevo_archivo_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nuevo_archivo_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nuevo_archivo_padre, carpes[numero - 1].bname, 'a');

    if (existe != -1) {
        printf("ERROR: YA EXISTE EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    if(!can_write(disco,&sb,sesion,padre)){
        printf("ERROR: EL USUARIO NO TIENE PERMISOS PARA ESCRITURA EN \"%s\".\n",nuevo_archivo_padre.i_name);
        fclose(disco);
        return;
    }

    inodo_S2 nueva_archivoino = nuevo_inodo(-1, 'a', sesion->UID, carpes[numero - 1].bname);
    int nuevo_archivo_posino = insert_inodo(&sb, disco, &nueva_archivoino, -1);

    enlazar_inodos(disco, &sb, &nuevo_archivo_padre, nuevo_archivo_posino);

    insert_inodo(&sb, disco, &nuevo_archivo_padre, nuevo_archivo_padre.i_llave);

    nueva_bitacora('1', 'a', carpes[numero - 1].bname, sb, disco);

    editar_archivo(disco, &sb, nuevo_archivo_posino, contenido, carpes[numero - 1].bname);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CREACION DE ARCHIVO \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void eliminar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'a');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    int acept = 1;

    eliminar_recursivamente(disco, &sb, existe);
    
    if(!acept){
        fclose(disco);
        return;
    }

    desenlazar_inodos(disco, &sb, &nueva_carpeta_padre, existe);

    insert_inodo(&sb, disco, &nueva_carpeta_padre, nueva_carpeta_padre.i_llave);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("ELIMINACION DE ARCHIVO \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void modificar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'a');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    int access = check_access(disco,&sb,sesion,existe);
    
    if(access !=7 && access !=6){
        printf("ERROR: EL USUARIO NO TIENE PERMISOS DE ESCRITURA Y ESCRITURA SOBRE EL ELEMENTO \"%s\".\n",carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    inodo_S2 archivo;
    
    fseek(disco, sb.sb_ap_inodos+existe*sb.sb_tamano_inodo, SEEK_SET);
    fread(&archivo, sizeof (inodo_S2), 1, disco);

    char contenido[archivo.i_tam_archivo];
    
    clean_string(contenido,archivo.i_tam_archivo);

    contenido_archivo(contenido, disco, sb, archivo);
    
    FILE *modificacion;
    
    modificacion = fopen("file_modificacion.txt", "w+");
    
    if(!modificacion){
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        fclose(disco);
        return;
    }
    
    fprintf(modificacion,"%s",contenido);
    
    fclose(modificacion);
    
    system("gnome-open file_modificacion.txt");
    
    printf("CUANDO TERMINE DE EDITAR SU DOCUMENTO, CIERRE LA VENTANA DE EDICION Y SEGUIDO PRECIONE ENTER.\n");

    char enter = '\0';
    while (enter != '\r' && enter != '\n') {
        enter = getchar(); 
    }
    
    modificacion = fopen("file_modificacion.txt", "r+");
    
    while(!modificacion){
        printf("ERROR: CIERRE LA VENTANA DE EDICION Y LUEGO PRECIONE ENTER.\n");
        enter = '\0';
        while (enter != '\r' && enter != '\n') {
            enter = getchar(); 
        }
        modificacion = fopen("file_modificacion.txt", "r+");
    }
    
    fseek(modificacion,0,SEEK_END);
    
    int cant_total = ftell(modificacion);
    
    if(cant_total>(MAX_BD_CONT_SIZE*MAX_DIREC_APUNT + MAX_BD_CONT_SIZE*MAX_BD_APUNTS + MAX_BD_CONT_SIZE*MAX_BD_APUNTS*MAX_BD_APUNTS)){
        printf("ERROR: EL CONTENIDO DEL ARCHIVO EXCEDE LA CANTIDAD QUE PUEDE MANEJAR EL SITEMA DE ARCHIVOS.\n");
        fclose(modificacion);
        fclose(disco);
        return;
    }
    
    fseek(modificacion,0,SEEK_SET);
    
    char texto_modificado[cant_total];
    
    clean_string(texto_modificado,cant_total);
    
    fgets(texto_modificado, cant_total, modificacion);
    
    fclose(modificacion);
    
    editar_archivo(disco, &sb, archivo.i_llave, texto_modificado, archivo.i_name);
    
    insert_SB_S2(disco, &sb, encontrado.comienzo);
    
    fclose(disco);

    printf("MODIFICACION DEL ARCHIVO \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void update_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[], char name[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'a');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    int repetido = dir_exist(disco, &sb, nueva_carpeta_padre, name, 'a');

    if (repetido != -1) {
        printf("ERROR: YA EXISTE UN ARCHIVO CON EL NOMBRE \"%s\"\n", name);
        fclose(disco);
        return;
    }
    
    if(!can_write(disco,&sb,sesion,existe)){
        printf("ERROR: NO TIENE PERMISOS DE ESCRITURA SOBRE EL ARCHIVO \"%s\".\n",carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    update_archivo(disco, &sb, existe, name);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CAMBIO DE NOMBRE DEL ARCHIVO \"%s\" - \"%s\" A \"%s\" EXITOSO.\n", carpes[numero - 1].bname, carpeta_a_crear, name);

}