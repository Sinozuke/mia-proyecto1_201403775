#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "formato.h"
#include "Montaje.h"
#include "Aux_FS.h"

void formatear_mkfs(Montaje *montaje, char *id, char type) {
    char path[100];
    char byte = '\0';

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }
    strcpy(path, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    printf("-------------COMENZADO FORMATEO----------------\n");
    printf("---------------------DATOS---------------------\n");
    printf("Particion a Formatear: %s\n", encontrado.nombre);
    printf("Montaje: %s\n", encontrado.id);
    switch (type) {
        case 'a':
            printf("Tipo de Formateo: Rapido\n");
            break;
        case 'u':
            printf("Tipo de Formateo: Completo\n");
            break;
    }

    printf("Sistema de Archivos: EXT3\n");

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("--------------FORMATEO FALLIDO-----------------\n");
        return;
    }

    if (encontrado.status == 's') {
        if (encontrado.tipo == 'e') {
            printf("ERROR:el tipo de particion montada es EXTENDIDA.\nNo se puede aplicar formato a este tipo de particion.\n");
            printf("--------------FORMATEO FALLIDO-----------------\n");
            fclose(disco);
            return;
        }
    } else {
        printf("ADVERTENCIA: la particion montada ya contiene un sistema de archivos, desea formatear de iogual forma?(S/n)\n");
        char res;
        fgets(&res, sizeof (char), stdin);
        while (res != 'n' && res != 'S') {
            printf("Respuesta no Valida.(S/n)\n");
            fgets(&res, 10, stdin);
        }

        if (res == 'n') {
            printf("--------------FORMATEO CANCELADO-----------------\n");
            return;
        }
    }
    if (type == 'u') {
        fseek(disco, encontrado.comienzo, SEEK_SET);
        for (int i = 0; i < encontrado.size; i++)
            fwrite(&byte, sizeof (char), 1, disco);
    }

    char vacio = '0';
    char lleno = '1';

    //    int n = (encontrado.size - 2*sizeof(SB))/(27 + sizeof(AVD)+sizeof(DD)+5*sizeof(inodo) + 20*(sizeof(BD)) + 15*sizeof(Log));

    int n = (encontrado.size - sizeof (SB_S2)) / (1 + NUMBRE_BLOCKS + sizeof (inodo_S2) + NUMBRE_BLOCKS * sizeof (BD_S2) + NUMBER_LOGS_FS * sizeof (Log_S2));

    SB_S2 super_bloque;
    inodo_S2 inode;
    BD_S2 bloque;
    Log_S2 log;

    log = bitacora_vacia();

    inode.i_idPropetario = -1;
    inode.i_asig_bloques = -1;
    inode.i_llave = -1;
    inode.i_tam_archivo = -1;
    inode.i_tipo = '-';
    inode.i_fecha_creacion = time(NULL);
    strncpy(inode.i_idPermisos, "000", 3);
    clean_string(inode.i_name, MAX_NAME_SIZE);

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        inode.i_bloque[i] = -1;
    }
    for (int i = 0; i < MAX_INDIREC_APUNT; i++) {
        inode.i_indirecto[i] = -1;
    }

    clean_string(bloque.db_data, MAX_BD_CONT_SIZE);
    clean_string(bloque.db_nombre, MAX_NAME_SIZE);
    clean_string(bloque.db_padre, MAX_NAME_SIZE);

    super_bloque.sb_numero_inodos = n;
    super_bloque.sb_numero_bloques = NUMBRE_BLOCKS*n;
    super_bloque.sb_tamano_bloque = sizeof (BD_S2);
    super_bloque.sb_tamano_inodo = sizeof (inodo_S2);
    super_bloque.sb_numero_magico = 201403775;
    super_bloque.sb_inodos_free = n;
    super_bloque.sb_bloques_free = NUMBRE_BLOCKS * n;
    super_bloque.sb_ap_bitmap_inodos = encontrado.comienzo + sizeof (SB_S2);
    super_bloque.sb_ap_bitmap_ficheros = super_bloque.sb_ap_bitmap_inodos + super_bloque.sb_numero_inodos;
    super_bloque.sb_ap_inodos = super_bloque.sb_ap_bitmap_ficheros + super_bloque.sb_numero_bloques;
    super_bloque.sb_ap_ficheros = super_bloque.sb_ap_inodos + n * sizeof (inodo_S2);
    super_bloque.sb_first_free_inodo = 0;
    super_bloque.sb_first_free_fichero = 0;

    fseek(disco, encontrado.comienzo, SEEK_SET);

    int asdfasdf=ftell(disco);
    
    fwrite(&super_bloque, sizeof (SB_S2), 1, disco);

    asdfasdf=ftell(disco);
    
    /* BITMAP INODO */
    for (int i = 0; i < super_bloque.sb_numero_inodos; i++) {
        fwrite(&vacio, sizeof (char), 1, disco);
    }
    
    asdfasdf=ftell(disco);
    
    /* BITMAP BLOQUES */
    for (int i = 0; i < super_bloque.sb_numero_bloques; i++) {
        fwrite(&vacio, sizeof (char), 1, disco);
    }

    asdfasdf=ftell(disco);
    
    /* INODOS */
    for (int i = 0; i < super_bloque.sb_numero_inodos; i++) {
        fwrite(&inode, sizeof (inodo_S2), 1, disco);
    }

    asdfasdf=ftell(disco);
    
    /* BLOQUES */
    for (int i = 0; i < super_bloque.sb_numero_bloques; i++) {
        fwrite(&bloque, sizeof (BD_S2), 1, disco);
    }

    asdfasdf=ftell(disco);
    

    
    /* LOG */
    for (int i = 0; i < (NUMBER_LOGS_FS * n); i++) {
        fwrite(&log, sizeof (Log_S2), 1, disco);
    }

    asdfasdf=ftell(disco);
    
    if (encontrado.tipo == 'p') {
        MBR mbr;

        fseek(disco, 0, SEEK_SET);
        if (fread(&mbr, sizeof (MBR), 1, disco) != 1) {
            printf("ERROR:al cargar la data del disco\n");
            printf("--------------FORMATEO FALLIDO-----------------\n");
            fclose(disco);
            return;
        }

        if (mbr.mbr_partition_1.part_start == encontrado.comienzo)
            mbr.mbr_partition_1.part_status = 'f';
        else if (mbr.mbr_partition_2.part_start == encontrado.comienzo)
            mbr.mbr_partition_2.part_status = 'f';
        else if (mbr.mbr_partition_3.part_start == encontrado.comienzo)
            mbr.mbr_partition_3.part_status = 'f';
        else if (mbr.mbr_partition_4.part_start == encontrado.comienzo)
            mbr.mbr_partition_4.part_status = 'f';
        else {
            EBR ebr;
            fseek(disco, encontrado.comienzo - sizeof (EBR), SEEK_SET);
            fread(&ebr, sizeof (EBR), 1, disco);
            ebr.part_status = 'f';
            fseek(disco, sizeof (EBR), SEEK_CUR);
            fwrite(&ebr, sizeof (EBR), 1, disco);
        }

        fseek(disco, 0, SEEK_SET);
        fwrite(&mbr, sizeof (MBR), 1, disco);
    } else {
        fseek(disco, encontrado.comienzo, SEEK_SET);
        fseek(disco, -sizeof (EBR), SEEK_CUR);
        EBR ebr;

        if (fread(&ebr, sizeof (EBR), 1, disco) != 1) {
            printf("ERROR:al cargar la data del disco\n");
            printf("--------------FORMATEO FALLIDO-----------------\n");
            fclose(disco);
            return;
        }

        ebr.part_status = 'f';
        fseek(disco, -sizeof (EBR), SEEK_CUR);
        fwrite(&ebr, sizeof (EBR), 1, disco);
    }

    encontrado.status = 'f';
    montaje[id[2] - 97].lista[a] = encontrado;

    fclose(disco);

    
    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("--------------FORMATEO FALLIDO-----------------\n");
        return;
    }
    
    fseek(disco,encontrado.comienzo,SEEK_SET);
    fread(&super_bloque,sizeof(SB_S2),1,disco);
    
    printf("CREACION DE CARPETA \"/\"\n");
    
    BC_S2 bcraiz = nuevo_bloque_BC("/", "/");
    inodo_S2 iraiz = nuevo_inodo(super_bloque.sb_first_free_inodo,'c',1,"/");
    
    iraiz.i_bloque[0] = insert_BC(&super_bloque, disco, bcraiz,-1);
    iraiz.i_asig_bloques = 1;
    strncpy(iraiz.i_idPermisos, "777", 3);
    int bc_raiz = insert_inodo(&super_bloque, disco, &iraiz,-1);
    
    nueva_bitacora('1', 'c',"/",super_bloque,disco);
    
    printf("CREACION DE ARCHIVO \"users.txt\"\n");
    
    inodo_S2 iusers = nuevo_inodo(super_bloque.sb_first_free_inodo,'a',1,"users.txt");
    
    char contenido_users[33] = "1,g,root\n1,u,root,root,201403775\n";
    int puntero = 0;
    int longitud = 33;
    int asig_bloques =0;
    
    for(int i=0;i<MAX_AVD_DIREC_APUNT && puntero<longitud;i++,asig_bloques++){
        iusers.i_bloque[i] = insert_BD(&super_bloque, disco, nuevo_bloque_BD(&puntero,longitud, contenido_users,"users.txt"),-1);
    }
    
    iusers.i_asig_bloques=asig_bloques;
    iusers.i_tam_archivo=longitud;
    
    bcraiz.avd_bloque[0] = insert_inodo(&super_bloque, disco, &iusers,-1);
    iraiz.i_bloque[0] = insert_BC(&super_bloque, disco, bcraiz,iraiz.i_bloque[0]);
    insert_inodo(&super_bloque, disco, &iraiz,bc_raiz);

    nueva_bitacora('1', 'a',"users.txt",super_bloque,disco);
    
    insert_SB_S2(disco, &super_bloque,encontrado.comienzo);
    
    fclose(disco);
    
    printf("--------------FORMATEO COMPLETO-----------------\n");

}