#ifndef SESION_H
#define SESION_H

void login(Sesion *sesion,Montaje *montaje,char id[],char username[],char password[]);
void logout(Sesion *sesion);
int can_read(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo);
int can_write(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo);
int check_access(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo);

#endif /* SESION_H */

