#ifndef DIRECTORIO_H
#define DIRECTORIO_H

void crear_Directorio(Montaje *montajes, Sesion *sesion, char id[], char path[]);
void update_carpeta(Montaje *montaje, Sesion *sesion, char id[], char path[],char name[]);
void eliminar_Directorio(Montaje *montaje, Sesion *sesion, char id[], char path[]);

#endif /* DIRECTORIO_H */

