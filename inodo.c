#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_inode(Montaje *montaje, char *id, char *path) {

    char directorio[200];

    for (int i = 0; i < 200; i++) {
        directorio[i] = '\0';
    }


    strncpy(directorio, path, strlen(path));

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("**********GENERACION FALLIDA***********\n\n");
        return;
    }

    if (!strstr(directorio, ".svg")) {
        strcat(directorio, ".svg");
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------GENERACION FALLIDA------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" INODE.dot", 11);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");
    reporte = fopen("INODE.dot", "w");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    SB_S2 sb;
    inodo_S2 inode;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fseek(disco, sb.sb_ap_bitmap_inodos, SEEK_SET);
    char leido = '0';

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "node [shape=plaintext];\n");
    fprintf(reporte, "rankdir = LR;\n");

    for (int i = 0; i < sb.sb_numero_inodos; i++) {
        fseek(disco, sb.sb_ap_bitmap_inodos + i, SEEK_SET);
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            fseek(disco, sb.sb_ap_inodos + i * sb.sb_tamano_inodo, SEEK_SET);
            fread(&inode, sb.sb_tamano_inodo, 1, disco);
            fprintf(reporte, "\"%i\" [label=<\n", i + 1);
            fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",INODE_COLOR);
            fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Inodo No.%i</TD>\n</TR>\n", i + 1);
            switch (inode.i_tipo) {
                case 'c':
                    fprintf(reporte, "<TR>\n<TD>i_type</TD><TD>Carpeta</TD>\n</TR>\n");
                    break;
                case 'a':
                    fprintf(reporte, "<TR>\n<TD>i_type</TD><TD>Archivo</TD>\n</TR>\n");
                    break;
            }
            fprintf(reporte, "<TR>\n<TD>i_nombre</TD><TD>%.24s</TD>\n</TR>\n", inode.i_name);
            fprintf(reporte, "<TR>\n<TD>i_llave</TD><TD>%i</TD>\n</TR>\n", i + 1);
            fprintf(reporte, "<TR>\n<TD>i_tam_archivo</TD><TD>%i</TD>\n</TR>\n", inode.i_tam_archivo);
            fprintf(reporte, "<TR>\n<TD>i_asig_bloques</TD><TD>%i</TD>\n</TR>\n", inode.i_asig_bloques);
            for (int j = 0; j < MAX_DIREC_APUNT; j++)
                fprintf(reporte, "<TR>\n<TD>i_bloque_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, inode.i_bloque[j]);
            for (int j = 0; j < MAX_INDIREC_APUNT; j++)
                fprintf(reporte, "<TR>\n<TD>i_indirecto_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, inode.i_indirecto[j]);
            fprintf(reporte, "<TR>\n<TD>I_idPropetario</TD><TD>%i</TD>\n</TR>\n", inode.i_idPropetario);
            fprintf(reporte, "<TR>\n<TD>I_idPermisos</TD><TD>%.3s</TD>\n</TR>\n", inode.i_idPermisos);
            fprintf(reporte, "</TABLE>>];\n");
        }
    }

    int anterior = 0;
    int actual = 0;
    fseek(disco, sb.sb_ap_bitmap_inodos, SEEK_SET);

    for (int i = 0; i < sb.sb_numero_inodos; i++) {
        fread(&leido, sizeof (char), 1, disco);
        if (leido != '0') {
            anterior = actual;
            actual = i;
            if (actual != 0)
                fprintf(reporte, "\"%i\"->\"%i\";\n", anterior + 1, actual + 1);
        }
    }

    fprintf(reporte, "}");

    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);

    printf("-- REPORTE INODO GENERADO --\n");
    sleep(SLEEP_TIME);
}
