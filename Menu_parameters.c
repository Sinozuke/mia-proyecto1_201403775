#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main_data.h"
#include "main_functions.h"
#include "Menu_parameters.h"
#include "Disco.h"
#include "Particion.h"
#include "Montaje.h"
#include "formato.h"
#include "Sesion.h"
#include "Reportes.h"
#include "usuarios.h"
#include "grupos.h"
#include "Directorio.h"
#include "Archivo.h"
#include "FS_OP.h"
#include "Permisos.h"

void Test(Montaje *montajes) {

    /* ELIMINACION DE DISCO 1 */

    char directorio_eliminar[MAXLON + 1];

    clean_string(directorio_eliminar, MAXLON);

    strcpy(directorio_eliminar, "/home/son/MIA/Discos/prueba_disco.dsk");

    printf("**********************ELIMINANDO DISCO************************\n");
    printf("****************************DATOS*****************************\n");
    printf("Directorio:%s\n", directorio_eliminar);
    printf("*************************ELIMINANDO**************************\n");
    destruir_disco(montajes, directorio_eliminar);

    /* ELIMINACION DE DISCO 2 */
    /*
    char directorio_eliminar2[MAXLON + 1];

    clean_string(directorio_eliminar2, MAXLON);

    strcpy(directorio_eliminar2, "/home/son/MIA/Discos/prueba_disco2.dsk");

    printf("**********************ELIMINANDO DISCO************************\n");
    printf("****************************DATOS*****************************\n");
    printf("Directorio:%s\n", directorio_eliminar2);
    printf("*************************ELIMINANDO**************************\n");
    destruir_disco(montajes, directorio_eliminar2);
     */
    /* CREAR DISCO 1 */

    char nombre_disco[MAXLON + 1];
    char directorio_disco[MAXLON + 1];
    int tipo_disco = 1;
    int tamano_disco = -1;

    clean_string(nombre_disco, MAXLON);
    clean_string(directorio_disco, MAXLON);

    strcpy(nombre_disco, "prueba_disco");
    strcpy(directorio_disco, "/home/son/MIA/Discos/");
    tipo_disco = 1;
    tamano_disco = 256;

    printf("*******************GENERANDO NUEVO DISCO**********************\n");
    printf("****************************DATOS*****************************\n");
    printf("Nombre:%s\n", nombre_disco);
    printf("Directorio:%s\n", directorio_disco);
    printf("tamño:%i", tamano_disco);
    if (tipo_disco == 1) {
        printf("(Mb)\n");
    } else {
        printf("(Kb)\n");
    }
    printf("**************************GENERANDO***************************\n");
    generar_disco(nombre_disco, directorio_disco, tamano_disco, tipo_disco);

    /* CREAR DISCO 2 */
    /*
    char nombre_disco2[MAXLON + 1];
    char directorio_disco2[MAXLON + 1];
    int tipo_disco2 = 1;
    int tamano_disco2 = -1;

    clean_string(nombre_disco2, MAXLON);
    clean_string(directorio_disco2, MAXLON);

    strcpy(nombre_disco2, "prueba_disco2");
    strcpy(directorio_disco2, "/home/son/MIA/Discos/");
    tipo_disco2 = 1;
    tamano_disco2 = 256;

    printf("*******************GENERANDO NUEVO DISCO**********************\n");
    printf("****************************DATOS*****************************\n");
    printf("Nombre:%s\n", nombre_disco2);
    printf("Directorio:%s\n", directorio_disco2);
    printf("tamño:%i", tamano_disco2);
    if (tipo_disco2 == 1) {
        printf("(Mb)\n");
    } else {
        printf("(Kb)\n");
    }
    printf("**************************GENERANDO***************************\n");
    generar_disco(nombre_disco2, directorio_disco2, tamano_disco2, tipo_disco2);
     */
    /* CREAR PARTICION 1 */

    char nombre_particion[MAXLON + 1];
    char directorio_particion[MAXLON + 1];
    int tamano_particion;
    int unit_particion;
    int tipo_particion;

    clean_string(nombre_particion, MAXLON);
    clean_string(directorio_particion, MAXLON);

    strcpy(nombre_particion, "p_prueba");
    strcpy(directorio_particion, "/home/son/MIA/Discos/prueba_disco.dsk");
    tamano_particion = 128;
    unit_particion = 1;
    tipo_particion = 1;

    crear_pp(nombre_particion, directorio_particion, tamano_particion, unit_particion);

    /* CREAR PARTICION 2 */
    /*
    char nombre_particion2[MAXLON + 1];
    char directorio_particion2[MAXLON + 1];
    int tamano_particion2;
    int unit_particion2;
    int tipo_particion2;

    clean_string(nombre_particion2, MAXLON);
    clean_string(directorio_particion2, MAXLON);

    strcpy(nombre_particion2, "p_prueba2");
    strcpy(directorio_particion2, "/home/son/MIA/Discos/prueba_disco.dsk");
    tamano_particion2 = 64;
    unit_particion2 = 1;
    tipo_particion2 = 1;
    
    crear_pp(nombre_particion2, directorio_particion2, tamano_particion2, unit_particion2);
     */
    /* CREAR PARTICION 3 */
    /*
    char nombre_particion3[MAXLON + 1];
    char directorio_particion3[MAXLON + 1];
    int tamano_particion3;
    int unit_particion3;
    int tipo_particion3;

    clean_string(nombre_particion3, MAXLON);
    clean_string(directorio_particion3, MAXLON);

    strcpy(nombre_particion3, "p_prueba3");
    strcpy(directorio_particion3, "/home/son/MIA/Discos/prueba_disco2.dsk");
    tamano_particion3 = 128;
    unit_particion3 = 1;
    tipo_particion3 = 1;

    crear_pp(nombre_particion3, directorio_particion3, tamano_particion3, unit_particion3);
     */
    /* CREAR PARTICION 4 */
    /*
    char nombre_particion4[MAXLON + 1];
    char directorio_particion4[MAXLON + 1];
    int tamano_particion4;
    int unit_particion4;
    int tipo_particion4;

    clean_string(nombre_particion4, MAXLON);
    clean_string(directorio_particion4, MAXLON);

    strcpy(nombre_particion4, "p_prueba4");
    strcpy(directorio_particion4, "/home/son/MIA/Discos/prueba_disco.dsk");
    tamano_particion4 = 32;
    unit_particion4 = 1;
    tipo_particion4 = 1;

    crear_pp(nombre_particion4, directorio_particion4, tamano_particion4, unit_particion4);
     */
    /* MONTAR PARTICION 1 */

    char nombre_montaje[MAXLON + 1];
    char directorio_montaje[MAXLON + 1];

    clean_string(nombre_montaje, MAXLON);
    clean_string(directorio_montaje, MAXLON);

    strcpy(nombre_montaje, "p_prueba");
    strcpy(directorio_montaje, "/home/son/MIA/Discos/prueba_disco.dsk");

    mount_c(montajes, directorio_montaje, nombre_montaje);

    /* MONTAR PARTICION 2 */
    /*
    char nombre_montaje2[MAXLON + 1];
    char directorio_montaje2[MAXLON + 1];

    clean_string(nombre_montaje2, MAXLON);
    clean_string(directorio_montaje2, MAXLON);

    strcpy(nombre_montaje2, "p_prueba2");
    strcpy(directorio_montaje2, "/home/son/MIA/Discos/prueba_disco.dsk");

    mount_c(montajes, directorio_montaje2, nombre_montaje2);
     */
    /* MONTAR PARTICION 3 */
    /*
    char nombre_montaje3[MAXLON + 1];
    char directorio_montaje3[MAXLON + 1];

    clean_string(nombre_montaje3, MAXLON);
    clean_string(directorio_montaje3, MAXLON);

    strcpy(nombre_montaje3, "p_prueba3");
    strcpy(directorio_montaje3, "/home/son/MIA/Discos/prueba_disco2.dsk");

    mount_c(montajes, directorio_montaje3, nombre_montaje3);
     */
    /* DESMONTAR PARTICION 1 */
    /*
    char id_particion[MAXLON+1];
    
    clean_string(id_particion,MAXLON);
    
    strcpy(id_particion,"vda1");
    
    umount_c(montajes, id_particion);
     */
    /* MONTAR PARTICION 4 */
    /*
    char nombre_montaje4[MAXLON + 1];
    char directorio_montaje4[MAXLON + 1];

    clean_string(nombre_montaje4, MAXLON);
    clean_string(directorio_montaje4, MAXLON);

    strcpy(nombre_montaje4, "p_prueba4");
    strcpy(directorio_montaje4, "/home/son/MIA/Discos/prueba_disco.dsk");

    mount_c(montajes, directorio_montaje4, nombre_montaje4);
     */
    /* FORMATEAR PARTICION 1 (vda1)[FULL] */

    char id_formato[MAXLON + 1];
    char type_formato; // a || u

    clean_string(id_formato, MAXLON);

    strcpy(id_formato, "vda1");
    type_formato = 'u';

    formatear_mkfs(montajes, id_formato, type_formato);

}

void Test1(Montaje *Montajes, Sesion *sesion) {

    char id_login[MAXLON + 1];
    char username[MAXLON + 1];
    char password[MAXLON + 1];

    /* MONTAR PARTICION 1 */
    /*
    char nombre_montaje[MAXLON + 1];
    char directorio_montaje[MAXLON + 1];

    clean_string(nombre_montaje, MAXLON);
    clean_string(directorio_montaje, MAXLON);

    strcpy(nombre_montaje, "p_prueba");
    strcpy(directorio_montaje, "/home/son/MIA/Discos/prueba_disco.dsk");

    mount_c(Montajes, directorio_montaje, nombre_montaje);
    
     */
    /* LOGIN 1234 NO USER */
    /*
    clean_string(id_login,MAXLON);
    clean_string(username,MAXLON);
    clean_string(password,MAXLON);
    
    strncpy(id_login,"vda1",4);
    strncpy(username,"1234",4);
    strncpy(password,"201403775",9);
    
    login(sesion, Montajes, id_login, username,password);
     */
    /* LOGIN root wrong password */
    /*
    clean_string(id_login,MAXLON);
    clean_string(username,MAXLON);
    clean_string(password,MAXLON);
    
    strncpy(id_login,"vda1",4);
    strncpy(username,"root",4);
    strncpy(password,"201403778",9);
    
    login(sesion, Montajes, id_login, username,password);
     */
    /* LOGOUT FAIL */
    /*
    logout(sesion);
     */

    /* LOGIN ROOT */

    clean_string(id_login, MAXLON);
    clean_string(username, MAXLON);
    clean_string(password, MAXLON);

    strncpy(id_login, "vda1", 4);
    strncpy(username, "root", 4);
    strncpy(password, "201403775", 9);

    login(sesion, Montajes, id_login, username, password);

    /* LOGOUT ROOT */
    /*
    logout(sesion);
     */

    /* PERMISOS PARA PODER EJECUTAR CREACION E ELIMINACION DE USUARIOS Y GRUPOS */
    /*
    if(sesion->UID!=1){
        printf("ERROR: Creacion de Usuario Denegado.\n");
        return;
    }
     */

    /* CREACION DE USUARIO */

    char new_user[MAXLON + 1];
    char new_pass[MAXLON + 1];
    char user_grp[MAXLON + 1];

    clean_string(new_user, MAXLON);
    clean_string(new_pass, MAXLON);
    clean_string(user_grp, MAXLON);
    clean_string(id_login, MAXLON);

    strncpy(id_login, "vda1", 4);
    strncpy(new_user, "Edwin", 5);
    strncpy(new_pass, "Alfonzo", 7);
    strncpy(user_grp, "root", 4);

    crear_usuario(Montajes, new_user, user_grp, new_pass, id_login);
    /* ELIMINAR USUARIO */
    eliminar_usuario(Montajes, new_user, id_login);
    /* REACTIVAR USUARIO EDWIN */
    crear_usuario(Montajes, new_user, user_grp, new_pass, id_login);
    /* ERROR: USUARIO YA EXISTE */
    crear_usuario(Montajes, new_user, user_grp, new_pass, id_login);
    /* ERROR: GRUPO NO EXISTE */
    clean_string(new_user, MAXLON);
    clean_string(new_pass, MAXLON);
    clean_string(user_grp, MAXLON);
    clean_string(id_login, MAXLON);

    strncpy(id_login, "vda1", 4);
    strncpy(new_user, "Mercy", 5);
    strncpy(new_pass, "mercy", 7);
    strncpy(user_grp, "raat", 4);
    crear_usuario(Montajes, new_user, user_grp, new_pass, id_login);

    /* CREAR GRUPO raat */

    char grp_name[MAXLON + 1];
    clean_string(grp_name, MAXLON);
    strncpy(grp_name, "raat", 4);

    crear_grupo(Montajes, grp_name, id_login);

    /* CREAR USUARIO GRUPO raat YA EXISTE */
    clean_string(new_user, MAXLON);
    clean_string(new_pass, MAXLON);
    clean_string(user_grp, MAXLON);
    clean_string(id_login, MAXLON);

    strncpy(id_login, "vda1", 4);
    strncpy(new_user, "Mercy", 5);
    strncpy(new_pass, "mercy", 7);
    strncpy(user_grp, "raat", 4);
    crear_usuario(Montajes, new_user, user_grp, new_pass, id_login);

    /* ELIMINAR GRUPO raat */

    clean_string(grp_name, MAXLON);
    strncpy(grp_name, "raat", 4);

    eliminar_grupo(Montajes, grp_name, id_login);

    /* CREAR GRUPO raat */

    clean_string(grp_name, MAXLON);
    strncpy(grp_name, "raat", 4);

    crear_grupo(Montajes, grp_name, id_login);

    /* CREAR CARPETA /user */
    crear_Directorio(Montajes, sesion, id_login, "/home");
    crear_Directorio(Montajes, sesion, id_login, "/home/son");

    char str[50];
    for (int i = 1; i < 101; i++) {
        snprintf(str, 50, "/home_%d", i);
        crear_Directorio(Montajes, sesion, id_login, str);
    }


    for (int j = 1; j < 7; j = j + 2) {
        for (int i = 1; i < 11; i++) {
            snprintf(str, 50, "/home_%d/son_%d", j, i);
            crear_Directorio(Montajes, sesion, id_login, str);
        }
    }

    for (int i = 21; i < 101; i++) {
        snprintf(str, 50, "/home_%i", i);
        eliminar_Directorio(Montajes, sesion, id_login, str);
    }

    //    for (int i = 50; i < 92; i++) {
    //        snprintf(str, 50, "/home_%d", i);
    //        eliminar_Directorio(Montajes, sesion, id_login, str);
    //    }
    //    


    char contenido2[100];
    char numero[10] = "0123456789";
    clean_string(contenido2, 100);
    for (int i = 1; i < 10; i++) {
        strncat(contenido2, numero, 10);
    }
    crear_Archivo(Montajes, sesion, id_login, "/home_1/prueba1.txt", contenido2);
    crear_Archivo(Montajes, sesion, id_login, "/home_1/son_5/prueba2.txt", contenido2);
    crear_Archivo(Montajes, sesion, id_login, "/home_1/son_10/prueba3.txt", contenido2);
    crear_Archivo(Montajes, sesion, id_login, "/home_1/prueba4.txt", contenido2);



    //    char nombre_archivo[100];
    //    clean_string(nombre_archivo,100);
    //    for (int i = 1; i < 100; i++) {
    //        snprintf(nombre_archivo, 100, "/home_16/prueba%d.txt", i);
    //        crear_Archivo(Montajes,sesion, id_login,nombre_archivo,contenido2);
    //    }
    //    
    /* RENOMBRAR ARCHIVO */

    update_Archivo(Montajes, sesion, id_login, "/home_1/son_10/prueba3.txt", "cambio.txt");

    copiar_elemento(Montajes, sesion, id_login, "/home/son", "/home_1");
    copiar_elemento(Montajes, sesion, id_login, "/home_2", "/home_1/prueba1.txt");

    update_carpeta(Montajes, sesion, id_login, "/home/son/home_1", "nuevo_home");

    //    modificar_Archivo(Montajes, sesion, id_login, "/home_1/son_10/cambio.txt");

    cambiar_permisos(Montajes, sesion, id_login, "/home/son/nuevo_home", "111");

    cambiar_propetario(Montajes, sesion, id_login, "/home/son/nuevo_home", "Mercy");
    cambiar_propetario(Montajes, sesion, id_login, "/home_1/son_10/cambio.txt", "Mercy");

    cambiar_grupo(Montajes, "root", id_login, "Mercy");

    mover_elemento(Montajes, sesion, id_login, "/home_5/son_1", "/home_5/son_2");
    mover_elemento(Montajes, sesion, id_login, "/home_5/son_1", "/home_5/son_3");
    mover_elemento(Montajes, sesion, id_login, "/home_5/son_1", "/home_5/son_4");
    mover_elemento(Montajes, sesion, id_login, "/home_5/son_1", "/home_5/son_5");
    mover_elemento(Montajes, sesion, id_login, "/home_5/son_1", "/home_1/son_10/cambio.txt");
}

void Test_Reportes(Montaje *montajes) {

    /* MONTAR PARTICION 1 */

    char nombre_montaje[MAXLON + 1];
    char directorio_montaje[MAXLON + 1];

    clean_string(nombre_montaje, MAXLON);
    clean_string(directorio_montaje, MAXLON);

    strcpy(nombre_montaje, "p_prueba");
    strcpy(directorio_montaje, "/home/son/MIA/Discos/prueba_disco.dsk");

    mount_c(montajes, directorio_montaje, nombre_montaje);

    /* REPORTE BITMAP-INODO */

    char report1[MAXLON + 1];
    char report_dire1[MAXLON + 1];

    clean_string(report1, MAXLON);
    clean_string(report_dire1, MAXLON);

    strcpy(report1, "vda1");
    strcpy(report_dire1, "/home/son/MIA/Reportes/bm_inodo");

    rep_bm_inode(montajes, report1, report_dire1);

    /* REPORTE BITMAP-BLOQUES */

    char report2[MAXLON + 1];
    char report_dire2[MAXLON + 1];

    clean_string(report2, MAXLON);
    clean_string(report_dire2, MAXLON);

    strcpy(report2, "vda1");
    strcpy(report_dire2, "/home/son/MIA/Reportes/bm_block");

    rep_bm_block(montajes, report2, report_dire2);

    /* REPORTE JOURNALING */

    char report3[MAXLON + 1];
    char report_dire3[MAXLON + 1];

    clean_string(report3, MAXLON);
    clean_string(report_dire3, MAXLON);

    strcpy(report3, "vda1");
    strcpy(report_dire3, "/home/son/MIA/Reportes/Journaling");

    rep_journaling(montajes, report3, report_dire3);

    /* REPORTE INODE */

    char report4[MAXLON + 1];
    char report_dire4[MAXLON + 1];

    clean_string(report4, MAXLON);
    clean_string(report_dire4, MAXLON);

    strcpy(report4, "vda1");
    strcpy(report_dire4, "/home/son/MIA/Reportes/inode");

    rep_inode(montajes, report4, report_dire4);

    /* REPORTE BLOCK */

    char report5[MAXLON + 1];
    char report_dire5[MAXLON + 1];

    clean_string(report5, MAXLON);
    clean_string(report_dire5, MAXLON);

    strcpy(report5, "vda1");
    strcpy(report_dire5, "/home/son/MIA/Reportes/block");

    rep_block(montajes, report5, report_dire5);

    /* REPORTE SB */

    char report6[MAXLON + 1];
    char report_dire6[MAXLON + 1];

    clean_string(report6, MAXLON);
    clean_string(report_dire6, MAXLON);

    strcpy(report6, "vda1");
    strcpy(report_dire6, "/home/son/MIA/Reportes/sb");

    rep_sb(montajes, report6, report_dire6);

    /* REPORTE Directorio */

    char report7[MAXLON + 1];
    char report_dire7[MAXLON + 1];

    clean_string(report7, MAXLON);
    clean_string(report_dire7, MAXLON);

    strcpy(report7, "vda1");
    strcpy(report_dire7, "/home/son/MIA/Reportes/Direcotior1");

    rep_Directorio(montajes, report7, report_dire7);

    /* REPORTE Directorio 2 */

    char path_mostrar[MAXLON + 1];

    clean_string(path_mostrar, MAXLON);

    strcpy(report_dire7, "/home/son/MIA/Reportes/Direcotior2");
    strcpy(path_mostrar, "/home_15/prueba.txt");

    rep_Directorio2(montajes, report7, report_dire7, path_mostrar);


    /* REPORTE Directorio 3 */

    clean_string(report_dire7, MAXLON);
    strcpy(report_dire7, "/home/son/MIA/Reportes/Direcotior_Completo");
    rep_Sistema(montajes, report7, report_dire7);

    /* REPORTE Directorio 4 */


    clean_string(path_mostrar, MAXLON);
    clean_string(report_dire7, MAXLON);

    strcpy(path_mostrar, "/home_16");
    strcpy(report_dire7, "/home/son/MIA/Reportes/Directorio4");

    rep_Directorio3(montajes, report7, report_dire7, path_mostrar);

}

void correr_Test(Montaje *montajes, Sesion *sesion) {

    Test(montajes);
    Test1(montajes, sesion);
    Test_Reportes(montajes);


}

void menu_crear_disco() {

    char nombre[MAXLON + 1];
    char directorio[MAXLON + 1];
    int tipo = -1;
    int tamano = -1;

    clean_string(nombre, MAXLON);
    clean_string(directorio, MAXLON);

    printf("INSERTE EL NOMBRE DEL DISCO:\n");
    get_string(nombre);
    printf("INSERTE EL DIRECTORIO DONDE SE CREARA EL DISCO:\n");
    get_string(directorio);
    printf("INSERTE EL TAMAÑO DEL DISCO A CREAR:\n");
    get_number(&tamano);
    while (1) {
        print_menu(6);
        get_number(&tipo);

        if (tipo > 2 || tipo < 1) {
            printf("opcion invalida\n");
        } else {
            break;
        }
    }

    printf("*******************GENERANDO NUEVO DISCO**********************\n");
    printf("****************************DATOS*****************************\n");
    printf("Nombre:%s\n", nombre);
    printf("Directorio:%s\n", directorio);
    printf("tamño:%i", tamano);
    if (tipo == 1) {
        printf("(Mb)\n");
    } else {
        printf("(Kb)\n");
    }
    printf("**************************GENERANDO***************************\n");
    generar_disco(nombre, directorio, tamano, tipo);

}

void menu_eliminar_disco(Montaje *montajes) {

    char directorio[MAXLON + 1];

    clean_string(directorio, MAXLON);

    printf("INSERTE EL DIRECTORIO DONDE SE ELIMNARA EL DISCO:\n");
    get_string(directorio);

    printf("**********************ELIMINANDO DISCO************************\n");
    printf("****************************DATOS*****************************\n");
    printf("Directorio:%s\n", directorio);
    printf("*************************ELIMINANDO**************************\n");
    destruir_disco(montajes, directorio);
}

void menu_crear_particion() {

    char nombre[MAXLON + 1];
    char directorio[MAXLON + 1];
    int tamano;
    int unit;
    int tipo_particion;

    clean_string(nombre, MAXLON);
    clean_string(directorio, MAXLON);

    while (1) {
        print_menu(7);
        get_number(&tipo_particion);
        if (tipo_particion > 3 && tipo_particion < 1) {
            printf("Error: el valor ingresado no es valido.\n");
        } else {
            break;
        }
    }

    while (1) {
        print_menu(8);
        get_number(&unit);
        if (unit > 3 && unit < 1) {
            printf("Error: el valor ingresado no es valido.\n");
        } else {
            break;
        }
    }

    int tamano_p;

    printf("INSERTE EL TAMAÑO DE LA PARTICION A CREAR:\n");
    while (1) {
        get_number(&tamano);
        switch (unit) {
            case 1:
                tamano_p = tamano * 1024 * 1024;
                break;
            case 2:
                tamano_p = tamano * 1024;
                break;
            case 3:
                tamano_p = tamano;
                break;
        }
        if (tamano_p % 8 != 0) {
            printf("Error: el valor ingresado no es valido, deve ser multiplo de 8 el valor del tamaño.\n");
        } else {
            break;
        }
    }

    printf("INSERTE EN NOMBRE DE LA NUEVA PARTICION");
    int longitud;
    while (1) {
        get_string(nombre);
        longitud = string_lenght(nombre);
        if (longitud > 16 && longitud < 1) {
            printf("Error: el nombre no deve ser nulo o contener mas de 16 caracteres.'n");
        } else {
            break;
        }
    }

    printf("INSERTE EL LA DIRECCION ABSOLUTA DE LA UBICACION DEL DISCO EN DONDE SE CREARA LA PARTICION.\n");
    get_string(directorio);

    switch (tipo_particion) {
        case 1:
            crear_pp(nombre, directorio, tamano, unit);
            break;
        case 2:
            crear_pe(nombre, directorio, tamano, unit);
            break;
        case 3:
            crear_pl(nombre, directorio, tamano, unit);
            break;
    }

}

void menu_montar_particion(Montaje *montajes) {

    char nombre_montaje[MAXLON + 1];
    char directorio_montaje[MAXLON + 1];

    clean_string(nombre_montaje, MAXLON);
    clean_string(directorio_montaje, MAXLON);

    printf("Ingrese el nombre de la particion a montar:\n");
    get_string(nombre_montaje);

    printf("Ingrese el directorio donde se encuentra el disco que contiene la particion \"%s\":\n", nombre_montaje);
    get_string(directorio_montaje);

    mount_c(montajes, directorio_montaje, nombre_montaje);

}

void menu_desmontar_particion(Montaje *montajes) {

    char id_particion[MAXLON + 1];

    clean_string(id_particion, MAXLON);

    printf("Ingrese el punto de montaje de la particion que desea desmontar:\n");
    get_string(id_particion);

    umount_c(montajes, id_particion);

}

void menu_formatear_particion(Montaje *montajes) {

    char id_formato[MAXLON + 1];
    char type_formato; // a || u

    clean_string(id_formato, MAXLON);

    printf("Ingresa el punto de montaje de la particion que deseas formatear");
    get_string(id_formato);

    int opcion = -1;

    while (opcion != 1 || opcion != 2) {
        print_menu(9);
        get_number(&opcion);
    }

    if (opcion == 1) {
        type_formato = 'a';
    } else {
        type_formato = 'u';
    }

    formatear_mkfs(montajes, id_formato, type_formato);

}

void menu_login(Montaje *montajes, Sesion *sesion) {

    char id_login[MAXLON + 1];
    char username[MAXLON + 1];
    char password[MAXLON + 1];

    clean_string(id_login, MAXLON);
    clean_string(username, MAXLON);
    clean_string(password, MAXLON);

    printf("Ingresa el punto de montaje de la particion donde deseas iniciar sesion.\n");
    get_string(id_login);

    printf("Ingresa el tu nombre de usuario.\n");
    get_string(username);

    printf("Ingresa la contraseña.\n");
    get_string(password);

    login(sesion, montajes, id_login, username, password);

}

void menu_logout(Sesion *sesion) {
    logout(sesion);
}

void menu_crear_grupo(Montaje *montajes, Sesion *sesion) {

    if (sesion->UID != ROOT_ID) {
        printf("ERROR: ESTE COMANDO SOLO LO PUEDE UTILIZAR EL USUARIO \"root\".\n");
        return;
    }

    char grp_name[MAXLON + 1];
    char id_login[MAXLON + 1];

    clean_string(grp_name, MAXLON);
    clean_string(id_login, MAXLON);

    printf("Ingrese el punto de montaje de la particion donde desea que se cree el nuevo grupo.\n");
    get_string(grp_name);

    printf("Ingrese el nombre del grupo a crear\n");
    get_string(grp_name);

    crear_grupo(montajes, grp_name, id_login);

}

void menu_eliminar_grupo(Montaje *montajes, Sesion *sesion) {

    if (sesion->UID != ROOT_ID) {
        printf("ERROR: ESTE COMANDO SOLO LO PUEDE UTILIZAR EL USUARIO \"root\".\n");
        return;
    }

    char grp_name[MAXLON + 1];
    char id_login[MAXLON + 1];

    clean_string(grp_name, MAXLON);
    clean_string(id_login, MAXLON);

    printf("Ingrese el punto de montaje de la particion donde desea que se cree el nuevo grupo.\n");
    get_string(grp_name);

    printf("Ingrese el nombre del grupo a eliminar\n");
    get_string(grp_name);


    eliminar_grupo(montajes, grp_name, id_login);


}

void menu_crear_usuario(Montaje *montajes, Sesion *sesion) {

    char new_user[MAXLON + 1];
    char new_pass[MAXLON + 1];
    char user_grp[MAXLON + 1];
    char id_login[MAXLON + 1];

    clean_string(new_user, MAXLON);
    clean_string(new_pass, MAXLON);
    clean_string(user_grp, MAXLON);
    clean_string(id_login, MAXLON);

    printf("Ingrese el punto de montado de la particion en donde quiere crear un nuevo usuario\n");
    get_string(id_login);

    printf("Ingrese el nombre del nuevo usuario\n");
    get_string(new_user);

    printf("Ingrese la contraseña del nuevo usuario\n");
    get_string(new_pass);

    printf("Ingrese el nombre del grupo al que pertenesera el nuevo usuario\n");
    get_string(user_grp);

    crear_usuario(montajes, new_user, user_grp, new_pass, id_login);

}

void menu_eliminar_usuario(Montaje *montajes, Sesion *sesion) {

    char new_user[MAXLON + 1];
    char id_login[MAXLON + 1];

    clean_string(new_user, MAXLON);
    clean_string(new_user, MAXLON);

    printf("Ingrese el punto de montado de la particion en donde quiere crear un nuevo usuario\n");
    get_string(id_login);

    printf("Ingrese el nombre del usuario a eliminar\n");
    get_string(new_user);

    eliminar_usuario(montajes, new_user, id_login);
}

void menu_cambiar_permisos(Montaje *montajes, Sesion *sesion) {

    char id_login[MAXLON + 1];
    char elemento[MAXLON + 1];
    char permisos[MAXLON + 1];

    clean_string(id_login, MAXLON);
    clean_string(elemento, MAXLON);
    clean_string(permisos, MAXLON);

    printf("Ingrese el punto de montaje de la particion en la que desea efectuar la operacion\n");
    get_string(id_login);

    printf("Ingrese la ruta de elemento al que desea cambiar los permisos\n");
    get_string(elemento);

    printf("Ingrese los permisos a aplicar\n");
    get_permisos(permisos);

    cambiar_permisos(montajes, sesion, id_login, elemento, permisos);

}

void menu_crear_archivo(Montaje *montajes, Sesion *sesion) {


    char id_login[MAXLON + 1];
    char nuevo_archivo[MAXLON + 1];
    char contenido[MAXLON + 1];

    clean_string(id_login, MAXLON);
    clean_string(nuevo_archivo, MAXLON);
    clean_string(contenido, MAXLON);

    printf("Ingresar punto de montaje de la particion\n");
    get_string(id_login);

    printf("Ingresa el nombre del archivo junto con la direccion que tiene (ej. /home/son/doc.txt )\n");
    get_string(nuevo_archivo);

    printf("Escriba el contenido del archivo, cuando termine, precione ENTER\n");
    get_string(contenido);

    int longitud = strlen(contenido);

    if (longitud > (MAX_DIREC_APUNT * MAX_BD_CONT_SIZE + MAX_BD_APUNTS * MAX_BD_CONT_SIZE + MAX_BD_APUNTS * MAX_BD_APUNTS * MAX_BD_CONT_SIZE)) {
        printf("ERROR: la longitud que ha ingresado es mayor a la que el sistema de archivos puede manejar");
        return;
    }

    crear_Archivo(montajes, sesion, id_login, nuevo_archivo, contenido);

}

void menu_crear_directorio(Montaje *montajes, Sesion *sesion) {

    char id_login[MAXLON + 1];
    char elemento[MAXLON + 1];

    clean_string(id_login, MAXLON);
    clean_string(elemento, MAXLON);

    printf("ingrese el punto de montaje en el cual desea crear la carpeta\n");
    get_string(id_login);

    printf("ingrese el path de la carpeta (ej. /home/son/carpeta1/carpeta1_1 ) - no agrege una ultima diagonal \"/\"\n");
    get_string(elemento);

    crear_Directorio(montajes, sesion, id_login, elemento);
}

void menu_mostrar_contenido(Montaje *montajes, Sesion *sesion) {


    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo.txt )\n");
    get_string(path);

    mostrar_Archivo(montajes, sesion, id, path);

}

void menu_eliminar_archivo(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo.txt )\n");
    get_string(path);

    eliminar_Archivo(montajes, sesion, id, path);
}

void menu_eliminar_carpeta(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo.txt )\n");
    get_string(path);

    eliminar_Directorio(montajes, sesion, id, path);
}

void menu_editar_archivo(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo.txt )\n");
    get_string(path);

    modificar_Archivo(montajes, sesion, id, path);

}

void menu_renombrar_archivo(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];
    char nombre[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);
    clean_string(nombre, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo.txt )\n");
    get_string(path);

    printf("ingrese el nuevo nombre del archivo\n");
    get_string(nombre);

    update_Archivo(montajes, sesion, id, path, nombre);

}

void menu_renombrar_carpeta(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];
    char nombre[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);
    clean_string(nombre, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde esta el archivo (ej. /home/son/archivo )\n");
    get_string(path);

    printf("ingrese el nuevo nombre del archivo\n");
    get_string(nombre);

    update_carpeta(montajes, sesion, id, path, nombre);

}

void menu_copiar(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char destino[MAXLON + 1];
    char elemento[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(destino, MAXLON);
    clean_string(elemento, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde se copiara el elemento (ej. /home/son/archivo )\n");
    get_string(destino);

    printf("ingrese la direccion en donde se ubica el elemento\n");
    get_string(elemento);

    copiar_elemento(montajes, sesion, id, destino, elemento);

}

void menu_mover(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char destino[MAXLON + 1];
    char elemento[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(destino, MAXLON);
    clean_string(elemento, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde se movera el elemento (ej. /home/son/archivo )\n");
    get_string(destino);

    printf("ingrese la direccion en donde se ubica el elemento\n");
    get_string(elemento);

    mover_elemento(montajes, sesion, id, destino, elemento);

}

void menu_cambiar_propetario(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char nombre[MAXLON + 1];
    char elemento[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(nombre, MAXLON);
    clean_string(elemento, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese la direccion en donde se ubica el elemento\n");
    get_string(elemento);

    printf("ingrese el nombre del nuevo propetario\n");
    get_string(nombre);

    cambiar_propetario(montajes, sesion, id, elemento, nombre);

}

void menu_cambiar_grupo(Montaje *montajes, Sesion *sesion) {

    char id[MAXLON + 1];
    char usuario[MAXLON + 1];
    char grupo[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(usuario, MAXLON);
    clean_string(grupo, MAXLON);

    printf("ingrese el punto de montaje en donde se localiza el archivo\n");
    get_string(id);

    printf("ingrese el nombre del usuario al cual se le hara el cambio\n");
    get_string(usuario);

    printf("ingrese el nombre del grupo al que pertenecera el usuario\n");
    get_string(grupo);

    cambiar_grupo(montajes, grupo, id, usuario);

}

void menu_rep_bm_block(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_bm_block(montaje, id, path);
}

void menu_rep_bm_inode(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_bm_inode(montaje, id, path);
}

void menu_rep_journaling(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_journaling(montaje, id, path);
}

void menu_rep_inode(Montaje *montaje) {
    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_inode(montaje, id, path);
}

void menu_rep_block(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_block(montaje, id, path);
}

void menu_rep_sb(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_sb(montaje, id, path);
}

void menu_rep_Directorio(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_Directorio(montaje, id, path);
}

void menu_rep_Directorio2(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];
    char path_mostrar[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);
    clean_string(path_mostrar, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    printf("ingrese la direccion del elemento que desee que se muestre ( ej. /home/son/reporte )\n");
    get_string(path_mostrar);

    rep_Directorio2(montaje, id, path, path_mostrar);
}

void menu_rep_Directorio3(Montaje *montaje) {

    char id[MAXLON + 1];
    char path[MAXLON + 1];
    char path_mostrar[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);
    clean_string(path_mostrar, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    printf("ingrese la direccion del elemento que desee que se muestre ( ej. /home/son/reporte )\n");
    get_string(path_mostrar);

    rep_Directorio3(montaje, id, path, path_mostrar);
}

void menu_rep_Sistema(Montaje *montaje) {


    char id[MAXLON + 1];
    char path[MAXLON + 1];

    clean_string(id, MAXLON);
    clean_string(path, MAXLON);

    printf("ingrese el punto de montado de la particion\n");
    get_string(id);

    printf("ingrese la direccion de su computador en donde quiere que se almacene el reporte ( ej. /home/son/reporte )\n");
    get_string(path);

    rep_Sistema(montaje, id, path);
}