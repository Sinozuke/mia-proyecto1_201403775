#ifndef MENU_PARAMETERS_H
#define MENU_PARAMETERS_H

#include "main_data.h"

void correr_Test(Montaje *montajes, Sesion *sesion);

void menu_crear_disco();
void menu_eliminar_disco(Montaje *montajes);
void menu_crear_particion();
void menu_montar_particion(Montaje *montajes);
void menu_desmontar_particion(Montaje *montajes);
void menu_formatear_particion(Montaje *montajes);
void menu_login(Montaje *montajes, Sesion *sesion);
void menu_logout(Sesion *sesion);
void menu_crear_grupo(Montaje *montajes, Sesion *sesion);
void menu_eliminar_grupo(Montaje *montajes, Sesion *sesion);
void menu_crear_usuario(Montaje *montajes, Sesion *sesion);
void menu_eliminar_usuario(Montaje *montajes, Sesion *sesion);
void menu_cambiar_permisos(Montaje *montajes, Sesion *sesion);
void menu_crear_archivo(Montaje *montajes, Sesion *sesion);
void menu_crear_directorio(Montaje *montajes, Sesion *sesion);
void menu_mostrar_contenido(Montaje *montajes, Sesion *sesion);
void menu_eliminar_archivo(Montaje *montajes, Sesion *sesion);
void menu_eliminar_carpeta(Montaje *montajes, Sesion *sesion);
void menu_editar_archivo(Montaje *montajes, Sesion *sesion);
void menu_renombrar_archivo(Montaje *montajes, Sesion *sesion);
void menu_renombrar_carpeta(Montaje *montajes, Sesion *sesion);
void menu_copiar(Montaje *montajes, Sesion *sesion);
void menu_mover(Montaje *montajes, Sesion *sesion);
void menu_cambiar_propetario(Montaje *montajes, Sesion *sesion);
void menu_cambiar_grupo(Montaje *montajes, Sesion *sesion);
void menu_rep_bm_block(Montaje *montaje);
void menu_rep_bm_inode(Montaje *montaje);
void menu_rep_journaling(Montaje *montaje);
void menu_rep_inode(Montaje *montaje);
void menu_rep_block(Montaje *montaje);
void menu_rep_sb(Montaje *montaje);
void menu_rep_Directorio(Montaje *montaje);
void menu_rep_Directorio2(Montaje *montaje);
void menu_rep_Directorio3(Montaje *montaje);
void menu_rep_Sistema(Montaje *montaje);

#endif /* MENU_PARAMETERS_H */

