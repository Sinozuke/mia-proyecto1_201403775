#ifndef DISCO_H
#define DISCO_H

#include "main_data.h"

void generar_disco(char *nombre, char *path, int tamano, int tipo);
void destruir_disco(Montaje *montajes, char *path);
/*
void aumentar_disco();
void reducir_disco();
*/
#endif /* DISCO_H */

