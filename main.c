#include <stdio.h>
#include <stdlib.h>
#include "main_data.h"
#include "main_functions.h"
#include "Menu_parameters.h"

Montaje montaje[26];
Sesion sesion = {-1, -1, -1, -1};

int main() {

    int opcion=-1;
    print_welcome();
        
    while(1){
        print_menu(1);
        get_number(&opcion);
        switch(opcion){
            case 0://salida de la aplicacion
                return (EXIT_SUCCESS);    
            case 1:
                menu_crear_disco();
                break;
            case 2:
                menu_eliminar_disco(montaje);
                break;
            case 5:
                menu_crear_particion();
                break;
            case 6:
                menu_montar_particion(montaje);
                break;
            case 7:
                menu_desmontar_particion(montaje);
                break;
            case 8:
                menu_formatear_particion(montaje);
                break;
            case 9:
                menu_login(montaje,&sesion);
                break;
            case 10:
                menu_logout(&sesion);
                break;
            case 11:
                menu_crear_grupo(montaje, &sesion);
                break;
            case 12:
                menu_eliminar_grupo(montaje,&sesion);
                break;
            case 13:
                menu_crear_usuario(montaje, &sesion);
                break;
            case 14:
                menu_eliminar_usuario(montaje, &sesion);
                break;
            case 15:
                menu_cambiar_permisos(montaje, &sesion);
                break;
            case 16:
                menu_crear_archivo(montaje,&sesion);
                break;
            case 17:
                menu_crear_directorio(montaje,&sesion);
                break;
            case 18:
                menu_mostrar_contenido(montaje,&sesion);
                break;
            case 19:
                menu_eliminar_archivo(montaje,&sesion);
                break;
            case 20:
                menu_eliminar_carpeta(montaje,&sesion);
                break;
            case 21:
                menu_editar_archivo(montaje,&sesion);
                break;
            case 22:
                menu_renombrar_archivo(montaje,&sesion);
                break;
            case 23:
                menu_renombrar_carpeta(montaje,&sesion);
                break;
            case 24:
                menu_copiar(montaje,&sesion);
                break;
            case 25:
                menu_mover(montaje,&sesion);
                break;
            case 26:
                menu_cambiar_propetario(montaje,&sesion);
                break;
            case 27:
                menu_cambiar_grupo(montaje,&sesion);
                break;
            case 28:
                menu_rep_bm_block(montaje);
                break;
            case 29:
                menu_rep_bm_inode(montaje);
                break;
            case 30:
                menu_rep_journaling(montaje);
                break;
            case 31:
                menu_rep_inode(montaje);
                break;
            case 32:
                menu_rep_block(montaje);
                break;
            case 33:
                menu_rep_sb(montaje);
                break;
            case 34:
                menu_rep_Directorio(montaje);
                break;
            case 35:
                menu_rep_Directorio2(montaje);
                break;
            case 36:
                menu_rep_Directorio3(montaje);
                break;
            case 37:
                menu_rep_Sistema(montaje);
                break;
            case 100://TESTEO RAPIDO
                correr_Test(montaje,&sesion);
                break;
            default:
                printf("Error: respuesta no valida.\n");
                break;
        }
    }

}

