#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "main_data.h"
#include "primeros.h"

int primer_inode_libre(FILE *disco,SB_S2 sb){
    char leido='0';
    fseek(disco,sb.sb_ap_bitmap_inodos,SEEK_SET);
    for(int i=0;i<sb.sb_numero_inodos;i++){
        fread(&leido,sizeof(char),1,disco);
        if(leido=='0')
            return i;
    }
    return -1;
}

int primer_BD_libre(FILE *disco,SB_S2 sb){
    char leido='0';
    fseek(disco,sb.sb_ap_bitmap_ficheros,SEEK_SET);
    for(int i=0;i<sb.sb_numero_bloques;i++){
        fread(&leido,sizeof(char),1,disco);
        if(leido=='0')
            return i;
    }
    return -1;
}