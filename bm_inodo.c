#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"

void rep_bm_inode(Montaje *montaje, char *id, char *path) {

    char print_id[5];
    
    clean_string(print_id,5);
    strncpy(print_id,id,5);
    
    char directorio[150];
    strcpy(directorio, path);
    if (!strstr(directorio, ".txt")) {
        strcat(directorio, ".txt");
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strcpy(path_disco, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.tipo == 'e') {
        printf("ERROR: La Particion Mopntada es de Tipo Extendida, solo puede ser te tipo Logica o Primaria para generar el reporte.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;

    disco = fopen(path_disco, "r+b");
    reporte = fopen(directorio, "w");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        fclose(reporte);
        return;
    }

    switch (encontrado.status) {
        case 'f':
            fseek(disco, encontrado.comienzo, SEEK_SET);
            break;
        default:
            printf("ERROR: La Particion Montada No esta formateada o no tiene un sistema de archivos valido.\n");
            printf("-----------REPORTE FALLIDO-----------\n\n");
            fclose(disco);
            return;
    }

    char comando_abertura[100] = "gnome-open ";

    strcat(comando_abertura, directorio);

    SB_S2 sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char bloque = '0';
    int contador = 1;
    
    fprintf(reporte,"Disco: %s\n",print_id);
    fprintf(reporte,"Nombre Particion: %s\n",encontrado.nombre);
    fprintf(reporte,"Reporte de inodos\n");
    
    char buffer[20];
    
    time_t now = time(NULL); 
    
    for (int i = 0; i < 20; i++) {
        buffer[i] = '\0';
    }
    strftime(buffer, 80, "%x - %I:%M%p", localtime(&now));

    fprintf(reporte,"Fecha del Reporte: %s\n\n",buffer);
    
    for(int i=0;i<60;i++)
        fprintf(reporte,"-");
    fprintf(reporte,"\n\n");
    
    for (int i = 0; i < sb.sb_numero_inodos; i++, contador++) {
        fseek(disco,sb.sb_ap_bitmap_inodos+i,SEEK_SET);
        fread(&bloque, sizeof (char), 1, disco);
        fprintf(reporte, " %c ", bloque);
        if (contador == 20) {
            contador = 0;
            fprintf(reporte, "\n");
        }
    }

    fclose(reporte);
    fclose(disco);

    system(comando_abertura);

    printf("-- REPORTE BM_INODO GENERADO --\n");
    sleep(SLEEP_TIME);
}