#ifndef AUX_SESION_H
#define AUX_SESION_H

int contar_saltos(char *cont);
int contar_g_u(Registro *registros, int cantidad, char tipo);
int lon_contenido(Registro *registro, int cantidad);
void vaciar(Registro *registro, int numero);
void almacenar_registros(Registro *regis, char *cont);
void encontrar_id_anterior(Registro *registros, int *retorno, int cantidad, char *nombre, char tipo);
void eliminar_id(Registro *registros, int *retorno, int cantidad, char *nombre, char tipo);
void rtc(Registro *registro, int cantidad, char *dest, int longitud);


#endif /* AUX_SESION_H */

