#ifndef MAIN_FUNCTIONS_H
#define MAIN_FUNCTIONS_H

void get_string(char *buffer);
void get_number(int *buffer);
void get_menu_option(int *opcion);
void clean_string(char *buffer, int longitud);
void print_welcome();
void print_menu(int menu);
int crear_directorios(char *path);
int string_lenght(char *buffer);
void get_permisos(char *buffer);

#endif /* MAIN_FUNCTIONS_H */

