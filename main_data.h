#ifndef MAIN_DATA_H
#define MAIN_DATA_H

#define MAXLON              500
#define MAX_BD_CONT_SIZE    24
#define MAX_NAME_SIZE       24
#define MAX_INDIREC_APUNT   2
#define MAX_DIREC_APUNT     4
#define MAX_AVD_DIREC_APUNT 6
//#define MAX_BD_APUNTS       18
#define MAX_BD_APUNTS       6 
#define NUMBER_LOGS_FS      2
#define NUMBRE_BLOCKS       (MAX_DIREC_APUNT) + (MAX_BD_APUNTS)+(MAX_BD_APUNTS*MAX_BD_APUNTS)
#define SLEEP_TIME          3
#define BA_COLOR            "#99bbff"
#define BC_COLOR            "#ffbf00"
#define BD_COLOR            "#33cc33"
#define INODE_COLOR         "#ff5c33"
#define ROOT_ID             1

//time_t 8

typedef struct {
    char part_status;
    char part_type;
    int part_start;
    int part_size;
    char part_name[16];
} particion;

typedef struct {
    int mbr_tamano;
    time_t mbr_fecha_creacion;
    int mbr_disk_signature;
    particion mbr_partition_1;
    particion mbr_partition_2;
    particion mbr_partition_3;
    particion mbr_partition_4;
} MBR;

typedef struct {
    char part_status;
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
} EBR;

typedef struct {
    int numero;
    char letra;
    char id[5];
    char nombre[25];
    char status;
    int size;
    char tipo;
    int comienzo;
} ids;

typedef struct {
    ids lista[100];
    char indice;
    char path[100];
} Montaje;

typedef struct {
    int montaje;
    int lista;
    int UID;
    int GID;
} Sesion;

typedef struct {
    char username[10];
    char password[10];
    char grupo[10];
    char tipo;
    int id;
} Registro;

typedef struct {
    char bname[MAX_NAME_SIZE];
} carpetas;

typedef struct { // size = 62 
    
    int i_llave;
    char i_name[MAX_NAME_SIZE];
    time_t i_fecha_creacion;
    int i_tam_archivo;
    int i_asig_bloques;
    char i_tipo;
    int i_bloque[MAX_DIREC_APUNT];
    int i_indirecto[MAX_INDIREC_APUNT];
    int i_idPropetario;
    char i_idPermisos[3];
    
} inodo_S2; // BLOQUE DIRECTORIO

typedef struct { // size = 72
    char avd_nombre_directorio[MAX_NAME_SIZE];
    char avd_padre[MAX_NAME_SIZE];
    int avd_bloque[MAX_AVD_DIREC_APUNT];    
} BC_S2; // BLOQUE DE ARBOL DE DIRECTORIO

typedef struct { // size = 72
    char db_nombre[MAX_NAME_SIZE];
    char db_padre[MAX_NAME_SIZE];
    char db_data[MAX_BD_CONT_SIZE];
} BD_S2; // BLOQUE DE DATOS
/*
typedef struct{ // size = 72
    int apuntador[MAX_BD_APUNTS];
} BA_S2; // BLOQUE DE APUNTADORES
*/
typedef struct{ // size = 72
    int apuntador[18];
} BA_S2; // BLOQUE DE APUNTADORES

typedef struct { // size = 112
    int sb_numero_inodos;
    int sb_numero_bloques;
    int sb_tamano_bloque;
    int sb_tamano_inodo;
    int sb_numero_magico;
    int sb_bloques_free;
    int sb_inodos_free;
    int sb_ap_inodos;
    int sb_ap_ficheros;
    int sb_ap_bitmap_inodos;
    int sb_ap_bitmap_ficheros;
    int sb_first_free_inodo;
    int sb_first_free_fichero;
    time_t sb_mount_time;
} SB_S2; // SUPER BLOQUE

typedef struct {
    char log_tipo_operacion;
    char log_tipo;
    char log_nombre[MAX_NAME_SIZE];
    time_t Journal_fecha;
} Log_S2;

#endif /* MAIN_DATA_H */

