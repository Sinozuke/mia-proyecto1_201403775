#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"

void escribir_dir_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_bc) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] == -1) {
            fprintf(reporte, "\t\t<TD BGCOLOR=\"#b3daff\">%i</TD>\n", bc.avd_bloque[i]);
        } else {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            fprintf(reporte, "\t\t<TD BGCOLOR=\"#b3daff\" PORT=\"f%i\">%i</TD>\n", bc.avd_bloque[i], bc.avd_bloque[i]);
        }
    }


}

void escribir_dir_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        escribir_dir_lvl0(disco, reporte, sb, ba.apuntador[i]);
    }


}

void escribir_dir_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        escribir_dir_lvl1(disco, reporte, sb, ba.apuntador[i]);
    }

}

void contar_dir_lvl0(FILE *disco, SB_S2 *sb, int pos_bc, int *total) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        *total = (*total) + 1;
    }


}

void contar_dir_lvl1(FILE *disco, SB_S2 *sb, int pos_ba, int *total) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contar_dir_lvl0(disco, sb, ba.apuntador[i], total);
    }


}

void contar_dir_lvl2(FILE *disco, SB_S2 *sb, int pos_ba, int *total) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contar_dir_lvl1(disco, sb, ba.apuntador[i], total);
    }

}

void contar_dir(FILE *disco, SB_S2 *sb, inodo_S2 *ino, int *total) {

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        if (ino->i_bloque[i] != -1) {
            contar_dir_lvl0(disco, sb, ino->i_bloque[i], total);
        }
    }
    contar_dir_lvl1(disco, sb, ino->i_indirecto[0], total);
    contar_dir_lvl2(disco, sb, ino->i_indirecto[1], total);

}

void escribir_dir(FILE *disco, FILE *reporte, SB_S2 *sb, int i) {

    inodo_S2 ino;
    int total = 0;

    fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);
    if (ino.i_tipo == 'c') {
        fprintf(reporte, "\"D%i\" [label=<\n", i);
        fprintf(reporte, "<TABLE CELLSPACING=\"0\" BGCOLOR=\"%s\">\n",BC_COLOR);
        fprintf(reporte, "\t<TR>\n");
        contar_dir(disco, sb, &ino, &total);
        if (total == 0) {
            fprintf(reporte, "\t\t<TD COLSPAN=\"6\" WIDTH=\"50\" HEIGHT=\"50\">\n");
        } else {
            fprintf(reporte, "\t\t<TD COLSPAN=\"%i\" WIDTH=\"50\" HEIGHT=\"50\">\n", total);
        }
        fprintf(reporte, "\t\t\t<FONT POINT-SIZE=\"50\">%s</FONT>\n", ino.i_name);
        fprintf(reporte, "\t\t</TD>\n");
        fprintf(reporte, "\t</TR>\n");
        fprintf(reporte, "\t<TR>\n");
        for (int j = 0; j < MAX_DIREC_APUNT; j++) {
            escribir_dir_lvl0(disco, reporte, sb, ino.i_bloque[j]);
        }
        escribir_dir_lvl1(disco, reporte, sb, ino.i_indirecto[0]);
        escribir_dir_lvl2(disco, reporte, sb, ino.i_indirecto[1]);

        fprintf(reporte, "\t</TR>\n");
        fprintf(reporte, "</TABLE>>];\n");
    }

}

void escribir_inodo_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int bd){
    
    if(bd==-1){
        return;
    }
    
    BD_S2 bloque;
    
    fseek(disco,sb->sb_ap_ficheros+bd*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    fprintf(reporte, "\"D%i\" [label=<\n", bd);
    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BD_COLOR);
    fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Bloque No.%i</TD>\n</TR>\n", bd);
    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Dato</TD>\n</TR>\n");
    fprintf(reporte, "<TR>\n<TD>Nombre</TD><TD>%.24s</TD>\n</TR>\n",bloque.db_nombre);
    fprintf(reporte, "<TR>\n<TD>Contenido</TD><TD>");                    
    for(int j=0;j<MAX_BD_CONT_SIZE && bloque.db_data[j]!='\0';j++){
        if(bloque.db_data[j]=='\n'){
            fprintf(reporte, "\\n");                    
        }else{
            fprintf(reporte, "%c",bloque.db_data[j]);                    
        }
    }
    fprintf(reporte, "</TD>\n</TR>\n");
    fprintf(reporte, "</TABLE>>];\n");

}

void escribir_inodo_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int ba){

    if(ba==-1){
        return;
    }
    
    BA_S2 bloque;
    
    fseek(disco,sb->sb_ap_ficheros+ba*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    for(int i=0;i<MAX_BD_APUNTS;i++){
        escribir_inodo_lvl0(disco,reporte,sb, bloque.apuntador[i]);
    }
    
    fprintf(reporte, "\"D%i\" [label=<\n", ba);
    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BA_COLOR);
    fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Bloque No.%i</TD>\n</TR>\n", ba);
    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Apuntador</TD>\n</TR>\n");
    for(int j=0;j<MAX_BD_APUNTS;j++){
        if(bloque.apuntador[j]==-1){
            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD>%i</TD>\n</TR>\n",j+1,bloque.apuntador[j]);
        }else{
            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n",j+1,bloque.apuntador[j],bloque.apuntador[j]);
        }
        
    }
    fprintf(reporte, "</TABLE>>];\n");
    
    for(int j=0;j<MAX_BD_APUNTS;j++){
        if(bloque.apuntador[j]!=-1){
            fprintf(reporte, "\"D%i\":f%i->\"D%i\";\n",ba,bloque.apuntador[j],bloque.apuntador[j]);
        }
    }

}

void escribir_inodo_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int ba){

    if(ba==-1){
        return;
    }
    
    BA_S2 bloque;
    
    fseek(disco,sb->sb_ap_ficheros+ba*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    for(int i=0;i<MAX_BD_APUNTS;i++){
        escribir_inodo_lvl1(disco,reporte,sb, bloque.apuntador[i]);
    }
    
    fprintf(reporte, "\"D%i\" [label=<\n", ba);
    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BA_COLOR);
    fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Bloque No.%i</TD>\n</TR>\n", ba);
    fprintf(reporte, "<TR>\n<TD>Tipo de bloque</TD><TD>Apuntador</TD>\n</TR>\n");
    
    for(int j=0;j<MAX_BD_APUNTS;j++){
        if(bloque.apuntador[j]==-1){
            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD>%i</TD>\n</TR>\n",j+1,bloque.apuntador[j]);
        }else{
            fprintf(reporte, "<TR>\n<TD>Apuntador %i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n",j+1,bloque.apuntador[j],bloque.apuntador[j]);
        }
        
    }
    
    fprintf(reporte, "</TABLE>>];\n");

    for(int j=0;j<MAX_BD_APUNTS;j++){
        if(bloque.apuntador[j]!=-1){
            fprintf(reporte, "\"D%i\":f%i->\"D%i\";\n",ba,bloque.apuntador[j],bloque.apuntador[j]);
        }
    }
    
}

void escribir_inodo(FILE *disco, FILE *reporte, SB_S2 *sb, int i) {

    inodo_S2 ino;
    int total = 0;

    fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    fprintf(reporte, "\"I%i\" [label=<\n", i);
    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",INODE_COLOR);
    fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">Inodo No.%i</TD>\n</TR>\n", i + 1);
    fprintf(reporte, "<TR>\n<TD>i_type</TD><TD>Archivo</TD>\n</TR>\n");
    fprintf(reporte, "<TR>\n<TD>i_nombre</TD><TD>%.24s</TD>\n</TR>\n", ino.i_name);
    fprintf(reporte, "<TR>\n<TD>i_llave</TD><TD>%i</TD>\n</TR>\n", i + 1);
    fprintf(reporte, "<TR>\n<TD>i_tam_archivo</TD><TD>%i</TD>\n</TR>\n", ino.i_tam_archivo);
    fprintf(reporte, "<TR>\n<TD>i_asig_bloques</TD><TD>%i</TD>\n</TR>\n", ino.i_asig_bloques);
    for (int j = 0; j < MAX_DIREC_APUNT; j++) {
        if (ino.i_bloque[j] == -1) {
            fprintf(reporte, "<TR>\n<TD>i_bloque_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, ino.i_bloque[j]);
        } else {
            fprintf(reporte, "<TR>\n<TD>i_bloque_%i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n", j + 1, ino.i_bloque[j], ino.i_bloque[j]);
        }
    }
    for (int j = 0; j < MAX_INDIREC_APUNT; j++){
        if (ino.i_bloque[j] == -1) {
            fprintf(reporte, "<TR>\n<TD>i_indirecto_%i</TD><TD>%i</TD>\n</TR>\n", j + 1, ino.i_indirecto[j]);
        } else {
            fprintf(reporte, "<TR>\n<TD>i_indirecto_%i</TD><TD PORT=\"f%i\">%i</TD>\n</TR>\n", j + 1, ino.i_indirecto[j], ino.i_indirecto[j]);
        }
    }        
    fprintf(reporte, "<TR>\n<TD>I_idPropetario</TD><TD>%i</TD>\n</TR>\n", ino.i_idPropetario);
    fprintf(reporte, "<TR>\n<TD>I_idPermisos</TD><TD>%.3s</TD>\n</TR>\n", ino.i_idPermisos);
    fprintf(reporte, "</TABLE>>];\n");

    for (int j = 0; j < MAX_DIREC_APUNT; j++) {
        escribir_inodo_lvl0(disco,reporte,sb, ino.i_bloque[j]);
        if(ino.i_bloque[j]!=-1){    
            fprintf(reporte,"\"I%i\":f%i->\"D%i\";\n",i,ino.i_bloque[j],ino.i_bloque[j]);
        }
    }
    
    if(ino.i_indirecto[0]!=-1){    
        fprintf(reporte,"\"I%i\":f%i->\"D%i\";\n",i,ino.i_indirecto[0],ino.i_indirecto[0]);
    }
    if(ino.i_indirecto[1]!=-1){    
        fprintf(reporte,"\"I%i\":f%i->\"D%i\";\n",i,ino.i_indirecto[1],ino.i_indirecto[1]);
    }
    escribir_inodo_lvl1(disco,reporte,sb, ino.i_indirecto[0]);
    escribir_inodo_lvl2(disco,reporte,sb, ino.i_indirecto[1]);

}

void enlazar_dir_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_bc, int padre) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (ino.i_tipo == 'c') {
                fprintf(reporte, "\"Directorio%i\":f%i->\"Directorio%i\";\n", padre, bc.avd_bloque[i], bc.avd_bloque[i]);
            }
        }
    }


}

void enlazar_dir_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba, int padre) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_dir_lvl0(disco, reporte, sb, ba.apuntador[i], padre);
    }


}

void enlazar_dir_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int pos_ba, int padre) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_dir_lvl1(disco, reporte, sb, ba.apuntador[i], padre);
    }

}

void enlazar_dir(FILE *disco, FILE *reporte, SB_S2 *sb, int i) {

    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);
    if (ino.i_tipo == 'c') {
        for (int j = 0; j < MAX_DIREC_APUNT; j++) {
            enlazar_dir_lvl0(disco, reporte, sb, ino.i_bloque[j], ino.i_llave);
        }
        enlazar_dir_lvl1(disco, reporte, sb, ino.i_indirecto[0], ino.i_llave);
        enlazar_dir_lvl2(disco, reporte, sb, ino.i_indirecto[1], ino.i_llave);
    }

}

void escribir_dir_padre_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BC, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_BC * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT && (total - 1) != *actual; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (strcmp(ino.i_name, carpes[*actual].bname) == 0 && ino.i_tipo == 'c') {
                escribir_dir(disco, reporte, sb, ino2->i_llave);
                fprintf(reporte, "\"D%i\":f%i->\"D%i\";\n", ino2->i_llave, ino.i_llave, ino.i_llave);
                *encontrado = ino.i_llave;
                *actual = *actual + 1;
                *i2 = -1;
                fseek(disco, sb->sb_ap_inodos + (*encontrado) * sb->sb_tamano_inodo, SEEK_SET);
                fread(ino2, sb->sb_tamano_inodo, 1, disco);
                return;
            }
        } else {
            *encontrado = -1;
            return;
        }
    }

}

void escribir_dir_padre_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BA, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_BA * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && (total - 1) != *actual; i++) {
        if (ba.apuntador[i] != -1) {
            escribir_dir_padre_lvl0(disco, reporte, sb, actual, encontrado, total, ba.apuntador[i], carpes, i2, ino2);
        } else {
            *encontrado = -1;
            return;
        }
    }

}

void escribir_dir_padre_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BA, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_BA * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && (total - 1) != *actual; i++) {
        if (ba.apuntador[i] != -1) {
            escribir_dir_padre_lvl1(disco, reporte, sb, actual, encontrado, total, ba.apuntador[i], carpes, i2, ino2);
        } else {
            *encontrado = -1;
            return;
        }
    }

}

void escribir_dir_padre(FILE *disco, FILE *reporte, SB_S2 *sb, carpetas carpes[], int total) {

    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);
    int encontrado = 0;
    int actual = 0;

    for (int i = 0; i < MAX_DIREC_APUNT + 2 && (total - 1) != actual; i++) {
        switch (i) {
            case MAX_DIREC_APUNT:
                escribir_dir_padre_lvl1(disco, reporte, sb, &actual, &encontrado, total, ino.i_indirecto[0], carpes, &i, &ino);
                break;
            case MAX_DIREC_APUNT + 1:
                escribir_dir_padre_lvl2(disco, reporte, sb, &actual, &encontrado, total, ino.i_indirecto[1], carpes, &i, &ino);
                break;
            default:
                if (ino.i_bloque[i] != -1) {
                    escribir_dir_padre_lvl0(disco, reporte, sb, &actual, &encontrado, total, ino.i_bloque[i], carpes, &i, &ino);
                }
                break;
        }
    }

}

void rep_Directorio2(Montaje *montaje, char *id, char *path, char path_mostrar[]) {

    char directorio[MAXLON + 1];

    clean_string(directorio, MAXLON);

    strncpy(directorio, path, MAXLON);

    if (!strstr(directorio, ".svg")) {
        strncat(directorio, ".svg", 4);
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strncpy(path_disco, montaje[id[2] - 97].path, strlen(montaje[id[2] - 97].path));

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: La Particion Montada No esta formateada con un sistema de archivos valido.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" DIR2.dot", 10);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    reporte = fopen("DIR2.dot", "w");

    if (!reporte) {
        printf("ERROR: el Reporte no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    SB_S2 sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path_mostrar, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path_mostrar)];
    clean_string(copia_path, strlen(path_mostrar));
    strncpy(copia_path, path_mostrar, strlen(path_mostrar));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            fclose(reporte);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        fclose(reporte);
        return;
    }

    inodo_S2 carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, carpeta_padre, carpes[numero - 1].bname, 'a');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        fclose(reporte);
        return;
    }

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");

    escribir_dir_padre(disco, reporte, &sb, carpes, numero);
    escribir_dir(disco, reporte, &sb, carpeta_padre.i_llave);
    escribir_inodo(disco, reporte, &sb, existe);
    fprintf(reporte,"\"D%i\":f%i->\"I%i\";\n", carpeta_padre.i_llave, existe, existe);

    fprintf(reporte, "}");


    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);
    printf("-- REPORTE DIR2 GENERADO --\n");
    sleep(SLEEP_TIME);

}