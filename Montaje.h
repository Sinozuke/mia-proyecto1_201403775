#ifndef MONTAJE_H
#define MONTAJE_H

#include "main_data.h"

void mount_c(Montaje *montajes, char *path, char *name);
void monstrar_montajes(Montaje *montajes);
void umount_c(Montaje *mont, char *id);
int encontrar_id(Montaje *montajes, int j, char *id);

#endif /* MONTAJE_H */

