#ifndef REPORTES_H
#define REPORTES_H

void rep_bm_block(Montaje *montaje, char *id, char *path);
void rep_bm_inode(Montaje *montaje, char *id, char *path);
void rep_journaling(Montaje *montaje, char *id, char *path);
void rep_inode(Montaje *montaje, char *id, char *path);
void rep_block(Montaje *montaje, char *id, char *path);
void rep_sb(Montaje *montaje, char *id, char *path);
void rep_Directorio(Montaje *montaje, char *id, char *path);
void rep_Directorio2(Montaje *montaje, char *id, char *path , char path_mostrar[]);
void rep_Directorio3(Montaje *montaje, char *id, char *path, char path_mostrar[]);
void rep_Sistema(Montaje *montaje, char *id, char *path);

#endif /* REPORTES_H */
