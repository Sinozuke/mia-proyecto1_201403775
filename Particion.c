#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "Particion.h"
#include "main_data.h"
#include "main_functions.h"

struct stat st = {0};

int lugar(MBR mbr, int tamano);
int name_exist(FILE *disco, MBR mbr, char *name);
int extendida_exist(MBR mbr);

void crear_pp(char *nombre, char *path, int tamano, int unit) {
    printf("-------------CREANDO PARTICION-------------\n");

    if (stat(path, &st) == -1) {
        printf("ERROR: el archivo especificado no existe.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    MBR leido;

    if (fread(&leido, sizeof (MBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    int tamano_real;

    switch (unit) {
        case 1:
            tamano_real = tamano * 1024 * 1024;
            break;
        case 2:
            tamano_real = tamano * 1024;
            break;
        case 3:
            tamano_real = tamano;
            break;
    }

    printf("-------------------DATOS-------------------\n");
    printf("Nombre: %s\n", nombre);
    printf("Path: %s\n", path);

    switch (unit) {
        case 1:
            printf("Unidad: MegaBytes\nTamano: %i\n", tamano);
            break;
        case 2:
            printf("Unidad: KiloBytes\nTamano: %i\n", tamano);
            break;
        case 3:
            printf("Unidad: Bytes\nTamano: %i\n", tamano);
            break;
    }

    printf("Tipo: Primaria\n");

    int posicion = lugar(leido, tamano_real);

    if (posicion == 0) {
        printf("ERROR: ya no es posible crear particion devido a que ya se a sobrepadaso el numero de particiones posibles.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (posicion == -1) {
        printf("ERROR: ya no es posible crear particion devido a que ya no espacio disponible.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (name_exist(disco, leido, nombre)) {
        printf("ERROR: el nombre ya existen entre las particiones.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    particion partnueva;

    partnueva.part_status = 's';
    partnueva.part_type = 'p';
    switch (unit) {
        case 1:
            partnueva.part_size = tamano * 1024 * 1024;
            break;
        case 2:
            partnueva.part_size = tamano * 1024;
            break;
        case 3:
            partnueva.part_size = tamano;
            break;
    }
    strcpy(partnueva.part_name, nombre);

    switch (posicion) {
        case 1:
            partnueva.part_start = sizeof (MBR);
            leido.mbr_partition_1 = partnueva;
            break;
        case 2:
            partnueva.part_start = leido.mbr_partition_1.part_start + leido.mbr_partition_1.part_size;
            leido.mbr_partition_2 = partnueva;
            break;
        case 3:
            partnueva.part_start = leido.mbr_partition_2.part_start + leido.mbr_partition_2.part_size;
            leido.mbr_partition_3 = partnueva;
            break;
        case 4:
            partnueva.part_start = leido.mbr_partition_3.part_start + leido.mbr_partition_3.part_size;
            leido.mbr_partition_4 = partnueva;
            break;
    }

    fseek(disco, 0, SEEK_SET);
    fwrite(&leido, sizeof (MBR), 1, disco);
    printf("-------- -----CREACION EXITOSA--------------\n");

    fclose(disco);
}

void crear_pe(char *nombre, char *path, int tamano, int unit) {
    printf("-------------CREANDO PARTICION-------------\n");

    if (stat(path, &st) == -1) {
        printf("ERROR: el archivo especificado no existe.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    MBR leido;

    if (fread(&leido, sizeof (MBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    int tamano_real;

    switch (unit) {
        case 1:
            tamano_real = tamano * 1024 * 1024;
            break;
        case 2:
            tamano_real = tamano * 1024;
            break;
        case 3:
            tamano_real = tamano;
            break;
    }

    printf("-------------------DATOS-------------------\n");
    printf("Nombre: %s\n", nombre);
    printf("Path: %s\n", path);

    switch (unit) {
        case 1:
            printf("Unidad: MegaBytes\nTamano: %i", tamano);
            break;
        case 2:
            printf("Unidad: KiloBytes\nTamano: %i", tamano);
            break;
        case 3:
            printf("Unidad: Bytes\nTamano: %i", tamano);
            break;
    }

    printf("Tipo: Extendida\n");

    int posicion = lugar(leido, tamano_real);

    if (posicion == 0) {
        printf("ERROR: ya no es posible crear particion devido a que ya se a sobrepadaso el numero de particiones posibles.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (posicion == -1) {
        printf("ERROR: ya no es posible crear particion devido a que ya no espacio disponible.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (name_exist(disco, leido, nombre)) {
        printf("ERROR: el nombre ya existen entre las particiones.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (extendida_exist(leido) != 0) {
        printf("ERROR: ya existe una particion Extendida en el disco.\n");
        fclose(disco);
        return;
    }

    particion partnueva;

    partnueva.part_status = 's';
    partnueva.part_type = 'e';

    switch (unit) {
        case 1:
            partnueva.part_size = tamano * 1024 * 1024;
            break;
        case 2:
            partnueva.part_size = tamano * 1024;
            break;
        case 3:
            partnueva.part_size = tamano;
            break;
    }
    strcpy(partnueva.part_name, nombre);

    switch (posicion) {
        case 1:
            partnueva.part_start = sizeof (MBR);
            leido.mbr_partition_1 = partnueva;
            break;
        case 2:
            partnueva.part_start = leido.mbr_partition_1.part_start + leido.mbr_partition_1.part_size;
            leido.mbr_partition_2 = partnueva;
            break;
        case 3:
            partnueva.part_start = leido.mbr_partition_2.part_start + leido.mbr_partition_2.part_size;
            leido.mbr_partition_3 = partnueva;
            break;
        case 4:
            partnueva.part_start = leido.mbr_partition_3.part_start + leido.mbr_partition_3.part_size;
            leido.mbr_partition_4 = partnueva;
            break;
    }

    fseek(disco, 0, SEEK_SET);
    fwrite(&leido, sizeof (MBR), 1, disco);

    EBR nuevoebr;

    strcpy(nuevoebr.part_name, "-vacio-");
    nuevoebr.part_next = -1;
    nuevoebr.part_status = 'n';
    nuevoebr.part_size = 0;
    nuevoebr.part_start = -1;
    switch (posicion) {
        case 1:
            nuevoebr.part_size = leido.mbr_partition_1.part_size - sizeof (EBR);
            fseek(disco, leido.mbr_partition_1.part_start, SEEK_SET);
            fwrite(&nuevoebr, sizeof (EBR), 1, disco);
            break;
        case 2:
            nuevoebr.part_size = leido.mbr_partition_2.part_size - sizeof (EBR);
            fseek(disco, leido.mbr_partition_2.part_start, SEEK_SET);
            fwrite(&nuevoebr, sizeof (EBR), 1, disco);
            break;
        case 3:
            nuevoebr.part_size = leido.mbr_partition_3.part_size - sizeof (EBR);
            fseek(disco, leido.mbr_partition_3.part_start, SEEK_SET);
            fwrite(&nuevoebr, sizeof (EBR), 1, disco);
            break;
        case 4:
            nuevoebr.part_size = leido.mbr_partition_4.part_size - sizeof (EBR);
            fseek(disco, leido.mbr_partition_4.part_start, SEEK_SET);
            fwrite(&nuevoebr, sizeof (EBR), 1, disco);
            break;
    }

    printf("-------------CREACION EXITOSA--------------\n");

    fclose(disco);
}

void crear_pl(char nombre[200], char path[200], int tamano, int unit) {
    printf("-------------CREANDO PARTICION-------------\n");

    if (stat(path, &st) == -1) {
        printf("ERROR: el archivo especificado no existe.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    MBR leido;

    if (fread(&leido, sizeof (MBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    int tamano_real;

    switch (unit) {
        case 1:
            tamano_real = tamano * 1024 * 1024;
            break;
        case 2:
            tamano_real = tamano * 1024;
            break;
        case 3:
            tamano_real = tamano;
            break;
    }

    printf("-------------------DATOS-------------------\n");
    printf("Nombre: %s\n", nombre);
    printf("Path: %s\n", path);

    switch (unit) {
        case 1:
            printf("Unidad: MegaBytes\nTamano: %i", tamano);
            break;
        case 2:
            printf("Unidad: KiloBytes\nTamano: %i", tamano);
            break;
        case 3:
            printf("Unidad: Bytes\nTamano: %i", tamano);
            break;
    }

    printf("Tipo: Logica\n");

    int parti = extendida_exist(leido);

    if (parti == 0) {
        printf("ERROR: no exite una particion extendida.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    if (name_exist(disco, leido, nombre) != 0) {
        printf("ERROR: El nombre ya existe como particion.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    EBR leido2;
    int tamano_particion = 0;

    switch (parti) {
        case 1:
            fseek(disco, leido.mbr_partition_1.part_start, SEEK_SET);
            tamano_particion = leido.mbr_partition_1.part_size;
            break;
        case 2:
            fseek(disco, leido.mbr_partition_2.part_start, SEEK_SET);
            tamano_particion = leido.mbr_partition_2.part_size;
            break;
        case 3:
            fseek(disco, leido.mbr_partition_3.part_start, SEEK_SET);
            tamano_particion = leido.mbr_partition_3.part_size;
            break;
        case 4:
            fseek(disco, leido.mbr_partition_4.part_start, SEEK_SET);
            tamano_particion = leido.mbr_partition_4.part_size;
            break;
    }

    if (fread(&leido2, sizeof (EBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }
    /* Creacion de una logica */

    EBR nuevoebr;

    strcpy(nuevoebr.part_name, "-vacio-");
    nuevoebr.part_next = -1;
    nuevoebr.part_status = 'n';
    nuevoebr.part_size = 0;
    nuevoebr.part_start = -1;

    EBR nuevoebr2;

    strcpy(nuevoebr2.part_name, "-vacio-");
    nuevoebr2.part_next = -1;
    nuevoebr2.part_status = 'n';
    nuevoebr2.part_size = 0;
    nuevoebr2.part_start = -1;


    while (1) {
        if (leido2.part_next == -1) {
            if ((leido2.part_size + sizeof (EBR)) > tamano_real) {
                strcpy(leido2.part_name, nombre);
                leido2.part_next = ftell(disco) + tamano_real;
                leido2.part_size = tamano_real;
                leido2.part_start = ftell(disco);
                leido2.part_status = 's';
                fseek(disco, -sizeof (EBR), SEEK_CUR);
                fwrite(&leido2, sizeof (EBR), 1, disco);
                fseek(disco, leido2.part_next, SEEK_SET);
                nuevoebr.part_size = tamano_particion - (tamano_real + sizeof(EBR));
                fwrite(&nuevoebr, sizeof (EBR), 1, disco);
                printf("-------------CREACION EXITOSA--------------\n\n");
                break;
            } else {
                printf("ERROR: no queda espacio libre para crear una nueva particion logica.\n");
                printf("-------------CREACION FALLIDA--------------\n\n");
                break;
            }
        } else {
            if (leido2.part_status == 'n') {
                if (leido2.part_size >= tamano_real) {
                    strcpy(leido2.part_name, nombre);
                    leido2.part_size = tamano_real;
                    leido2.part_status = 's';
                    fseek(disco, -sizeof (EBR), SEEK_CUR);
                    fwrite(&leido2, sizeof (EBR), 1, disco);
                    printf("-------------CREACION EXITOSA--------------\n\n");
                    break;
                }
            }

            int residuo = (leido2.part_start + leido2.part_size) - leido2.part_next;
            if (residuo != 0) {
                if (residuo > (sizeof (EBR) + tamano_real)) {

                    leido2.part_next = ftell(disco) + tamano_real;
                    fseek(disco, -sizeof (EBR), SEEK_CUR);

                    fwrite(&leido2, sizeof (EBR), 1, disco);

                    fseek(disco, leido2.part_size, SEEK_CUR);

                    strcpy(nuevoebr2.part_name, nombre);
                    nuevoebr2.part_next = leido2.part_next;
                    nuevoebr2.part_size = tamano_real;
                    nuevoebr2.part_start = ftell(disco);
                    nuevoebr2.part_status = 's';

                    fwrite(&nuevoebr2, sizeof (EBR), 1, disco);
                    printf("-------------CREACION EXITOSA--------------\n\n");
                    break;
                }
            }
        }
        fseek(disco, leido2.part_next, SEEK_SET);
        if (fread(&leido2, sizeof (EBR), 1, disco) != 1) {
            printf("ERROR:al cargar la data del disco\n");
            printf("-------------CREACION FALLIDA--------------\n\n");
            break;
        }
    }

    fclose(disco);
}

//falta eliminar el punto de montaje si la particion s encuentra montada...
void eliminar_particion(char *name, char *path, char tipo) {
    printf("-------------ELIMINAR PARTICION------------\n");
    printf("Nombre: %s\nPath: %s\nTipo: %c\n", name, path, tipo);

    if (stat(path, &st) == -1) {
        printf("ERROR: el archivo especificado no existe.\n");
        printf("-----------ELIMINACION FALLIDA-----------\n\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-----------ELIMINACION FALLIDA-----------\n\n");
        return;
    }

    MBR leido;

    if (fread(&leido, sizeof (MBR), 1, disco) != 1) {
        printf("ERROR:al cargar la data del disco\n");
        printf("-----------ELIMINACION FALLIDA-----------\n\n");
        fclose(disco);
        return;
    }

    int extendida = 0;
    char byte = '\0';

    switch (name_exist(disco, leido, name)) {
        case 0:
            printf("ERROR: No existe la particion.\n");
            printf("----------ELIMINACION FALLIDA----------\n\n");
            fclose(disco);
            return;
        case 1:
            if (tipo == 'a') {
                if (leido.mbr_partition_1.part_status != 'n')
                    if (strcmp(leido.mbr_partition_1.part_name, name) == 0) {
                        leido.mbr_partition_1.part_status = 'n';
                    }
                if (leido.mbr_partition_2.part_status != 'n')
                    if (strcmp(leido.mbr_partition_2.part_name, name) == 0) {
                        leido.mbr_partition_2.part_status = 'n';
                    }
                if (leido.mbr_partition_3.part_status != 'n')
                    if (strcmp(leido.mbr_partition_3.part_name, name) == 0) {
                        leido.mbr_partition_3.part_status = 'n';
                    }
                if (leido.mbr_partition_4.part_status != 'n')
                    if (strcmp(leido.mbr_partition_4.part_name, name) == 0) {
                        leido.mbr_partition_4.part_status = 'n';
                    }
            } else if (tipo == 'u') {
                if (leido.mbr_partition_1.part_status != 'n')
                    if (strcmp(leido.mbr_partition_1.part_name, name) == 0) {
                        leido.mbr_partition_1.part_status = 'n';
                        fseek(disco, leido.mbr_partition_1.part_start, SEEK_SET);
                        for (int i = 0; i < leido.mbr_partition_1.part_size; i++)
                            fwrite(&byte, sizeof (char), 1, disco);
                    }
                if (leido.mbr_partition_2.part_status != 'n')
                    if (strcmp(leido.mbr_partition_2.part_name, name) == 0) {
                        leido.mbr_partition_2.part_status = 'n';
                        fseek(disco, leido.mbr_partition_2.part_start, SEEK_SET);
                        for (int i = 0; i < leido.mbr_partition_2.part_size; i++)
                            fwrite(&byte, sizeof (char), 1, disco);
                    }
                if (leido.mbr_partition_3.part_status != 'n')
                    if (strcmp(leido.mbr_partition_3.part_name, name) == 0) {
                        leido.mbr_partition_3.part_status = 'n';
                        fseek(disco, leido.mbr_partition_3.part_start, SEEK_SET);
                        for (int i = 0; i < leido.mbr_partition_3.part_size; i++)
                            fwrite(&byte, sizeof (char), 1, disco);
                    }
                if (leido.mbr_partition_4.part_status != 'n')
                    if (strcmp(leido.mbr_partition_4.part_name, name) == 0) {
                        leido.mbr_partition_4.part_status = 'n';
                        fseek(disco, leido.mbr_partition_4.part_start, SEEK_SET);
                        for (int i = 0; i < leido.mbr_partition_4.part_size; i++)
                            fwrite(&byte, sizeof (char), 1, disco);
                    }
            }
            fseek(disco, 0, SEEK_SET);
            fwrite(&leido, sizeof (MBR), 1, disco);
            break;
        case 2:
            if (leido.mbr_partition_1.part_status != 'n')
                if (leido.mbr_partition_1.part_type == 'e')
                    extendida = 1;

            if (leido.mbr_partition_2.part_status != 'n')
                if (leido.mbr_partition_2.part_type == 'e')
                    extendida = 2;

            if (leido.mbr_partition_3.part_status != 'n')
                if (leido.mbr_partition_3.part_type == 'e')
                    extendida = 3;

            if (leido.mbr_partition_4.part_status != 'n')
                if (leido.mbr_partition_4.part_type == 'e')
                    extendida = 4;

            if (extendida != 0) {

                switch (extendida) {
                    case 1:
                        fseek(disco, leido.mbr_partition_1.part_start, SEEK_SET);
                        break;
                    case 2:
                        fseek(disco, leido.mbr_partition_2.part_start, SEEK_SET);
                        break;
                    case 3:
                        fseek(disco, leido.mbr_partition_3.part_start, SEEK_SET);
                        break;
                    case 4:
                        fseek(disco, leido.mbr_partition_4.part_start, SEEK_SET);
                        break;
                }

                EBR leidoe;
                EBR anterior;

                if (fread(&leidoe, sizeof (EBR), 1, disco) != 1) {
                    printf("ERROR:al cargar la data del disco\n");
                    fclose(disco);
                    return;
                }

                anterior = leidoe;
                while (strcmp(leidoe.part_name, name) != 0 && leidoe.part_next != -1) {
                    if (fseek(disco, leidoe.part_size, SEEK_CUR) != 0) {
                        printf("cagada.\n");
                        break;
                    };
                    anterior = leidoe;
                    if (fread(&leidoe, sizeof (EBR), 1, disco) != 1) {
                        printf("cagada.\n");
                        break;
                    };
                }

                if (strcmp(leidoe.part_name, name) == 0) {
                    if (tipo == 'a') {
                        if (strcmp(anterior.part_name, leidoe.part_name) == 0) {
                            leidoe.part_status = 'n';
                            fseek(disco, -sizeof (EBR), SEEK_CUR);
                            fwrite(&leido, sizeof (EBR), 1, disco);
                        } else {
                            anterior.part_next = leidoe.part_next;
                            fseek(disco, -2 * sizeof (EBR) - anterior.part_size, SEEK_CUR);
                            fwrite(&anterior, sizeof (EBR), 1, disco);
                        }
                    } else if (tipo == 'u') {
                        if (strcmp(anterior.part_name, leidoe.part_name) == 0) {
                            leidoe.part_status = 'n';
                            fseek(disco, -sizeof (EBR), SEEK_CUR);
                            fwrite(&leido, sizeof (EBR), 1, disco);
                            for (int i = 0; i < leidoe.part_size; i++)
                                fwrite(&byte, sizeof (char), 1, disco);
                        } else {
                            anterior.part_next = leidoe.part_next;
                            fseek(disco, -2 * sizeof (EBR) - anterior.part_size, SEEK_CUR);
                            fwrite(&anterior, sizeof (EBR), 1, disco);
                            fseek(disco, anterior.part_size, SEEK_CUR);
                            for (int i = 0; i < leidoe.part_size + sizeof(EBR); i++)
                                fwrite(&byte, sizeof (char), 1, disco);
                        }
                    }
                }
            }
            break;
    }
    fclose(disco);
    printf("-------------ELIMINACION EXITOSA-----------\n\n");
}

int lugar(MBR mbr, int tamano) {
    int bloques[4] = {1, 1, 1, 1};

    for (int i = 0; i < 4; i++)
        switch (i) {
            case 0:
                if (mbr.mbr_partition_1.part_status == 'n')
                    bloques[i] = 0;
                break;
            case 1:
                if (mbr.mbr_partition_2.part_status == 'n')
                    bloques[i] = 0;
                break;
            case 2:
                if (mbr.mbr_partition_3.part_status == 'n')
                    bloques[i] = 0;
                break;
            case 3:
                if (mbr.mbr_partition_4.part_status == 'n')
                    bloques[i] = 0;
                break;
        }

    if (bloques[0] == 1 && bloques[1] == 1 && bloques[2] == 1 && bloques[3] == 1) {
        return 0;
    }

    if (bloques[0] == 1 && bloques[1] == 1 && bloques[2] == 1 && bloques[3] == 0) {
        if ((tamano + mbr.mbr_partition_3.part_start + mbr.mbr_partition_3.part_size) - 1 < mbr.mbr_tamano)
            return 2;
    }

    if (bloques[0] == 1 && bloques[1] == 1 && bloques[2] == 0 && bloques[3] == 1) {
        if ((tamano + mbr.mbr_partition_2.part_start + mbr.mbr_partition_2.part_size) - 1 < mbr.mbr_partition_4.part_start)
            return 3;
    }

    if (bloques[0] == 1 && bloques[1] == 0 && bloques[2] == 1 && bloques[3] == 1) {
        if ((tamano + mbr.mbr_partition_1.part_start + mbr.mbr_partition_1.part_size) - 1 < mbr.mbr_partition_3.part_start)
            return 2;
    }

    if (bloques[0] == 0 && bloques[1] == 1 && bloques[2] == 1 && bloques[3] == 1) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_2.part_start)
            return 1;
    }

    if (bloques[0] == 0 && bloques[1] == 1 && bloques[2] == 1 && bloques[3] == 0) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_2.part_start)
            return 1;
        else if (tamano + (mbr.mbr_partition_3.part_start + mbr.mbr_partition_3.part_size) - 1 < mbr.mbr_tamano)
            return 4;
    }

    if (bloques[0] == 0 && bloques[1] == 1 && bloques[2] == 0 && bloques[3] == 1) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_2.part_start)
            return 1;
        else if (tamano + (mbr.mbr_partition_2.part_start + mbr.mbr_partition_2.part_size) - 1 < mbr.mbr_partition_4.part_start)
            return 3;
    }

    if (bloques[0] == 0 && bloques[1] == 0 && bloques[2] == 1 && bloques[3] == 1) {
        if (tamano + sizeof (MBR) < mbr.mbr_partition_3.part_start)
            return 1;
    }

    if (bloques[0] == 1 && bloques[1] == 0 && bloques[2] == 1 && bloques[3] == 0) {
        if ((tamano + mbr.mbr_partition_1.part_start + mbr.mbr_partition_1.part_size) < mbr.mbr_partition_3.part_start)
            return 2;
        else if (tamano + (mbr.mbr_partition_3.part_start + mbr.mbr_partition_3.part_size) - 1 < mbr.mbr_tamano)
            return 4;
    }

    if (bloques[0] == 1 && bloques[1] == 1 && bloques[2] == 0 && bloques[3] == 0) {
        if (tamano + (mbr.mbr_partition_2.part_start + mbr.mbr_partition_2.part_size) - 1 < mbr.mbr_tamano)
            return 3;
    }

    if (bloques[0] == 1 && bloques[1] == 0 && bloques[2] == 0 && bloques[3] == 1) {
        if (tamano + (mbr.mbr_partition_1.part_start + mbr.mbr_partition_1.part_size) - 1 < mbr.mbr_partition_4.part_start)
            return 2;
    }

    if (bloques[0] == 1 && bloques[1] == 0 && bloques[2] == 0 && bloques[3] == 0) {
        if (tamano + (mbr.mbr_partition_1.part_start + mbr.mbr_partition_1.part_size) - 1 < mbr.mbr_tamano)
            return 2;
    }

    if (bloques[0] == 0 && bloques[1] == 1 && bloques[2] == 0 && bloques[3] == 0) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_2.part_start)
            return 1;
        else if (tamano + (mbr.mbr_partition_2.part_start + mbr.mbr_partition_2.part_size) - 1 < mbr.mbr_tamano)
            return 3;
    }

    if (bloques[0] == 0 && bloques[1] == 0 && bloques[2] == 1 && bloques[3] == 0) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_3.part_start)
            return 1;
        else if (tamano + (mbr.mbr_partition_3.part_start + mbr.mbr_partition_3.part_size) - 1 < mbr.mbr_tamano)
            return 4;
    }

    if (bloques[0] == 0 && bloques[1] == 0 && bloques[2] == 0 && bloques[3] == 1) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_partition_4.part_start)
            return 1;
    }

    if (bloques[0] == 0 && bloques[1] == 0 && bloques[2] == 0 && bloques[3] == 0) {
        if ((tamano + sizeof (MBR)) < mbr.mbr_tamano)
            return 1;
    }

    return -1;
}

int name_exist(FILE *disco, MBR mbr, char *name) {

    if (mbr.mbr_partition_1.part_status != 'n')
        if (strcmp(mbr.mbr_partition_1.part_name, name) == 0)
            return 1;

    if (mbr.mbr_partition_2.part_status != 'n')
        if (strcmp(mbr.mbr_partition_2.part_name, name) == 0)
            return 1;

    if (mbr.mbr_partition_3.part_status != 'n')
        if (strcmp(mbr.mbr_partition_3.part_name, name) == 0)
            return 1;

    if (mbr.mbr_partition_4.part_status != 'n')
        if (strcmp(mbr.mbr_partition_4.part_name, name) == 0)
            return 1;

    int extendida = 0;

    if (mbr.mbr_partition_1.part_status != 'n')
        if (mbr.mbr_partition_1.part_type == 'e')
            extendida = 1;

    if (mbr.mbr_partition_2.part_status != 'n')
        if (mbr.mbr_partition_2.part_type == 'e')
            extendida = 2;

    if (mbr.mbr_partition_3.part_status != 'n')
        if (mbr.mbr_partition_3.part_type == 'e')
            extendida = 3;

    if (mbr.mbr_partition_4.part_status != 'n')
        if (mbr.mbr_partition_4.part_type == 'e')
            extendida = 4;

    if (extendida != 0) {
        EBR leido;

        switch (extendida) {
            case 1:
                fseek(disco, mbr.mbr_partition_1.part_start, SEEK_SET);
                break;
            case 2:
                fseek(disco, mbr.mbr_partition_2.part_start, SEEK_SET);
                break;
            case 3:
                fseek(disco, mbr.mbr_partition_3.part_start, SEEK_SET);
                break;
            case 4:
                fseek(disco, mbr.mbr_partition_4.part_start, SEEK_SET);
                break;
        }

        if (fread(&leido, sizeof (EBR), 1, disco) != 1) {
            printf("ERROR:al cargar la data del disco\n");
            fclose(disco);
            return-1;
        }

        while (leido.part_next != -1) {
            if (leido.part_status == 's' && strcmp(leido.part_name, name) == 0)return 2;
            fseek(disco, leido.part_next, SEEK_SET);
            fread(&leido, sizeof (EBR), 1, disco);
        }

        if (leido.part_status == 's' && strcmp(leido.part_name, name) == 0)return 2;
    }
    return 0;
}

int extendida_exist(MBR mbr) {
    int posicion = 0;

    if (mbr.mbr_partition_1.part_status != 'n')
        if (mbr.mbr_partition_1.part_type == 'e')
            posicion = 1;

    if (mbr.mbr_partition_2.part_status != 'n')
        if (mbr.mbr_partition_2.part_type == 'e')
            posicion = 2;

    if (mbr.mbr_partition_3.part_status != 'n')
        if (mbr.mbr_partition_3.part_type == 'e')
            posicion = 3;

    if (mbr.mbr_partition_4.part_status != 'n')
        if (mbr.mbr_partition_4.part_type == 'e')
            posicion = 4;

    return posicion;
}
