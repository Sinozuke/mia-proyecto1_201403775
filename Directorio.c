#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "Montaje.h"
#include "Directorio.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"
#include "Sesion.h"

void crear_Directorio(Montaje *montaje, Sesion *sesion, char id[], char path[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'c');

    if (existe != -1) {
        printf("ERROR: YA EXISTE EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    if(!can_write(disco,&sb,sesion,padre)){
        printf("ERROR: EL USUARIO NO TIENE PERMISOS PARA ESCRITURA EN \"%s\".\n",nueva_carpeta_padre.i_name);
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpetaino;

    BC_S2 nueva_carpetabc = nuevo_bloque_BC(nueva_carpeta_padre.i_name, carpes[numero - 1].bname);
    nueva_carpetaino = nuevo_inodo(-1, 'c', sesion->UID, carpes[numero - 1].bname);

    nueva_carpetaino.i_bloque[0] = insert_BC(&sb, disco, nueva_carpetabc, -1);
    nueva_carpetaino.i_asig_bloques++;

    enlazar_inodos(disco, &sb, &nueva_carpeta_padre, insert_inodo(&sb, disco, &nueva_carpetaino, -1));

    insert_inodo(&sb, disco, &nueva_carpeta_padre, nueva_carpeta_padre.i_llave);

    nueva_bitacora('1', 'c', carpes[numero - 1].bname, sb, disco);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CREACION DE CARPETA \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void eliminar_Directorio(Montaje *montaje, Sesion *sesion, char id[], char path[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }
    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------ELIMINACION FALLIDA--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'c');

    if (existe == -1) {
        printf("ERROR: NO EXISTE LA CARPETA CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    int acept = 1;

    eliminar_recursivamente(disco, &sb, existe);
    
    if(!acept){
        fclose(disco);
        return;
    }

    desenlazar_inodos(disco, &sb, &nueva_carpeta_padre, existe);

    insert_inodo(&sb, disco, &nueva_carpeta_padre, nueva_carpeta_padre.i_llave);

    nueva_bitacora('3', 'c', carpes[numero - 1].bname, sb, disco);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("ELIMINACION DE CARPETA \"%s\" - \"%s\" EXITOSA.\n", carpes[numero - 1].bname, carpeta_a_crear);

}

void update_carpeta(Montaje *montaje, Sesion *sesion, char id[], char path[], char name[]) {

    char path_1[MAXLON + 1];

    clean_string(path_1, MAXLON);

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    strncpy(path_1, montaje[id[2] - 97].path, MAXLON);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    SB_S2 sb;

    FILE *disco;

    disco = fopen(path_1, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CAMBIO FALLIDO--------------\n\n");
        return;
    }

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path)];
    clean_string(copia_path, strlen(path));
    strncpy(copia_path, path, strlen(path));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        return;
    }

    inodo_S2 nueva_carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&nueva_carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, nueva_carpeta_padre, carpes[numero - 1].bname, 'c');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        return;
    }

    int repetido = dir_exist(disco, &sb, nueva_carpeta_padre, name, 'c');

    if (repetido != -1) {
        repetido = dir_exist(disco, &sb, nueva_carpeta_padre, name, 'a');
        if(repetido != -1){
            printf("ERROR: YA EXISTE UN ELEMENTO CON EL NOMBRE \"%s\"\n", name);
            fclose(disco);
            return;        
        }
    }
    
    if(!can_write(disco,&sb,sesion,existe)){
        printf("ERROR: NO TIENE PERMISOS DE ESCRITURA SOBRE LA CARPETA \"%s\".\n",carpes[numero - 1].bname);
        fclose(disco);
        return;
    }
    
    update_directorio(disco, &sb, existe, name);

    insert_SB_S2(disco, &sb, encontrado.comienzo);

    fclose(disco);

    printf("CAMBIO DE NOMBRE DEL ARCHIVO \"%s\" - \"%s\" A \"%s\" EXITOSO.\n", carpes[numero - 1].bname, carpeta_a_crear, name);

}