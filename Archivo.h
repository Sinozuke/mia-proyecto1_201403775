#ifndef ARCHIVO_H
#define ARCHIVO_H

void crear_Archivo(Montaje *montajes, Sesion *sesion, char id[], char path[], char contenido[]);
void update_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[], char name[]);
void eliminar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]);
void modificar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]);
void mostrar_Archivo(Montaje *montaje, Sesion *sesion, char id[], char path[]);

#endif /* ARCHIVO_H */

