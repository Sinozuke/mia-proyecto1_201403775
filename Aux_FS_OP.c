#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "Aux_FS_OP.h"

int hacer_copia_inodo(FILE *disco, SB_S2 *sb, Sesion *sesion, int pos, char padre[]);

void insertar_en_destino_dir_lvl0(FILE *disco, SB_S2 *sb, char padre[], char nombre[], int *pos_bc, int valor, int *salir) {

    if (*salir) {
        return;
    }

    if (*pos_bc == -1) {
        *pos_bc = insert_BC(sb, disco, nuevo_bloque_BC(padre, nombre), -1);
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + *pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT && !(*salir); i++) {
        if (bc.avd_bloque[i] == -1) {
            bc.avd_bloque[i] = valor;
            *salir = 1;
        }
    }

    insert_BC(sb, disco, bc, *pos_bc);

}

void insertar_en_destino_dir_lvl1(FILE *disco, SB_S2 *sb, char padre[], char nombre[], int *pos_ba, int valor, int *salir) {

    if (*salir) {
        return;
    }

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        insertar_en_destino_dir_lvl0(disco, sb, padre, nombre, &ba.apuntador[i], valor, salir);
    }

    insert_BA(sb, disco, ba, *pos_ba);

}

void insertar_en_destino_dir_lvl2(FILE *disco, SB_S2 *sb, char padre[], char nombre[], int *pos_ba, int valor, int *salir) {

    if (*salir) {
        return;
    }

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        insertar_en_destino_dir_lvl1(disco, sb, padre, nombre, &ba.apuntador[i], valor, salir);
    }

    insert_BA(sb, disco, ba, *pos_ba);

}

void insertar_en_destino_dir(FILE *disco, SB_S2 *sb, char padre[], inodo_S2 *destino, int valor) {

    if(valor ==-1){
        return;
    }
    
    int salir = 0;

    for (int i = 0; i < MAX_DIREC_APUNT && !salir; i++) {
        insertar_en_destino_dir_lvl0(disco, sb, padre, destino->i_name, &destino->i_bloque[i], valor, &salir);
    }

    insertar_en_destino_dir_lvl1(disco, sb, padre, destino->i_name, &destino->i_indirecto[0], valor, &salir);
    insertar_en_destino_dir_lvl2(disco, sb, padre, destino->i_name, &destino->i_indirecto[1], valor, &salir);

}

void insertar_en_destino_file_lvl1(FILE *disco, SB_S2 *sb, int *pos_ba, int valor, int *salir) {

    if (*salir) {
        return;
    }

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        if (ba.apuntador[i] == -1) {
            ba.apuntador[i] = valor;
            *salir = 1;
        }
    }

    insert_BA(sb, disco, ba, *pos_ba);

}

void insertar_en_destino_file_lvl2(FILE *disco, SB_S2 *sb, int *pos_ba, int valor, int *salir) {

    if (*salir) {
        return;
    }

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        insertar_en_destino_file_lvl1(disco, sb, &ba.apuntador[i], valor, salir);
    }

    insert_BA(sb, disco, ba, *pos_ba);

}

void insertar_en_destino_file(FILE *disco, SB_S2 *sb, inodo_S2 *destino, int valor) {

    if (valor == -1) {
        return;
    }

    int salir = 0;

    for (int i = 0; i < MAX_DIREC_APUNT && !salir; i++) {
        if (destino->i_bloque[i] == -1) {
            destino->i_bloque[i] = valor;
            salir = 1;
        }
    }

    insertar_en_destino_file_lvl1(disco, sb, &destino->i_indirecto[0], valor, &salir);
    insertar_en_destino_file_lvl2(disco, sb, &destino->i_indirecto[1], valor, &salir);

}

int hacer_copia_inodo_file_lvl0(FILE *disco, SB_S2 *sb, char padre[], int pos) {

    if (pos == -1) {
        return -1;
    }

    BD_S2 nuevo;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&nuevo, sb->sb_tamano_bloque, 1, disco);

    clean_string(nuevo.db_padre, MAX_NAME_SIZE);
    strncpy(nuevo.db_padre, padre, MAX_NAME_SIZE);

    return insert_BD(sb, disco, nuevo, -1);

}

void hacer_copia_inodo_file_lvl1(FILE *disco, SB_S2 *sb, inodo_S2 *destino, char padre[], int pos) {

    if (pos == -1) {
        return;
    }

    BA_S2 leido;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        insertar_en_destino_file(disco, sb, destino, hacer_copia_inodo_file_lvl0(disco, sb, padre, leido.apuntador[i]));
    }

}

void hacer_copia_inodo_file_lvl2(FILE *disco, SB_S2 *sb, inodo_S2 *destino, char padre[], int pos) {

    if (pos == -1) {
        return;
    }

    BA_S2 leido;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        hacer_copia_inodo_file_lvl1(disco, sb, destino, padre, leido.apuntador[i]);
    }

}

void hacer_copia_inodo_dir_lvl0(FILE *disco, SB_S2 *sb,Sesion *sesion, inodo_S2 *destino, char padre[], int pos) {

    if (pos == -1) {
        return;
    }
    
    BC_S2 leido;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb->sb_tamano_bloque, 1, disco);

    for(int i = 0;i<MAX_AVD_DIREC_APUNT;i++){
        insertar_en_destino_dir(disco,sb,padre,destino, hacer_copia_inodo(disco,sb,sesion, leido.avd_bloque[i],destino->i_name));
    }

}

void hacer_copia_inodo_dir_lvl1(FILE *disco, SB_S2 *sb,Sesion *sesion, inodo_S2 *destino, char padre[], int pos) {

    if (pos == -1) {
        return;
    }
    
    BA_S2 leido;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb->sb_tamano_bloque, 1, disco);

    for(int i = 0;i<MAX_BD_APUNTS;i++){
        hacer_copia_inodo_dir_lvl0(disco, sb,sesion, destino, padre, leido.apuntador[i]);
    }

}

void hacer_copia_inodo_dir_lvl2(FILE *disco, SB_S2 *sb,Sesion *sesion, inodo_S2 *destino, char padre[], int pos) {

    if (pos == -1) {
        return;
    }
    
    BA_S2 leido;

    fseek(disco, sb->sb_ap_ficheros + pos * sb->sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb->sb_tamano_bloque, 1, disco);

    for(int i = 0;i<MAX_BD_APUNTS;i++){
        hacer_copia_inodo_dir_lvl1(disco, sb,sesion, destino, padre, leido.apuntador[i]);
    }
}

int hacer_copia_inodo(FILE *disco, SB_S2 *sb, Sesion *sesion, int pos, char padre[]) {

    if(pos==-1){
        return -1;
    }
    
    inodo_S2 nuevo;
    inodo_S2 origen;

    fseek(disco, sb->sb_ap_inodos + pos * sb->sb_tamano_inodo, SEEK_SET);
    fread(&origen, sb->sb_tamano_inodo, 1, disco);

    nuevo = nuevo_inodo(-1, origen.i_tipo, sesion->UID, origen.i_name);

    nuevo.i_asig_bloques = origen.i_asig_bloques;
    nuevo.i_tam_archivo = origen.i_tam_archivo;

    switch (origen.i_tipo) {
        case 'a':
            for (int i = 0; i < MAX_DIREC_APUNT; i++) {
                insertar_en_destino_file(disco, sb, &nuevo, hacer_copia_inodo_file_lvl0(disco, sb, padre, origen.i_bloque[i]));
            }
            hacer_copia_inodo_file_lvl1(disco, sb, &nuevo, padre, origen.i_indirecto[0]);
            hacer_copia_inodo_file_lvl2(disco, sb, &nuevo, padre, origen.i_indirecto[1]);
            break;
        case 'c':
            
            nuevo.i_bloque[0]=insert_BC(sb,disco,nuevo_bloque_BC(padre, nuevo.i_name) , -1);
            
            for (int i = 0; i < MAX_DIREC_APUNT; i++) {
                hacer_copia_inodo_dir_lvl0(disco, sb,sesion, &nuevo,padre, origen.i_bloque[i]);
            }
            hacer_copia_inodo_dir_lvl1(disco, sb,sesion, &nuevo,padre, origen.i_indirecto[0]);
            hacer_copia_inodo_dir_lvl2(disco, sb,sesion, &nuevo,padre, origen.i_indirecto[1]);
            break;
    }

    return insert_inodo(sb, disco, &nuevo, -1);

}

void copiar(FILE *disco, SB_S2 *sb,Sesion *sesion,char padre[], int destino, int origen) {

    inodo_S2 ino_destino;

    fseek(disco, sb->sb_ap_inodos + destino * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino_destino, sb->sb_tamano_inodo, 1, disco);

    insertar_en_destino_dir(disco, sb, padre, &ino_destino, hacer_copia_inodo(disco, sb,sesion, origen, ino_destino.i_name));
    
    insert_inodo(sb, disco, &ino_destino,destino);

}