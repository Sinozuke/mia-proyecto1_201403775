#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "Aux_Directorio.h"

void enlazar_inodos_lvl0(FILE *disco, SB_S2 *sb, inodo_S2 *padre, char nombre_padre[], int *pos_bc, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_bc == -1) {
        *pos_bc = insert_BC(sb, disco, nuevo_bloque_BC(nombre_padre, padre->i_name), -1);
        padre->i_asig_bloques++;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + *pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] == -1) {
            bc.avd_bloque[i] = hijo;
            insert_BC(sb, disco, bc, *pos_bc);
            *salir = 1;
            return;
        }
    }

}

void enlazar_inodos_lvl1(FILE *disco, SB_S2 *sb, inodo_S2 *padre, char nombre_padre[], int *pos_ba, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_inodos_lvl0(disco, sb, padre, nombre_padre, &ba.apuntador[i], hijo, salir);
        if (*salir) {
            insert_BA(sb, disco, ba, *pos_ba);
            return;
        }
    }

}

void enlazar_inodos_lvl2(FILE *disco, SB_S2 *sb, inodo_S2 *padre, char nombre_padre[], int *pos_ba, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        enlazar_inodos_lvl1(disco, sb, padre, nombre_padre, &ba.apuntador[i], hijo, salir);
        if (*salir) {
            insert_BA(sb, disco, ba, *pos_ba);
            return;
        }
    }

}

void enlazar_inodos(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int hijo) {

    BC_S2 bc;
    int salir = 0;
    char nombre_padre[MAX_NAME_SIZE];
    clean_string(nombre_padre, MAX_NAME_SIZE);

    for (int i = 0; i < MAX_DIREC_APUNT && !salir; i++) {
        if (i == 0) {
            fseek(disco, sb->sb_ap_ficheros + padre->i_bloque[0] * sb->sb_tamano_bloque, SEEK_SET);
            fread(&bc, sb->sb_tamano_bloque, 1, disco);
            strncpy(nombre_padre, bc.avd_padre, MAX_NAME_SIZE);
        }
        enlazar_inodos_lvl0(disco, sb, padre, nombre_padre, &padre->i_bloque[i], hijo, &salir);
    }

    enlazar_inodos_lvl1(disco, sb, padre, nombre_padre, &padre->i_indirecto[0], hijo, &salir);
    enlazar_inodos_lvl2(disco, sb, padre, nombre_padre, &padre->i_indirecto[1], hijo, &salir);

}

void desenlazar_inodos_lvl0(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int *pos_bc, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_bc == -1) {
        return;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + *pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] == hijo) {
            bc.avd_bloque[i] = -1;
            padre->i_asig_bloques--;
            *salir = 1;
        }
    }

    int borrar = 1;

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            borrar = 0;
        }
    }

    if (borrar) {
        free_BD(sb, disco, *pos_bc);
        *pos_bc = -1;
        padre->i_asig_bloques--;
    } else {
        insert_BC(sb, disco, bc, *pos_bc);
    }


}

void desenlazar_inodos_lvl1(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int *pos_ba, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        desenlazar_inodos_lvl0(disco, sb, padre, &ba.apuntador[i], hijo, salir);
    }

    int borrar = 1;

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            borrar = 0;
        }
    }

    if (borrar) {
        free_BD(sb, disco, *pos_ba);
        *pos_ba = -1;
    } else {
        insert_BA(sb, disco, ba, *pos_ba);
    }

}

void desenlazar_inodos_lvl2(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int *pos_ba, int hijo, int *salir) {

    if (*salir)
        return;

    if (*pos_ba == -1) {
        *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && !(*salir); i++) {
        desenlazar_inodos_lvl1(disco, sb, padre, &ba.apuntador[i], hijo, salir);
    }

    int borrar = 1;

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            borrar = 0;
        }
    }

    if (borrar) {
        free_BD(sb, disco, *pos_ba);
        *pos_ba = -1;
    } else {
        insert_BA(sb, disco, ba, *pos_ba);
    }

}

void desenlazar_inodos(FILE *disco, SB_S2 *sb, inodo_S2 *padre, int hijo) {

    BC_S2 bc;
    int salir = 0;

    for (int i = 0; i < MAX_DIREC_APUNT && !salir; i++) {
        desenlazar_inodos_lvl0(disco, sb, padre, &padre->i_bloque[i], hijo, &salir);
    }

    desenlazar_inodos_lvl1(disco, sb, padre, &padre->i_indirecto[0], hijo, &salir);
    desenlazar_inodos_lvl2(disco, sb, padre, &padre->i_indirecto[1], hijo, &salir);

}

void buscar_dir_lvl0(FILE *disco, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BC, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_BC * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT && (total - 1) != *actual; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (strcmp(ino.i_name, carpes[*actual].bname) == 0 && ino.i_tipo == 'c') {
                *encontrado = ino.i_llave;
                *actual = *actual + 1;
                *i2 = -1;
                fseek(disco, sb->sb_ap_inodos + (*encontrado) * sb->sb_tamano_inodo, SEEK_SET);
                fread(ino2, sb->sb_tamano_inodo, 1, disco);
                return;
            }
        }
    }

}

void buscar_dir_lvl1(FILE *disco, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BA, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BA_S2 ba;
    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_BA * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && (total - 1) != *actual; i++) {
        if (ba.apuntador[i] != -1) {
            buscar_dir_lvl0(disco, sb, actual, encontrado, total, ba.apuntador[i], carpes, i2, ino2);
        }
    }

}

void buscar_dir_lvl2(FILE *disco, SB_S2 *sb, int *actual, int *encontrado, int total, int pos_BA, carpetas carpes[], int *i2, inodo_S2 *ino2) {

    BA_S2 ba;
    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_BA * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && (total - 1) != *actual; i++) {
        if (ba.apuntador[i] != -1) {
            buscar_dir_lvl1(disco, sb, actual, encontrado, total, ba.apuntador[i], carpes, i2, ino2);
        }
    }

}

int buscar_dir_padre(FILE *disco, SB_S2 *sb, carpetas carpes[], int total) {

    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);
    int encontrado = 0;
    int actual = 0;

    for (int i = 0; i < MAX_DIREC_APUNT + 3 && (total - 1) != actual; i++) {
        switch (i) {
            case MAX_DIREC_APUNT:
                if (ino.i_indirecto[0] != -1) {
                    buscar_dir_lvl1(disco, sb, &actual, &encontrado, total, ino.i_indirecto[0], carpes, &i, &ino);
                }
                break;
            case MAX_DIREC_APUNT + 1:
                if (ino.i_indirecto[1] != -1) {
                    buscar_dir_lvl2(disco, sb, &actual, &encontrado, total, ino.i_indirecto[1], carpes, &i, &ino);
                }
                break;
            case MAX_DIREC_APUNT + 2:
                if ((total - 1) != actual) {
                    printf("ERROR: LA CARPETA \"%s\" NO EXISTE.\n", carpes[actual].bname);
                    return -1;
                }
                break;
            default:
                if (ino.i_bloque[i] != -1) {
                    buscar_dir_lvl0(disco, sb, &actual, &encontrado, total, ino.i_bloque[i], carpes, &i, &ino);
                }
                break;
        }
    }

    return encontrado;

}

void dir_exist_lvl0(FILE *disco, SB_S2 *sb, int pos_bc, char nombre[], int *resultado, char tipo) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT && *resultado == -1; i++) {
        if (bc.avd_bloque[i] != -1) {
            fseek(disco, sb->sb_ap_inodos + bc.avd_bloque[i] * sb->sb_tamano_inodo, SEEK_SET);
            fread(&ino, sb->sb_tamano_inodo, 1, disco);
            if (strcmp(ino.i_name, nombre) == 0 && ino.i_tipo == tipo) {
                *resultado = bc.avd_bloque[i];
                return;
            }
        }
    }

}

void dir_exist_lvl1(FILE *disco, SB_S2 *sb, int pos_ba, char nombre[], int *resultado, char tipo) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && *resultado == -1; i++) {
        dir_exist_lvl0(disco, sb, ba.apuntador[i], nombre, resultado, tipo);
    }

}

void dir_exist_lvl2(FILE *disco, SB_S2 *sb, int pos_ba, char nombre[], int *resultado, char tipo) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS && *resultado == -1; i++) {
        dir_exist_lvl1(disco, sb, ba.apuntador[i], nombre, resultado, tipo);
    }

}

int dir_exist(FILE *disco, SB_S2 *sb, inodo_S2 padre, char nombre[], char tipo) {

    int resultado = -1;

    for (int i = 0; i < MAX_DIREC_APUNT && resultado == -1; i++) {
        dir_exist_lvl0(disco, sb, padre.i_bloque[i], nombre, &resultado, tipo);
    }

    dir_exist_lvl1(disco, sb, padre.i_indirecto[0], nombre, &resultado, tipo);
    dir_exist_lvl2(disco, sb, padre.i_indirecto[1], nombre, &resultado, tipo);

    return resultado;

}

void eliminar_recursivamente_dir_lvl0(FILE *disco, SB_S2 *sb, int pos_bc) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        if (bc.avd_bloque[i] != -1) {
            eliminar_recursivamente(disco, sb, bc.avd_bloque[i]);
        }
    }

    free_BD(sb, disco, pos_bc);

}

void eliminar_recursivamente_dir_lvl1(FILE *disco, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            eliminar_recursivamente_dir_lvl0(disco, sb, ba.apuntador[i]);
        }
    }

    free_BD(sb, disco, pos_ba);

}

void eliminar_recursivamente_dir_lvl2(FILE *disco, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            eliminar_recursivamente_dir_lvl1(disco, sb, ba.apuntador[i]);
        }
    }

    free_BD(sb, disco, pos_ba);

}

void eliminar_recursivamente_archivo_lvl1(FILE *disco, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            free_BD(sb, disco, ba.apuntador[i]);
        }
    }

    free_BD(sb, disco, pos_ba);

}

void eliminar_recursivamente_archivo_lvl2(FILE *disco, SB_S2 *sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            eliminar_recursivamente_archivo_lvl1(disco, sb, ba.apuntador[i]);
        }
    }

    free_BD(sb, disco, pos_ba);

}

void eliminar_recursivamente(FILE *disco, SB_S2 *sb, int pos_ino) {

    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos + pos_ino * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    if (ino.i_tipo == 'c') {

        for (int i = 0; i < MAX_DIREC_APUNT; i++) {
            eliminar_recursivamente_dir_lvl0(disco, sb, ino.i_bloque[i]);
        }

        eliminar_recursivamente_dir_lvl1(disco, sb, ino.i_indirecto[0]);
        eliminar_recursivamente_dir_lvl2(disco, sb, ino.i_indirecto[1]);

    } else {
        for (int i = 0; i < MAX_DIREC_APUNT; i++) {
            if (ino.i_bloque[i] != -1) {
                free_BD(sb, disco, ino.i_bloque[i]);
            }
        }

        eliminar_recursivamente_archivo_lvl1(disco, sb, ino.i_indirecto[0]);
        eliminar_recursivamente_archivo_lvl2(disco, sb, ino.i_indirecto[1]);
    }


    free_inodo(sb, disco, ino.i_llave);

}

void update_directorio_padre_lvl0(FILE *disco, SB_S2 *sb, int pos_bc, char name[]) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    clean_string(bc.avd_padre, MAX_NAME_SIZE);
    strncpy(bc.avd_padre, name, MAX_NAME_SIZE);

    insert_BC(sb, disco, bc, pos_bc);

}

void update_directorio_padre_lvl1(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_directorio_padre_lvl0(disco, sb, ba.apuntador[i], name);
    }

}

void update_directorio_padre_lvl2(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_directorio_padre_lvl1(disco, sb, ba.apuntador[i], name);
    }


}

void update_directorio_padre(FILE *disco, SB_S2 *sb, int posicion_inodo, char name[]) {

    if (posicion_inodo == -1) {
        return;
    }

    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos + posicion_inodo * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    if (ino.i_tipo != 'c') {
        return;
    }

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        update_directorio_padre_lvl0(disco, sb, ino.i_bloque[i], name);
    }

    update_directorio_padre_lvl1(disco, sb, ino.i_indirecto[0], name);
    update_directorio_padre_lvl2(disco, sb, ino.i_indirecto[1], name);

}

void update_directorio_lvl0(FILE *disco, SB_S2 *sb, int pos_bc, char name[]) {

    if (pos_bc == -1) {
        return;
    }

    BC_S2 bc;

    fseek(disco, sb->sb_ap_ficheros + pos_bc * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bc, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        update_directorio_padre(disco, sb, bc.avd_bloque[i], name);
    }

}

void update_directorio_lvl1(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_directorio_lvl0(disco, sb, ba.apuntador[i], name);
    }

}

void update_directorio_lvl2(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_directorio_lvl1(disco, sb, ba.apuntador[i], name);
    }

}

void update_directorio(FILE *disco, SB_S2 *sb, int posicion_inodo, char name[]) {

    inodo_S2 ino;
    BD_S2 bd;

    fseek(disco, sb->sb_ap_inodos + posicion_inodo * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    nueva_bitacora('2', ino.i_tipo, ino.i_name, *sb, disco);

    clean_string(ino.i_name, MAX_NAME_SIZE);
    strncpy(ino.i_name, name, MAX_NAME_SIZE);

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        update_directorio_lvl0(disco, sb, ino.i_bloque[i], name);
    }

    update_directorio_lvl1(disco, sb, ino.i_indirecto[0], name);
    update_directorio_lvl2(disco, sb, ino.i_indirecto[1], name);

    insert_inodo(sb, disco, &ino, ino.i_llave);

    nueva_bitacora('1', ino.i_tipo, ino.i_name, *sb, disco);


}