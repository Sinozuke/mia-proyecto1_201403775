#ifndef AUX_FS_H
#define AUX_FS_H


inodo_S2 nuevo_inodo(int i_llave, char i_tipo, int propetario, char nombre[]);
int insert_inodo(SB_S2 *sb, FILE *disco, inodo_S2 *inode,int mod);

BD_S2 nuevo_bloque_BD(int *posicion,int longitud, char contenido[], char nombre[]);
int insert_BD(SB_S2 *sb, FILE *disco, BD_S2 bloque, int mod);

BC_S2 nuevo_bloque_BC(char padre[], char nombre[]);
int insert_BC(SB_S2 *sb, FILE *disco, BC_S2 bloque, int mod);

BA_S2 nuevo_bloque_BA();
int insert_BA(SB_S2 *sb, FILE *disco, BA_S2 bloque, int mod);

Log_S2 bitacora_vacia();
void nueva_bitacora(char tipo_op, char tipo, char nombre[], SB_S2 sb, FILE *disco);

void insert_SB_S2(FILE *disco, SB_S2 *sb, int comienzo);

void free_BD(SB_S2 *sb, FILE *disco,int posicion);

void free_inodo(SB_S2 *sb, FILE *disco,int posicion);

#endif /* AUX_FS_H */

