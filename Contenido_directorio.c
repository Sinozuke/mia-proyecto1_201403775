#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Reportes.h"
#include "Montaje.h"
#include "Aux_Directorio.h"
#include "Aux_carpes.h"

void escribir_inodo_dir_lvl0(FILE *disco, FILE *reporte, SB_S2 *sb, int bd){
    
    if(bd==-1){
        return;
    }
    
    BC_S2 bloque;
    inodo_S2 ino;
    
    fseek(disco,sb->sb_ap_ficheros+bd*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    for(int i=0;i<MAX_AVD_DIREC_APUNT;i++){
        if(bloque.avd_bloque[i]!=-1){
            fseek(disco,sb->sb_ap_inodos+bloque.avd_bloque[i]*sb->sb_tamano_inodo,SEEK_SET);
            fread(&ino,sb->sb_tamano_inodo,1,disco);
            if(ino.i_tipo=='a'){
                fprintf(reporte, "<TR>\n<TD BGCOLOR=\"%s\">%s</TD><TD BGCOLOR=\"%s\">%i</TD>\n</TR>\n",BD_COLOR, ino.i_name,BA_COLOR, bloque.avd_bloque[i]);
            }
        }
    }
}

void escribir_inodo_dir_lvl1(FILE *disco, FILE *reporte, SB_S2 *sb, int ba){

    if(ba==-1){
        return;
    }
    
    BA_S2 bloque;
    
    fseek(disco,sb->sb_ap_ficheros+ba*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    for(int i=0;i<MAX_BD_APUNTS;i++){
        escribir_inodo_dir_lvl0(disco, reporte, sb, bloque.apuntador[i]);
    }

}

void escribir_inodo_dir_lvl2(FILE *disco, FILE *reporte, SB_S2 *sb, int ba){

    if(ba==-1){
        return;
    }
    
    BA_S2 bloque;
    
    fseek(disco,sb->sb_ap_ficheros+ba*sb->sb_tamano_bloque,SEEK_SET);
    fread(&bloque,sb->sb_tamano_bloque,1,disco);
    
    for(int i=0;i<MAX_BD_APUNTS;i++){
        escribir_inodo_dir_lvl1(disco, reporte, sb, bloque.apuntador[i]);
    }
    
}

void escribir_inodo_dir(FILE *disco, FILE *reporte, SB_S2 *sb, int i) {

    inodo_S2 ino;
    int total = 0;

    fseek(disco, sb->sb_ap_inodos + i * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    fprintf(reporte, "\"I%i\" [label=<\n", i);
    fprintf(reporte, "<TABLE BGCOLOR=\"%s\">\n",BC_COLOR);
    fprintf(reporte, "<TR>\n<TD COLSPAN=\"2\">%s</TD>\n</TR>\n", ino.i_name);
    for (int j = 0; j < MAX_DIREC_APUNT; j++) {
        escribir_inodo_dir_lvl0(disco,reporte,sb, ino.i_bloque[j]);
    }      
    escribir_inodo_dir_lvl1(disco,reporte,sb, ino.i_indirecto[0]);
    escribir_inodo_dir_lvl2(disco,reporte,sb, ino.i_indirecto[1]);
    fprintf(reporte, "</TABLE>>];\n");

    
}

void rep_Directorio3(Montaje *montaje, char *id, char *path, char path_mostrar[]) {

    char directorio[MAXLON + 1];

    clean_string(directorio, MAXLON);

    strncpy(directorio, path, MAXLON);

    if (!strstr(directorio, ".svg")) {
        strncat(directorio, ".svg", 4);
    }

    if (crear_directorios(path) == 0) {
        printf("ERROR: el valor ingresado para path es erroneo.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    char path_disco[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    strncpy(path_disco, montaje[id[2] - 97].path, strlen(montaje[id[2] - 97].path));

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("ERROR: La Particion Montada No esta formateada con un sistema de archivos valido.\n");
        printf("-----------REPORTE FALLIDO-----------\n\n");
        return;
    }

    FILE *disco, *reporte;

    char comando_dot[300] = "dot -Tsvg -o ";
    char comando_abertura[300] = "gnome-open ";

    strncat(comando_dot, "\"", 1);
    strncat(comando_dot, directorio, strlen(directorio));
    strncat(comando_dot, "\" DIR2.dot", 10);

    strncat(comando_abertura, "\"", 1);
    strncat(comando_abertura, directorio, strlen(directorio));
    strncat(comando_abertura, "\"", 1);

    disco = fopen(path_disco, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        return;
    }

    reporte = fopen("DIR2.dot", "w");

    if (!reporte) {
        printf("ERROR: el Reporte no ha podido Abrirse.\n");
        printf("-------------CREACION FALLIDA--------------\n\n");
        fclose(disco);
        return;
    }

    SB_S2 sb;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    char carpeta_a_crear[MAXLON];
    strncpy(carpeta_a_crear, path_mostrar, MAXLON);

    char carpeta_final[MAX_NAME_SIZE];

    char *carpeta_p;
    char copia_path[strlen(path_mostrar)];
    clean_string(copia_path, strlen(path_mostrar));
    strncpy(copia_path, path_mostrar, strlen(path_mostrar));
    carpeta_p = strtok(copia_path, "/");
    while (carpeta_p) {
        if (strlen(carpeta_p) + 1 > MAX_NAME_SIZE) {
            printf("ERROR: la carpeta \"%s\" sobrepasa la longitud maxima para una carpeta.\n", carpeta_p);
            fclose(disco);
            fclose(reporte);
            return;
        }
        strncpy(carpeta_final, carpeta_p, MAX_NAME_SIZE);
        carpeta_p = strtok(NULL, "/");
    }

    int numero = contar_carpes(carpeta_a_crear);
    carpetas carpes[numero];

    vaciar_carpes(carpes, numero);
    llenar_carpes(carpes, carpeta_a_crear);

    int padre = buscar_dir_padre(disco, &sb, carpes, numero);

    if (padre == -1) {
        fclose(disco);
        fclose(reporte);
        return;
    }

    inodo_S2 carpeta_padre;
    fseek(disco, sb.sb_ap_inodos + padre * sb.sb_tamano_inodo, SEEK_SET);
    fread(&carpeta_padre, sb.sb_tamano_inodo, 1, disco);

    int existe = dir_exist(disco, &sb, carpeta_padre, carpes[numero - 1].bname, 'c');

    if (existe == -1) {
        printf("ERROR: NO EXISTE EL ARCHIVO CON EL NOMBRE \"%s\"\n", carpes[numero - 1].bname);
        fclose(disco);
        fclose(reporte);
        return;
    }

    fprintf(reporte, "digraph structs {\n");
    fprintf(reporte, "\tnode [shape=plaintext];\n");

    escribir_inodo_dir(disco, reporte, &sb, existe);

    fprintf(reporte, "}");


    fclose(reporte);
    fclose(disco);

    system(comando_dot);
    sleep(SLEEP_TIME);
    system(comando_abertura);
    printf("-- REPORTE DIR4 GENERADO --\n");
    sleep(SLEEP_TIME);

}