#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_Archivo.h"
#include "Aux_FS.h"

void contenido_archivo_lvl0(char *buffer, FILE *disco, SB_S2 sb, int pos_bd) {

    if (pos_bd == -1) {
        return;
    }

    BD_S2 leido;

    fseek(disco, sb.sb_ap_ficheros + pos_bd * sb.sb_tamano_bloque, SEEK_SET);
    fread(&leido, sb.sb_tamano_bloque, 1, disco);
    strncat(buffer, leido.db_data, MAX_BD_CONT_SIZE);

}

void contenido_archivo_lvl1(char *buffer, FILE *disco, SB_S2 sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb.sb_ap_ficheros + pos_ba * sb.sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb.sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contenido_archivo_lvl0(buffer, disco, sb, ba.apuntador[i]);
    }

}

void contenido_archivo_lvl2(char *buffer, FILE *disco, SB_S2 sb, int pos_ba) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb.sb_ap_ficheros + pos_ba * sb.sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb.sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        contenido_archivo_lvl1(buffer, disco, sb, ba.apuntador[i]);
    }

}

void contenido_archivo(char *buffer, FILE *disco, SB_S2 sb, inodo_S2 inode) {

    BD_S2 leido;
    inodo_S2 leidoi = inode;

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        contenido_archivo_lvl0(buffer, disco, sb, leidoi.i_bloque[i]);
    }

    contenido_archivo_lvl1(buffer, disco, sb, leidoi.i_indirecto[0]);
    contenido_archivo_lvl2(buffer, disco, sb, leidoi.i_indirecto[1]);

}

void editar_archivo_lvl0(FILE *disco, SB_S2 *sb, inodo_S2 *ino, int *pos_bd, int *recorrido, int longitud, char cont[]) {

    if (*pos_bd == -1) {
        if (*recorrido != longitud) {
            *pos_bd = insert_BD(sb, disco, nuevo_bloque_BD(recorrido, longitud, cont, ino->i_name), -1);
            ino->i_asig_bloques++;
            return;
        } else {
            return;
        }
    }

    if (*recorrido != longitud) { /* NO TERMINE DE ESCRIBIR ARCHIVO */
        insert_BD(sb, disco, nuevo_bloque_BD(recorrido, longitud, cont, ino->i_name), *pos_bd);
        ino->i_asig_bloques++;
    } else { /* YA TERMINE DE ESCRIBIR ARCHIVO */
        free_BD(sb, disco, *pos_bd);
        *pos_bd = -1;
    }

}

void editar_archivo_lvl1(FILE *disco, SB_S2 *sb, inodo_S2 *ino, int *pos_ba, int *recorrido, int longitud, char cont[]) {

    if (*pos_ba == -1) {
        if (*recorrido != longitud) { /* NO TERMINE DE ESCRIBIR ARCHIVO */
            *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
        } else { /* YA TERMINE DE ESCRIBIR ARCHIVO */
            return;
        }
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        editar_archivo_lvl0(disco, sb, ino, &ba.apuntador[i], recorrido, longitud, cont);
    }

    int liberar_ba = 1;

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            liberar_ba = 0;
        }
    }

    if (liberar_ba) { /* BLOQUE DE APUNTADORES CONTIENE SOLO APUNTADORES NULOS */
        free_BD(sb, disco, *pos_ba);
        *pos_ba = -1;
    } else { /* EL BLOQUE DE APUNTADORES CONTIENE UN BLOQUE NO NULO */
        insert_BA(sb, disco, ba, *pos_ba);
    }

}

void editar_archivo_lvl2(FILE *disco, SB_S2 *sb, inodo_S2 *ino, int *pos_ba, int *recorrido, int longitud, char cont[]) {

    if (*pos_ba == -1) {
        if (*recorrido != longitud) { /* NO TERMINE DE ESCRIBIR ARCHIVO */
            *pos_ba = insert_BA(sb, disco, nuevo_bloque_BA(), -1);
        } else { /* YA TERMINE DE ESCRIBIR ARCHIVO */
            return;
        }
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + *pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        editar_archivo_lvl1(disco, sb, ino, &ba.apuntador[i], recorrido, longitud, cont);
    }

    int liberar_ba = 1;

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        if (ba.apuntador[i] != -1) {
            liberar_ba = 0;
        }
    }

    if (liberar_ba) { /* BLOQUE DE APUNTADORES CONTIENE SOLO APUNTADORES NULOS */
        free_BD(sb, disco, *pos_ba);
        *pos_ba = -1;
    } else { /* EL BLOQUE DE APUNTADORES CONTIENE UN BLOQUE NO NULO */
        insert_BA(sb, disco, ba, *pos_ba);
    }

}

void editar_archivo(FILE *disco, SB_S2 *sb, int posicion_inodo, char cont[], char name[]) {
    int longitud = strlen(cont);
    int recorrido = 0;
    inodo_S2 ino;

    fseek(disco, sb->sb_ap_inodos + posicion_inodo * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    ino.i_asig_bloques = 0;
    strncpy(ino.i_name, name, MAX_NAME_SIZE);
    ino.i_tam_archivo = longitud;


    for (int i = 0; i < MAX_DIREC_APUNT && recorrido != longitud; i++) {
        editar_archivo_lvl0(disco, sb, &ino, &ino.i_bloque[i], &recorrido, longitud, cont);
    }

    editar_archivo_lvl1(disco, sb, &ino, &ino.i_indirecto[0], &recorrido, longitud, cont);
    editar_archivo_lvl2(disco, sb, &ino, &ino.i_indirecto[1], &recorrido, longitud, cont);

    insert_inodo(sb, disco, &ino, ino.i_llave);

    nueva_bitacora('2', ino.i_tipo, ino.i_name, *sb, disco);

}

void update_archivo_lvl0(FILE *disco, SB_S2 *sb, int pos_bd, char name[]) {

    if (pos_bd == -1) {
        return;
    }

    BD_S2 bd;

    fseek(disco, sb->sb_ap_ficheros + pos_bd * sb->sb_tamano_bloque, SEEK_SET);
    fread(&bd, sb->sb_tamano_bloque, 1, disco);


    clean_string(bd.db_nombre, MAX_NAME_SIZE);
    strncpy(bd.db_nombre, name, MAX_NAME_SIZE);
    insert_BD(sb, disco, bd, pos_bd);

}

void update_archivo_lvl1(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_archivo_lvl0(disco, sb, ba.apuntador[i], name);
    }

}

void update_archivo_lvl2(FILE *disco, SB_S2 *sb, int pos_ba, char name[]) {

    if (pos_ba == -1) {
        return;
    }

    BA_S2 ba;

    fseek(disco, sb->sb_ap_ficheros + pos_ba * sb->sb_tamano_bloque, SEEK_SET);
    fread(&ba, sb->sb_tamano_bloque, 1, disco);

    for (int i = 0; i < MAX_BD_APUNTS; i++) {
        update_archivo_lvl1(disco, sb, ba.apuntador[i], name);
    }

}

void update_archivo(FILE *disco, SB_S2 *sb, int posicion_inodo, char name[]) {

    inodo_S2 ino;
    BD_S2 bd;

    fseek(disco, sb->sb_ap_inodos + posicion_inodo * sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino, sb->sb_tamano_inodo, 1, disco);

    nueva_bitacora('2', ino.i_tipo, ino.i_name, *sb, disco);

    clean_string(ino.i_name, MAX_NAME_SIZE);
    strncpy(ino.i_name, name, MAX_NAME_SIZE);

    for (int i = 0; i < MAX_DIREC_APUNT; i++) {
        update_archivo_lvl0(disco, sb, ino.i_bloque[i], name);
    }

    update_archivo_lvl1(disco, sb, ino.i_indirecto[0], name);
    update_archivo_lvl2(disco, sb, ino.i_indirecto[1], name);

    insert_inodo(sb, disco, &ino, ino.i_llave);

    nueva_bitacora('1', ino.i_tipo, ino.i_name, *sb, disco);

}