#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Aux_FS.h"
#include "primeros.h"

int insert_inodo(SB_S2 *sb, FILE *disco, inodo_S2 *inode,int mod) {

    char lleno = '1';
    
    if(mod==-1)
        inode->i_llave = sb->sb_first_free_inodo;
    else
        inode->i_llave = mod;

    if(mod==-1)
        fseek(disco, sb->sb_ap_bitmap_inodos + sb->sb_first_free_inodo, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_bitmap_inodos + mod, SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);

    
    if(mod==-1)
        fseek(disco, sb->sb_ap_inodos + sb->sb_first_free_inodo * sizeof (inodo_S2), SEEK_SET);
    else
        fseek(disco, sb->sb_ap_inodos + mod * sb->sb_tamano_inodo, SEEK_SET);
        
    fwrite(inode, sizeof (inodo_S2), 1, disco);

    int retorno;

    if(mod==-1){
        retorno = sb->sb_first_free_inodo;
        sb->sb_first_free_inodo = primer_inode_libre(disco, *sb);
        sb->sb_inodos_free--;
    }else{
        retorno = mod;
    }

    return retorno;

}

int insert_BD(SB_S2 *sb, FILE *disco, BD_S2 bloque,int mod) {

    char lleno = 'd';

    if(mod==-1)
        fseek(disco, sb->sb_ap_bitmap_ficheros + sb->sb_first_free_fichero, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_bitmap_ficheros + mod, SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);

    if(mod==-1)
        fseek(disco, sb->sb_ap_ficheros + sb->sb_first_free_fichero * sb->sb_tamano_bloque, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_ficheros + mod * sb->sb_tamano_bloque, SEEK_SET);    
    fwrite(&bloque, sizeof (BD_S2), 1, disco);

    int retorno;

    if(mod==-1){
        retorno = sb->sb_first_free_fichero;
        sb->sb_first_free_fichero = primer_BD_libre(disco, *sb);
        sb->sb_bloques_free--;
    }else{
        retorno = mod;
    }
    
    return retorno;

}

void free_BD(SB_S2 *sb, FILE *disco,int posicion){

    char vacio='0';
    
    fseek(disco, sb->sb_ap_bitmap_ficheros + posicion, SEEK_SET);
    fwrite(&vacio, sizeof (char), 1, disco);
    if (sb->sb_first_free_fichero > posicion)
        sb->sb_first_free_fichero = posicion;
    sb->sb_bloques_free++;
    

}

void free_inodo(SB_S2 *sb, FILE *disco,int posicion){

    char vacio='0';
    
    fseek(disco, sb->sb_ap_bitmap_inodos + posicion, SEEK_SET);
    fwrite(&vacio, sizeof (char), 1, disco);
    if (sb->sb_first_free_inodo > posicion)
        sb->sb_first_free_inodo = posicion;
    sb->sb_inodos_free++;

}

int insert_BC(SB_S2 *sb, FILE *disco, BC_S2 bloque,int mod) {

    char lleno = 'c';

    if(mod==-1)
        fseek(disco, sb->sb_ap_bitmap_ficheros + sb->sb_first_free_fichero, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_bitmap_ficheros + mod, SEEK_SET);
    fwrite(&lleno, sizeof (char), 1, disco);

    if(mod==-1)
        fseek(disco, sb->sb_ap_ficheros + sb->sb_first_free_fichero * sb->sb_tamano_bloque, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_ficheros + mod * sb->sb_tamano_bloque, SEEK_SET);
        
    fwrite(&bloque, sizeof (BC_S2), 1, disco);

    int retorno;

    if(mod==-1){
        retorno = sb->sb_first_free_fichero;
        sb->sb_first_free_fichero = primer_BD_libre(disco, *sb);
        sb->sb_bloques_free--;
    }else{
        retorno = mod;
    }
    
    return retorno;

}

int insert_BA(SB_S2 *sb, FILE *disco, BA_S2 bloque, int mod) {

    char lleno = 'a';

    if(mod==-1)
        fseek(disco, sb->sb_ap_bitmap_ficheros + sb->sb_first_free_fichero, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_bitmap_ficheros + mod, SEEK_SET);
        
    fwrite(&lleno, sizeof (char), 1, disco);

    if(mod==-1)
        fseek(disco, sb->sb_ap_ficheros + sb->sb_first_free_fichero * sb->sb_tamano_bloque, SEEK_SET);
    else
        fseek(disco, sb->sb_ap_ficheros + mod * sb->sb_tamano_bloque, SEEK_SET);
    
    fwrite(&bloque, sizeof (BA_S2), 1, disco);

    int retorno;

    if(mod==-1){
        retorno = sb->sb_first_free_fichero;
        sb->sb_first_free_fichero = primer_BD_libre(disco, *sb);
        sb->sb_bloques_free--;
    }else{
        retorno = mod;
    }
    
    return retorno;

}

void escribir_bitacora(Log_S2 bitacora, FILE *disco, int comienzo, int n) {

    Log_S2 leida;
    fseek(disco, comienzo, SEEK_SET);
    for (int i = 0; i < NUMBER_LOGS_FS * n; i++) {
        fread(&leida, sizeof (Log_S2), 1, disco);
        if (leida.log_tipo_operacion == '0') {
            fseek(disco, -sizeof (Log_S2), SEEK_CUR);
            fwrite(&bitacora, sizeof (Log_S2), 1, disco);
            return;
        }
    }
}

inodo_S2 nuevo_inodo(int i_llave,char i_tipo,int propetario,char nombre[]){

    inodo_S2 nuevo_inodo;
    
    clean_string(nuevo_inodo.i_idPermisos,3);
    clean_string(nuevo_inodo.i_name,MAX_NAME_SIZE);
    
    nuevo_inodo.i_asig_bloques=0;
    nuevo_inodo.i_llave=i_llave;
    for(int i=0;i<MAX_DIREC_APUNT;i++){
        nuevo_inodo.i_bloque[i]=-1;
    }
    nuevo_inodo.i_fecha_creacion=time(NULL);
    strncpy(nuevo_inodo.i_idPermisos,"664",3);
    nuevo_inodo.i_idPropetario = propetario;
    for(int  i=0;i<MAX_INDIREC_APUNT;i++){
        nuevo_inodo.i_indirecto[i]=-1;
    }
    strncpy(nuevo_inodo.i_name,nombre,strlen(nombre));
    nuevo_inodo.i_tam_archivo=0;
    nuevo_inodo.i_tipo=i_tipo;
    
    return nuevo_inodo;
}

BD_S2 nuevo_bloque_BD(int *posicion,int longitud, char contenido[], char nombre[]) {

    BD_S2 bloque_nuevo;

    clean_string(bloque_nuevo.db_data, MAX_BD_CONT_SIZE);
    clean_string(bloque_nuevo.db_nombre, MAX_NAME_SIZE);
    clean_string(bloque_nuevo.db_padre, MAX_NAME_SIZE);

    for (int i = 0; i < MAX_BD_CONT_SIZE && *posicion < longitud; i++, (*posicion)++)
        bloque_nuevo.db_data[i] = contenido[*posicion];
    strncpy(bloque_nuevo.db_nombre, nombre, strlen(nombre));
    return bloque_nuevo;

}

BC_S2 nuevo_bloque_BC(char padre[], char nombre[]) {

    BC_S2 nuevo_bloque;

    clean_string(nuevo_bloque.avd_nombre_directorio, MAX_NAME_SIZE);
    clean_string(nuevo_bloque.avd_padre, MAX_NAME_SIZE);
    for (int i = 0; i < MAX_AVD_DIREC_APUNT; i++) {
        nuevo_bloque.avd_bloque[i] = -1;
    }
    strncpy(nuevo_bloque.avd_nombre_directorio,nombre,strlen(nombre));
    strncpy(nuevo_bloque.avd_padre,padre,strlen(padre));

    return nuevo_bloque;

}

BA_S2 nuevo_bloque_BA(){

    BA_S2 nuevo_bloque;
    
    for(int i=0;i<MAX_BD_APUNTS;i++){
        nuevo_bloque.apuntador[i]=-1;
    }
    
    return nuevo_bloque;

}

Log_S2 bitacora_vacia(){
    
    Log_S2 devolver;

    clean_string(devolver.log_nombre,MAX_NAME_SIZE);
    

    devolver.Journal_fecha = time(NULL);
    devolver.log_tipo = '0';
    devolver.log_tipo_operacion = '0';

    return devolver;
    
}

void nueva_bitacora(char tipo_op, char tipo, char nombre[],SB_S2 sb,FILE *disco) {

    Log_S2 devolver;

    clean_string(devolver.log_nombre,MAX_NAME_SIZE);

    devolver.Journal_fecha = time(NULL);
    strncpy(devolver.log_nombre, nombre, MAX_NAME_SIZE);
    devolver.log_tipo = tipo;
    devolver.log_tipo_operacion = tipo_op;
    
    escribir_bitacora(devolver, disco, sb.sb_ap_ficheros+sb.sb_numero_bloques*sb.sb_tamano_bloque, sb.sb_numero_inodos);

}

void insert_SB_S2(FILE *disco, SB_S2 *sb,int comienzo){

    fseek(disco,comienzo,SEEK_SET);
    fwrite(sb,sizeof(SB_S2),1,disco);

}

