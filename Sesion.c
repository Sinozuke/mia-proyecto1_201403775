#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "main_data.h"
#include "main_functions.h"
#include "Sesion.h"
#include "Montaje.h"
#include "Aux_Archivo.h"
#include "Aux_Sesion.h"

void login(Sesion *sesion, Montaje *montaje, char id[], char username[], char password[]) {

    if (sesion->UID != -1) {
        printf("Error: no se puede inciar sesion si ya hay una activa.\n");
        return;
    }

    char path[100];

    if (strlen(montaje[id[2] - 97].path) == 0) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }
    strcpy(path, montaje[id[2] - 97].path);

    int a = encontrar_id(montaje, id[2] - 97, id);

    if (a == -1) {
        printf("ERROR: No existe ninguna particion montada como \"%s\".\n", id);
        printf("----------FORMATEO FALLIDO------------");
        return;
    }

    ids encontrado = montaje[id[2] - 97].lista[a];

    if (encontrado.status != 'f') {
        printf("Error: la particion montada no ha sido formateada.\n");
        return;
    }

    FILE *disco;

    disco = fopen(path, "r+b");

    if (!disco) {
        printf("ERROR: el Disco no ha podido Abrirse.\n");
        printf("--------------FORMATEO FALLIDO-----------------\n");
        return;
    }

    SB_S2 sb;
    inodo_S2 users;

    fseek(disco, encontrado.comienzo, SEEK_SET);
    fread(&sb, sizeof (SB_S2), 1, disco);

    fseek(disco, sb.sb_ap_inodos+sb.sb_tamano_inodo, SEEK_SET);
    fread(&users, sizeof (inodo_S2), 1, disco);

    char contenido[users.i_tam_archivo];
    
    clean_string(contenido,users.i_tam_archivo);

    contenido_archivo(contenido, disco, sb, users);

    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    fclose(disco);

    int bandera = 1;

    for (int i = 0; i < numero_registros; i++) {
        if (registros[i].tipo == 'u') {
            if (strcmp(registros[i].username, username) == 0) {
                if (strcmp(registros[i].password, password) == 0) {
                    sesion->UID = registros[i].id;
                    for (int j = 0; j < numero_registros && bandera; j++){
                        if (registros[j].tipo == 'g'){
                            if (strcmp(registros[i].grupo, registros[j].grupo) == 0){
                                sesion->GID = registros[j].id;
                            }
                        }
                    }
                    sesion->lista = a;
                    sesion->montaje = id[2] - 97;
                    printf("--Logeado como \"%s\"--\n", username);
                    return;
                } else {
                    printf("--Contaseña Incorrecta--\n");
                    return;
                }
            }
        }
    }

    printf("--No Hay Ningun usuario como \"%s\"\n", username);

}

void logout(Sesion *sesion) {
    
    if(sesion->UID==-1){
        printf("Error: no hay niguna sesion iniciada.\n");
        return;
    }
    
    Sesion sesion1 = {-1, -1, -1, -1};
    *sesion = sesion1;
    
    printf("-- Sesion Finalizada --\n");
}

int can_read(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo){

    int access = 0;
    
    inodo_S2 users;

    fseek(disco, sb->sb_ap_inodos+sb->sb_tamano_inodo, SEEK_SET);
    fread(&users, sb->sb_tamano_inodo, 1, disco);

    inodo_S2 ino_leido;

    fseek(disco, sb->sb_ap_inodos+inodo*sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino_leido, sb->sb_tamano_inodo, 1, disco);
    
    char contenido[users.i_tam_archivo];
    
    clean_string(contenido,users.i_tam_archivo);

    contenido_archivo(contenido, disco, *sb, users);

    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    
    int grupo_propetario=-1;
    
    int salir=0;
    
    for (int i = 0; i < numero_registros && !salir; i++) {
        if (registros[i].tipo == 'u') {
            if (registros[i].id==ino_leido.i_idPropetario) {
                for (int j = 0; j < numero_registros && !salir; j++){
                    if (registros[j].tipo == 'g'){
                        if (strcmp(registros[i].grupo, registros[i].grupo) == 0){
                            grupo_propetario=registros[i].id;
                            salir=1;
                        }
                    }
                }
            }
        }
    }
    
    int tipo_accesso=-1;
    
    if(sesion->UID==ino_leido.i_idPropetario || sesion->UID==ROOT_ID){
        tipo_accesso=0;
    }else if(sesion->GID==grupo_propetario){
        tipo_accesso=1;
    }else{
        tipo_accesso=2;
    }
    
    switch(ino_leido.i_idPermisos[tipo_accesso]){
//        case '0':
//            break;
//        case '1':
//            break;
//        case '2':
//            break;
//        case '3':
//            break;
        case '4':
            access = 1;
            break;
        case '5':
            access = 1;
            break;
        case '6':
            access = 1;
            break;
        case '7':
            access = 1;
            break;
    }
    
    return access;

}

int can_write(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo){

    int access = 0;
    
    inodo_S2 users;

    fseek(disco, sb->sb_ap_inodos+sb->sb_tamano_inodo, SEEK_SET);
    fread(&users, sb->sb_tamano_inodo, 1, disco);

    inodo_S2 ino_leido;

    fseek(disco, sb->sb_ap_inodos+inodo*sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino_leido, sb->sb_tamano_inodo, 1, disco);
    
    char contenido[users.i_tam_archivo];
    
    clean_string(contenido,users.i_tam_archivo);

    contenido_archivo(contenido, disco, *sb, users);

    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    
    int grupo_propetario=-1;
    
    int salir=0;
    
    for (int i = 0; i < numero_registros && !salir; i++) {
        if (registros[i].tipo == 'u') {
            if (registros[i].id==ino_leido.i_idPropetario) {
                for (int j = 0; j < numero_registros && !salir; j++){
                    if (registros[j].tipo == 'g'){
                        if (strcmp(registros[i].grupo, registros[i].grupo) == 0){
                            grupo_propetario=registros[i].id;
                            salir=1;
                        }
                    }
                }
            }
        }
    }
    
    int tipo_accesso=-1;
    
    if(sesion->UID==ino_leido.i_idPropetario || sesion->UID==ROOT_ID){
        tipo_accesso=0;
    }else if(sesion->GID==grupo_propetario){
        tipo_accesso=1;
    }else{
        tipo_accesso=2;
    }
    
    switch(ino_leido.i_idPermisos[tipo_accesso]){
//        case '0':
//            access = 1;
//            break;
//        case '1':
//            access = 1;
//            break;
        case '2':
            access = 1;
            break;
        case '3':
            access = 1;
            break;
//        case '4':
//            access = 1;
//            break;
//        case '5':
//            access = 1;
//            break;
        case '6':
            access = 1;
            break;
        case '7':
            access = 1;
            break;
    }
    
    return access;

}

int check_access(FILE *disco,SB_S2 *sb,Sesion *sesion,int inodo){

    int access = -1;
    
    inodo_S2 users;

    fseek(disco, sb->sb_ap_inodos+sb->sb_tamano_inodo, SEEK_SET);
    fread(&users, sb->sb_tamano_inodo, 1, disco);

    inodo_S2 ino_leido;

    fseek(disco, sb->sb_ap_inodos+inodo*sb->sb_tamano_inodo, SEEK_SET);
    fread(&ino_leido, sb->sb_tamano_inodo, 1, disco);
    
    char contenido[users.i_tam_archivo];
    
    clean_string(contenido,users.i_tam_archivo);

    contenido_archivo(contenido, disco, *sb, users);

    int numero_registros = contar_saltos(contenido);
    Registro registros[numero_registros];
    vaciar(registros, numero_registros);
    almacenar_registros(registros, contenido);
    
    int grupo_propetario=-1;
    
    int salir=0;
    
    for (int i = 0; i < numero_registros && !salir; i++) {
        if (registros[i].tipo == 'u') {
            if (registros[i].id==ino_leido.i_idPropetario) {
                for (int j = 0; j < numero_registros && !salir; j++){
                    if (registros[j].tipo == 'g'){
                        if (strcmp(registros[i].grupo, registros[i].grupo) == 0){
                            grupo_propetario=registros[i].id;
                            salir=1;
                        }
                    }
                }
            }
        }
    }
    
    int tipo_accesso=-1;
    
    if(sesion->UID==ino_leido.i_idPropetario){
        tipo_accesso=0;
    }else if(sesion->GID==grupo_propetario){
        tipo_accesso=1;
    }else{
        tipo_accesso=2;
    }
    
    switch(ino_leido.i_idPermisos[tipo_accesso]){
        case '0':
            access = 0;
            break;
        case '1':
            access = 1;
            break;
        case '2':
            access = 2;
            break;
        case '3':
            access = 3;
            break;
        case '4':
            access = 4;
            break;
        case '5':
            access = 5;
            break;
        case '6':
            access = 6;
            break;
        case '7':
            access = 7;
            break;
    }
    
    return access;
}